import React from 'react';
import { render } from 'react-dom';

import { getLatestUf } from './app/actions/indeces.js';

import { connect, Provider } from 'react-redux';

import { ConnectedRouter } from 'react-router-redux';

import Root from './app/index.jsx';

import store, { history }  from './app/store.js';

const App = connect(state => ({
	location: state.routerReducer.location
}))(Root);

class ReactApp extends React.Component {
	render(){
		return (
			<div>
				<Provider store={store}>
					<ConnectedRouter history={history}>
						<App />
					</ConnectedRouter>
				</Provider>
			</div>
		);
	}
}

let reactRootNode = document.getElementById('react-root');
render(<ReactApp />, reactRootNode);

/* Sets up the app at boot */
store.dispatch(getLatestUf());
