const dict = {
	language: {
		and: {
			en: "and",
			es: "y"
		}
	},
	alert: {
		error: {
			propertyCodeNotFound: {
				en: "The property could not be found. Please make sure you typed in the correct code.",
				es: "La propiedad con este código no pudo ser encontrada. Por favor, verifica el código.",
				zh: ""
			}
		}
	},
	meta: {
		description: {
			default: {
				en: "Property purchase/sale/rent. Discover the best properties in Santiago and across Chile. We are experts in real estate and real estate investments, and provide different services including property management and short-term rentals.",
				es: "Compra, arriendo y venta de Propiedades. Descubre las mejores propiedades de Santiago y el resto de Chile. Somos expertos en corretaje e inversiones inmobiliarias y ofrecemos diversos servicios incluyendo administración de propiedades y estadias cortas."
			},
			articlesIndex: {
				en: "Real estate blog, ranging from investments to the market trends",
				es: "Blog relacionado al mundo del corretaje de propiedades, inversiones inmobiliarias y tendencias del mercado."
			},
			sale: {
				en: "Buy your dream property or invest in real estate.",
				es: "Compre la propiedad de sus sueños o invierta en el mercado inmobiliario."
			},
			rent: {
				en: "Rent an apartment or a house to live comfortably or a shop for your business.",
				es: "Arriende un departamento o una casa para vivir, o un local comercial para su negocio."
			},
			season: {
				en: "Discover our daily and weekly vacation rentals.",
				es: "Descubra las propiedades que tenemos para arrendar y disfrutar unos días de vacaciones. Estadias cortas y arriendos por semanas y meses."
			},
			quotes: {
				en: "If you are looking for a specific kind of property, we will help you find it. We work with many associates to make your dreams come true.",
				es: "Si está buscando un tipo de propiedad en particular, puede hacernos un encargo y encontraremos la propiedad de sus sueños o la siguiente gran inversión para usted. Contamos con una base de datos y una red extensa de agentes asociados."
			}
		},
		defaultName: {
			en: "LIU Realty",
			es: "LIU Propiedades",
		},
		homeTitle: {
			en: "Buy, Sell and Rent Properties",
			es: "Compra, Vende y Arrienda Propiedades"
		},
		saleTitle: {
			en: "Properties for sale",
			es: "Propiedades en venta"
		},
		rentTitle: {
			en: "Properties for rent",
			es: "Propiedades en arriendo"
		},
		seasonTitle: {
			en: "Short stays and short-term and mid-term rentals",
			es: "Estadías cortas. Arriendos por día o semanas"
		},
		selectLanguage: {
			en: "Select a language",
			es: "Selecciona un idioma",
		},
		articlesTitle: {
			en: "Blog",
			es: "Indice de Articulos"
		},
		quotesTitle: {
			en: "Find your dream property",
			es: "Encuentre la propiedad de sus sueños"
		},
		justListedTitle: {
			en: "Just listed",
			es: "Recien publicadas"
		},
		justListedSubtitle: {
			en: "Discover our new listings",
			es: "Descubre nuestras nuevas propiedades"
		},
		title: {
			operation: {
				sale: {
					en: "for sale",
					es: "se vende",
				}, 
				rent: {
					en: "for rent",
					es: "se arrienda"
				},
				season: {
					en: "short-term rental",
					es: "arriendo por día"
				}
			}
		}
	},
	error: {
		translation_failed: {
			en: "Could not fetch translation",
			es: "",
			zh: ""
		}
	},
	progress: {
		loading: {
			en: "loading",
			es: "cargando",
			zh: ""
		},
		translating: {
			en: "translating",
			es: "",
			zh: ""
		}
	},
	date: {
		postedOn: {
			en: "posted on",
			es: "publicado el",
			zh: ""
		},
		listed: {
			en: "Listed",
			es: "Publicado"
		}
	},
	fields: {
		fullname: {
			en: "Fullname",
			es: "Nombre completo",
			zh: ""
		},
		email: {
			en: "Email",
			es: "Email",
			zh: ""
		},
		phoneNumber: {
			en: "Phone number",
			es: "Teléfono",
			zh: ""
		},
		dreamProperty: {
			en: "Describe the property of your dreams...",
			es: "Describe la propiedad de tus sueños...",
			zh: ""
		},
		typeInCode: {
			en: "Type in your code: LIU17",
			es: "Ingresa el código: LIU17",
			zh: ""
		}
	},
	headers: {
		main: {
			en: "Find the property of your dreams",
			es: "Encuentra la propiedad ideal",
			zh: ""
		},
		listings: {
			en: "Properties on the market",
			es: "Propiedades disponibles",
			zh: ""
		},
		featured: {
			en: "Featured properties",
			es: "Propiedades destacadas",
			zh: ""
		},
		articles: {
			en: "Articles",
			es: "Artículos",
			zh: ""
		},
		quotes: {
			en: "You have made the right choice",
			es: "Has hecho la elección correcta",
			zh: ""
		},
		suggested: {
			en: "Suggested properties",
			es: "Propiedades sugeridas",
			zh: ""
		}
	},
	instructions: {
		quotes: {
			en: "Once you complete the required information below, one of our agents will contact you to find the property you are looking for.",
			es: "Una vez que completes la información que se solicita a continuación, un agente de LIU Propiedades se pondrá en contacto contigo para ayudarte a encontrar la propiedad que deseas.",
			zh: ""
		},
	},
	operationAsVerb: {
		sale: {
			en: "buy",
			es: "comprar",
			zh: ""
		},
		rent: {
			en: "rent",
			es: "arrendar",
			zh: "",
		},
		season: {
			en: "season",
			es: "temporada",
			zh: ""
		}
	},
	operationAsNoun: {
		sale: {
			en: "Sale",
			es: "Venta",
			zh: ""
		},
		rent: {
			en: "Lease",
			es: "Arriendo",
			zh: "",
		},
		season: {
			en: "Season",
			es: "Temporada",
			zh: ""
		}	
	},
	navigation: {
		language: {
			en: "Language",
			es: "Idioma"
		},
		navbar: {
			main: {
				en: "LIU Realty",
				es: "LIU Propiedades",
				zh: ""
			},
			blog: {
				en: "Blog",
				es: "Blog",
				zh: "",
			},
			quotes: {
				en: "Get a Quote",
				es: "Solicitudes",
				zh: ""
			},
			services: {
				en: "Services",
				es: "Servicios",
				zh: ""
			}
		},
		buttons: {
			search: {
				en: "search",
				es: "buscar",
				zh: ""
			},
			send: {
				en: "send",
				es: "enviar",
				zh: ""
			},
			fullscreen: {
				en: "fullscreen",
				es: "pantalla completa",
				zh: ""
			},
			description: {
				en: "description",
				es: "descripción",
				zh: ""
			},
			map: {
				en: "map",
				es: "mapa",
				zh: ""
			},
			video: {
				en: "video",
				es: "video",
				zh: ""
			},
			findByCode: {
				en: "Find by code",
				es: "Buscar por código",
				zh: "",
			},
			findByOperation: {
				en: "Find by operation",
				es: "Buscar por operación",
				zh: ""
			}
		}
	},
	property: {
		type: {
			apartment: {
				en: "apartment",
				es: "departamento",
				zh: ""
			},
			house: {
				en: "house",
				es: "casa",
				zh: "",
			},
			office: {
				en: "office",
				es: "oficina",
				zh: ""
			},
			commercial: {
				en: "commercial",
				es: "comercial",
				zh: ""
			},
			store: {//so we can use local comercial in meta title
				en: "shop",
				es: "local comercial",
				zh: ""
			},
			business: { //denotes derecho de llave / only for chile
				en: "business",
				es: "derecho de llave",
				zh: ""
			},
			industrial: {
				en: "industrial",
				es: "industrial",
				zh: ""
			},
			storage: {
				en: "storage",
				es: "bodega/galpon",
				zh: ""
			},
			lot: {
				en: "lot",
				es: "sitio",
				zh: ""
			},
			land: {
				en: "land",
				es: "terreno",
				zh: ""
			},
			parking: {
				en: "parking",
				es: "estacionamiento",
				zh: ""
			}
		},
		configuration: {
			bedroom: {
				en: "bedroom",
				es: "dormitorio",
				zh: ""
			},
			bathroom: {
				en: "bathroom",
				es: "baño",
				zh: ""
			},
			space: {
				en: "room",
				es: "espacio",
				zh: ""
			},
			guest: {
				en: "guest",
				es: "huésped",
				zh: ""
			},
			unit: {
				en: "unit",
				es: "unidad",
				zh: ""
			},
			studio: {
				en: "studio",
				es: "monoambiente",
				zh: ""
			},
			parking_spot: {
				en: "parking spot",
				es: "estacionamiento",
				zh: ""
			},
			storage_room: {
				en: "storage room",
				es: "bodega",
				zh: ""
			}
		},
		area: {
			total_area: {
				en: "total area",
				es: "superficie total",
			},
			built_up_area: {
				en: "built-up area",
				es: "superficie útil"
			},
			land_area: {
				en: "land area",
				es: "superficie terreno"
			}
		}
	},
	disclaimer: {
		footer: {
			en: "All information furnished regarding property for sale or rental is from sources deemed reliable, but no warranty or representation is made as to the accuracy thereof and same is submitted subject to errors, omissions, change of price, rental or other conditions, prior sale, lease or financing or withdrawal without notice.",
			es: "Toda la información presentada respecto a las propiedades para la venta o arriendo se tiene como confiable, pero no se garantiza su exactitud. La misma esta sujeta a errores, omisiones, cambios de precio u otras condiciones antes de la venta o el arriendo sin previo aviso.",
			zh: ""
		}
	}
};

export default dict;
