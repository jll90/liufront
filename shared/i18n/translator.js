import dict from '../../shared/i18n/dict.js';
import { isEmptyObject } from '../../shared/tools/object_manipulation.js';

export const findInDict = (lang, key, innerDict = {}) => {	
	const keysArr = key.split(".");
	const k = keysArr[0];

	const d = (isEmptyObject(innerDict) === true) ? dict : innerDict;
	if (dictHasKey(d, k)){
		keysArr.shift();
		const newKeys =  keysArr.join(".");
		const newDict = d[k];
		return findInDict(lang, newKeys, newDict);
	}

	if (atLangDef(d)){
		return d[lang];
	}
	
	throw("Could not find key in the dictionary");
}

const dictHasKey = (d, k) => {
	if (d.hasOwnProperty(k)){
		return true;
	} 
	return false;	
}

const atLangDef = (d) => {
	if (d.hasOwnProperty("es")){
		return true;
	}
	return false;
}

const traverseDict = (d, k) => {
	return d[k];
}