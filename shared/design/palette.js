const colors = {
	blue0: "#A4C8E1",
	blue1: "#91AFD7",
	blue2: "#0A5694",
	blue3: "#000D42",
	red: "#D00000"
};

export default colors;
