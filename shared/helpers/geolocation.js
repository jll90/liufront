export const parseAddress = addr => {
	const address = getAdressFromComponents(addr.addressComponents);

	return address;
}

export const getAdressFromComponents = components => {
	const length = components.length;
	var address = ""

	var streetNumber = null;
	var streetName = null;
	var comune = null;
	var city = null;

	for (var i = 0; i < length; i++){
		const component = components[i];
		const type = component.types[0]
		const longName = component.longName;

		switch(type){
			case "street_number":
				streetNumber = longName;
				break;
			case "route":
				streetName = longName;
				break;
			case "administrative_area_level_3":
				comune = longName;
				break;
			case "administrative_area_level_2":
				city = longName;
				break;
			default: 
				break;
		}
	}

	address = [
		streetName,
		" ",
		streetNumber,
		", ",
		comune,
		", ",
		city
	].join("");

	return address;
}
