import React from 'react'
/* Needs to move out frontEndRouter so
this file has no dependencies - redouced coupling */
import { frontEndRouter } from '../../app/routing/router.js';

export function getPermalinkFromRoute(pathname = window.location.pathname) {
	return pathname.split("/")[3];
}

export const isEditingProperty = () => {
	const pathname = window.location.pathname;
	return !(pathname.indexOf(frontEndRouter('newPropertyPath')) > -1);
}

export const getIndexParamsFromUrl = () => {
	const operation = getOperationFromWord(getOperationFromPathname());
	const propertyType = getPropertyTypeFromWord(getPropertyTypeFromPathname());
	return { operation, propertyType };
}

export const getOperationFromWord = (w) => {
	switch (w){
		case "venta":
		case "sale":
			return 0;

		case "rent":
		case "arriendo":
			return 1;
	}
}

export const getPropertyTypeFromWord = (w) => {
	switch (w) {
		case "casa":
		case "house":
			return 0; 
		case "departamento":
		case "apartment":
			return 1; 
		case "oficina":
		case "office":
			return 2; 
		case "comercial":
		case "commercial":
			return 3; 
	}
} 

export function pathnameWithOperationAndPropertyType(operation, propertyType, frontOrBack){
	const op = operation;
	const pt = propertyType;
	const router = frontEndRouter; 

	if (op === 0 || op === "venta" || op === "sale" ){
		switch(pt){
			case 0:
			case "departamento":
			case "apartment":
				return router('apartmentsForSalePath');
			case 1:
			case "casa":
			case "house":
				return router('housesForSalePath');
			case 2:
			case "oficina":
			case "office":
				return router('officesForSalePath');
			case 3:
			case "comercial":
			case "commercial":
			  return router('commercialForSalePath'); 				
		}
	}
	if (op === 1 || op === "arriendo" || op === "rent" ){
		switch(pt){
			case 0:
			case "departamento":
			case "apartment":
				return router('apartmentsForRentPath');
			case 1:
			case "casa":
			case "house":
				return router('housesForRentPath');
			case 2:
			case "oficina":
			case "office":
				return router('officesForRentPath');
			case 3:
			case "comercial":
			case "commercial":
				return router('commercialForRentPath');
		}
	}
	if (op === 2 || op === "temporada" || op === "season" ){
		switch(pt){
			case 0:
			case "departamento":
			case "apartment":
				return router('apartmentsSeasonPath');	
			case 1:
			case "casa":
			case "house":
				return router('housesSeasonPath');
		}
	}
	throw ("The path could not be determined");
}

export function pricedInCLP(pricingUnit){
	return (pricingUnit === "CLP");
}

export function pricedInUF(pricingzzzzUnit){
	return (pricingUnit === "UF");
}

export function getOperationFromPathname(){
	return window.location.pathname.split("/")[2];
}

export function getPropertyTypeFromPathname(){
	return window.location.pathname.split("/")[3];
}

export function canHouseGuests(g){
	return (g !== undefined && g !== null && g > 0);
}

export function isStudio(isStudio){
	return isStudio;
}

export function hasBedrooms(b){
	return (b > 0);
}

export function hasBathrooms(b){
	return (b > 0);
}

export function hasParkingSpots(s) {
	return (s > 0);
}

export function hasStorageRooms(r) {
	return (r > 0);
}

export function isHouse(t) {
	return (t === 0);
}

export function isApartment(t) {
  return (t === 1);
}

export function isOffice(t) {
  return (t === 2);
}

export function isCommercial(t) {
  return (t === 3);
}

export function isIndustrial(t) {
  return (t === 4);
} 

export function isStorage(t) {
  return (t === 5);
}

export function isLot(t) {
  return (t === 6);
}

export function isLand(t) {
  return (t === 7);
}

export function isParking(t) {
  return (t === 8);
}

export function isSale(o) {
  return (o === 0);
}

export function isRent(o) {
  return (o === 1);
}

export function isSeason(o) {
  return (o === 2);
}

export function hasVideo(v){
	return ( v !== null && v.length > 0);
}
