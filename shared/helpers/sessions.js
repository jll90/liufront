import { setItem, getItem, removeItem } from '../providers/storage_provider.jsx';

const LOGGED_IN = "loggedIn";

export function isLoggedIn(){
	return (getItem(LOGGED_IN) === "true");
}

export function logIn(){
	setItem(LOGGED_IN, true);
}

export function logOut(){
	removeItem(LOGGED_IN);
}