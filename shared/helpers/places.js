export function getStateIdFromCityId(cityId, cities){
	const c = cities;
	const l = cities.length;

  for (var i=0; i < l;i++){
		if (parseInt(c[i].id) === parseInt(cityId)){
			return c[i].stateId;
		}
	}

	throw ("Could not obtain city from id");
}

export function getNameFromId(id, set){
	if (parseInt(id) > 0 && set.length > 0){
		const l = set.length;
		for (var i=0; i < l; i++){
			if (parseInt(id) === set[i].id){
				return set[i].name;
			}
		}
		throw ("Could not retrieve name - invalid id");
	} else {
		return "";
	}
}
