export function formatOwnersAsOwnership(data, propertyId){
	var newData = data;
	var owners = data.owners;
	var length = owners.length;
	var ownerships = [];

	for (var i=0; i < length; i++){
		if (propertyId === undefined){
			ownerships.push({clientId: owners[i].id})
		} else {
			ownerships.push({clientId: owners[i].id, propertyId: propertyId })	
		}
		
	}

	/*FOR RoR use alone*/

	newData.ownershipsAttributes = ownerships;
	/*newData.owners = undefined;*/

	/*====*/ 
	
	return newData;
}