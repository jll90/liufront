export function alignFullScreenSlides(){
  /* todo check that container exists when function is called */
	var imgNodes = document.querySelectorAll(".fullscreen-slider-container .background-image");
	var slidesLength = imgNodes.length;

	var viewportWidth = document.body.scrollWidth;
	var viewportHeight = window.innerHeight;

	for (var i = 0; i < slidesLength; i ++){
		var imgNatWidth = imgNodes[i].naturalWidth;
		var imgNatHeight = imgNodes[i].naturalHeight;
		var imgAspectRatio = imgNatWidth / imgNatHeight;

		var imgBrowserWidth = viewportWidth; //img width is set to 100%;
		var imgBrowserHeight = imgBrowserWidth / imgAspectRatio;

		if (imgAspectRatio > 1){
			imgNodes[i].className += " landscape";
			var imgMarginTop = (viewportHeight - imgBrowserHeight)/2;
			imgNodes[i].style.marginTop = imgMarginTop + 'px';
		} else {
			imgNodes[i].className += " portrait";
			imgNodes[i].style.height = viewportHeight + 'px'; 
		}
	}
}

export function createStyleObject(url){
	const style = {
		"backgroundImage": ("url("+url+")"),
		"backgroundSize": 'cover',
		"backgroundPosition": 'center center',
		"backgroundRepeat": 'no-repeat'
	};

	return style;
}