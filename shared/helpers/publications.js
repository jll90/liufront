import moment from 'moment';

export function shouldUpdatePublication(p){
	const lastUpdated = moment(p.updatedAt);
	const timeNow = moment();

	const lastUpdateDiff = timeNow.diff(lastUpdated, "hours");
	const hoursToUpdate = p.daysToUpdate * 24;

	return (lastUpdateDiff > hoursToUpdate) ? true : false;
}