import { getSellingPointNameAndId } from '../tools/hashes.js';

export function sortSellingPointsByCategory(sellingPoints){
	 var sellingPointsLength = sellingPoints.length;
	 var uniqCategoriesIds = [];
	 var sellingPointsSortedByCategory = [];	 

	 //creates arrays with category names

	 for (var i = 0; i < sellingPointsLength; i++){
	 	var id = sellingPoints[i].category;
	 	
	 	if (uniqCategoriesIds.indexOf(id) === -1){
	 		uniqCategoriesIds.push(id);
	 	}
	 }

	 var uniqCategoriesLength = uniqCategoriesIds.length;

	 //maps array of [{id: '', name: ''}...] to [ [cat1id, cat1name, [desc1, desc2, desc3] ... ]
	 for (var i = 0; i < uniqCategoriesLength; i++){

	 	var id = uniqCategoriesIds[i];
	 	var categoryName = getSellingPointNameAndId(id).name;
	 	var categoryId = id;
	 	var currentEl = [];
	 	var matchingSellingPoints = [];	 	
	 	//pushes category id so it can be indexed later on
	 	//pushes category name
	 	currentEl.push(categoryId);
	 	currentEl.push(categoryName);

	 	for (var j = 0; j < sellingPointsLength; j++){
	 		//matches by category id 
	 		if (parseInt(categoryId) === parseInt(sellingPoints[j].category)){
	 			matchingSellingPoints.push(sellingPoints[j]);
	 		}
	 		
	 	}
	 	currentEl.push(matchingSellingPoints);
	 	sellingPointsSortedByCategory.push(currentEl);
	 }

	 return sellingPointsSortedByCategory;
}
