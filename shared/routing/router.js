import { isDevelopment, isProduction } from '../../app/config/environment.js';
import routes from './routes.js';

export function backEndRouter(pathname, config){

	if ( pathname === 'visitsPath' ){
		return "http://localhost:8080/visits";
	}

	if ( pathname === 'clientsPath' ){
		return "http://localhost:8080/clients";
	}

	if ( pathname === 'propertyVisitsPath'){
		return "http://localhost:8080/properties/"+config.id+"/visits";
	}

	if (routes.hasOwnProperty(pathname)){
		var route = routes[pathname];
		if (typeof route === "function"){
			route = route(config);
		}

		if ( pathname === 'fileUploadPath' || pathname === 'fileDeletePath' ){
			return "http://11.11.11.11" + route;
		}


		if ( isDevelopment() ){
			return "http://localhost:3000" + route;
		}


		if ( isProduction() ){
			return "http://server.penthouse.solutions" + route;
		}


		throw ("environment could not be determined");
	}

	throw ("route doesn't exist");
}
	
