const routes = {
	rootPath: "/",

	quotesPath: "/quotes",

	authenticatePath: "/authenticate",

	propertiesPath: "/properties",

	indexPropertiesPath: (config) => {
		const keys = [];
		for (let key in config){
			keys.push(`${key}=${config[key]}`);
		}	
		return "/properties?" + keys.join("&");
	},	
	propertyPath: function (config){
		return "/properties/"+ config.id;
	},
	privatePropertyPath: function (config){
		return "/properties/" + config.id + "/private_profile";
	},
	propertyPathByCode: function(config){
		return "/properties/code/" + config.code
	},
	editPropertyPath: function(config){
		return "/properties/" + config.id + "/edit";	
	},
	featuredPropertiesPath: "/properties/featured_index",
	justListedPropertiesPath: "/properties/just_listed_index",
	suggestedPropertiesPath: function(config){
		return ["/properties/suggestion?property_id=", config.id, "&operation_type=", config.operationType].join(""); 
	},
	dashboardPropertiesPath: function(config){
		return ["/properties/filtered_index?filter=", config.filter].join("");
	},

	newClientPath: "/clients/new",
	clientsPath: "/clients",

	publicationsPath: function(config){
		if (config.hasOwnProperty("propertyId")){
			return "/publications?property_id=" + config.propertyId;
		} else {
			return "/publications";
		}
	},
	publicationPath: function(config){
		return "/publications/" + config.id;	
	},

	createArticlePath: "/blog",
	articlesPath: function(config){
		return "/blog?lang=" + config.language;
	},
	articlePath: function(config){
		return "/blog/" + config.id;	
	},

	placesPath: "/places",

	propertyPhotosPath: function (config) {
		return ["/properties/", config.propertyId, "/photos"].join("");
	},
	photoPath: function (config) {
		return ["/photos/", config.id].join("");
	},

	singleEmailPath: "/mailer/get_to_know_us",
	processCsvPath: "/process_csv",
	welcomeEmailPath: "/mailer/send_welcome_email",

	inboxPath: "/inbox",

	indecesPath: function(config){
		return ["/indices?index=", config.indexName].join("");
	},
	usersPath: "/users",

	fileUploadPath: "/upload.php",
	fileDeletePath: function(config){
		return ["/delete.php?file=", config.filename].join("");
	}
};

export default routes;
