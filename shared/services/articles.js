import { nestObject } from '../tools/object_manipulation.js';

import serviceHandler from './handler.js';

export const create = config => {
	config.data = nestObject("article", config.data);
	return serviceHandler(config, 'articlesPath', "POST");
}

export const index = config => {
	return serviceHandler(config, 'articlesPath', "GET");
}

export const show = config => {
	return serviceHandler(config, 'articlePath', "GET");
}
