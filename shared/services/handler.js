import jsonProvider from '../../app/providers/json_provider.js';
import { backEndRouter } from '../routing/router.js';

const serviceHandler = (config, pathname, action) => {
	if (pathname.indexOf("http") > -1){
	/* hack for google services */
		var url = pathname;
	} else {
		var url = backEndRouter(pathname, config);	
	}
	let jsonCall = null
	switch (action){
		case "GET":
			jsonCall = jsonProvider.get;
			break;
		case "POST":
			jsonCall = jsonProvider.post;
			break;
		case "PATCH":
			jsonCall = jsonProvider.patch;
			break;
		case "DELETE":
			jsonCall = jsonProvider.del;
			break;
		default:	
			throw ("No action provided");
	}

	return jsonCall({...config, url});
}

export default serviceHandler;
