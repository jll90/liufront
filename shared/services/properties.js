import { nestObject, 
		clearNulls } from '../tools/object_manipulation.js';

import serviceHandler from './handler.js';

export const show = config => {
	const postProcessor = data => {
		return clearNulls(data);
	}
	return serviceHandler({...config, postProcessor}, 'propertyPath', "GET");
}

export const privateShow = config => {
	const postProcessor = data => {
		return clearNulls(data);
	}
	return serviceHandler({...config, postProcessor}, 'privatePropertyPath', "GET");
}

export const edit = config => {
	const postProcessor = data => {
		return clearNulls(data);
	}
	return serviceHandler({...config, postProcessor}, 'editPropertyPath', "GET");
}

export const index = config => {
	return serviceHandler(config, 'indexPropertiesPath', "GET"); 
}

export const create = config => {
	config.data = nestObject("property", config.data);
	return serviceHandler(config, 'propertiesPath', "POST");
}

export const update = config => {
	const data = config.data;
	delete data['userId'];
	delete data['id'];
	delete data['region'];
	delete data['fullAddress'];
	delete data['comune'];
	delete data['city'];
	delete data['neighborhood'];
	delete data['noComission'];
	delete data['updatedAt'];
	delete data['createdAt'];
	config.data = nestObject("property", data);
	return serviceHandler(config, 'propertyPath', "PATCH");
}

export const filteredIndex = config => {
	return serviceHandler(config, 'dashboardPropertiesPath', "GET"); 
}

export const justListedIndex = config => {
	return serviceHandler(config, 'justListedPropertiesPath', "GET");
}

export const findCode = config => {
	return serviceHandler(config, 'propertyPathByCode', "GET");
}
