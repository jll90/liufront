import serviceHandler from './handler.js';


export const getAddressInfo = config => {
	const url = "http://maps.google.com/maps/api/geocode/json?address=" + config.address;
	return serviceHandler({...config, noHeaders: true, noData: true}, url, "GET");
}

export const reverseGeocoding = config => {
	const lat = config.lat;
	const lng = config.lng;
	let url = [
		"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBsJh4-kNKuyZzwcAJkFDEFMxETHHtEbfM&latlng=",
		lat,
		",",
		lng,
	].join("");

	return serviceHandler({...config, noHeaders: true, noData: true}, url, "GET");

}
