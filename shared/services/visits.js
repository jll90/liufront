import jsonProvider from '../../app/providers/json_provider.js';
import { nestObject } from '../tools/object_manipulation.js';
import { backEndRouter } from '../routing/router.js';

export function create(config){
	const url = backEndRouter('visitsPath');
	jsonProvider.post({...config, url});
}

export function indexAll(config){
	const url = backEndRouter('visitsPath'); 
}

export function indexByProperty(config){
	const url = backEndRouter('propertyVisitsPath', { id: config.id });
	jsonProvider.get({...config, url});
}
