import jsonProvider from '../../app/providers/json_provider.js';
import { nestObject } from '../tools/object_manipulation.js';
import { backEndRouter } from '../routing/router.js';

export function createUser(config){
	const url = backEndRouter('usersPath');
	config.url = url;
	config.data = nestObject('user', config.data);
	jsonProvider.post(config);
}
