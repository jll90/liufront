import serviceHandler from './handler.js';
import { nestObject } from '../tools/object_manipulation.js';

export const create = config => {
	config.data = nestObject("client", config.data);
	return serviceHandler(config, 'clientsPath', "GET");
}

export const index = config => {
	return serviceHandler(config, 'clientsPath', "GET");
}
