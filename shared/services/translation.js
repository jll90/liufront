import jsonProvider from '../../app/providers/json_provider.js';

const apiKey = "AIzaSyA8AMv3-EnCI7vAWCCGFdoFZnHzQargGzk";

const translate = config => {
	var url = "https://translation.googleapis.com/language/translate/v2?";
	url += ["q=", config.str, "&", "key=", apiKey, "&", "target=" , config.target].join("");
	return jsonProvider.post({...config, url});
}

export default translate;
