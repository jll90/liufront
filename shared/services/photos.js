import serviceHandler from './handler.js';
import { nestObject }	from '../tools/object_manipulation.js';


//cannot use name delete
export const destroy = config => {
	return serviceHandler(config, 'photoPath', "DELETE");
}

export const create = config => {
	config.data = nestObject('photo', config.data);
	return serviceHandler(config, 'propertyPhotosPath', "POST");
}

export const update = config => {
	return serviceHandler(config, 'photoPath', "PATCH");
}

export const index = config => {
	return serviceHandler(config, 'propertyPhotosPath', "GET");
}

export const updateSort = config => {
	return serviceHandler(config, 'propertyPhotosPath', "PATCH");
}	