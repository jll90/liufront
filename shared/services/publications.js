import jsonProvider from '../../app/providers/json_provider.js';
import { nestObject	} from '../tools/object_manipulation.js';
import { backEndRouter } from '../routing/router.js';

export function create(config){
	const url = backEndRouter('publicationsPath', config);
	const ajaxConfig = config;
	ajaxConfig.url = url;
	jsonProvider.post(ajaxConfig);
}

export function index(config){
	const url = backEndRouter('publicationsPath', config);
	const ajaxConfig = config;
	ajaxConfig.url = url;
	jsonProvider.get(ajaxConfig);
}

export function update(config){
	const url = backEndRouter('publicationPath', config);
	const ajaxConfig = config;
	ajaxConfig.url = url;
	jsonProvider.patch(config);
}

export function destroy(config){
	const url = backEndRouter('publicationPath', config);
	const ajaxConfig = config;
	ajaxConfig.url = url;
	jsonProvider.del(config);	
}
