import serviceHandler from './handler.js';

export const create = config => {	
	return serviceHandler(config, 'quotesPath', "POST");
}

export const index = config => {
	return serviceHandler(config, 'quotesPath', "GET");
}
