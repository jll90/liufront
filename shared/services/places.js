import serviceHandler from './handler.js';

export const index = config => { 
	return serviceHandler(config, 'placesPath', "GET");
}
