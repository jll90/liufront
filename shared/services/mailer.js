import { nestObject } from '../tools/object_manipulation.js';
import serviceHandler from './handler.js';

export const sendEmail = config => {	
	config.data = nestObject("mailer", config.data);
	return serviceHandler(config, 'singleEmailPath', 'POST');
}

export const sendMultipleEmails = config => {
	config.data = {csv_data: JSON.stringify(config.data)};
	return serviceHandler(config, 'processCsvPath', 'POST');
}

export const sendWelcomeEmai = config => {
	config.data = nestObject("mailer", config.data);
	return serviceHandler(config, 'welcomeEmailPath', 'POST');
}