import serviceHandler from './handler.js';

export const authenticate = config => { 
	return serviceHandler(config, 'authenticatePath', "POST");
}
