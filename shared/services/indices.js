import serviceHandler from './handler.js';


function saveIndexValue(config){
	const url = backEndRouter('indecesPath', {indexName: 'uf'});
	const ajaxConfig = config;
	ajaxConfig.url = url;
	/*must be done like so, so the object moment doesn't get parsed incorrectly*/
	ajaxConfig.data = {
		"index": {
			name: config.data.name,
			reading_time: config.data.readingTime,
			value: config.data.value
		}
	}
	jsonProvider.post(ajaxConfig);
}



export const latestUf = () => {
	return serviceHandler({indexName: 'uf'}, 'indecesPath', "GET");
}
