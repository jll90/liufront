import jsonProvider from '../../app/providers/json_provider.js';

import { backEndRouter } from '../routing/router.js';

export function uploadFile(config) {
	const url = backEndRouter('fileUploadPath');
	config.url = url;
	config.noHeaders = true;
	config.noPreProcessing = true;
	jsonProvider.post(config);
}

export function deleteFile(config){
	const url = backEndRouter('fileDeletePath', {filename: config.filename});
	config.url = url;
	config.noHeaders = true;
	config.noPreProcessing = true;
	jsonProvider.del(config);
}
