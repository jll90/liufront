export const platforms = [
	"Selecciona una plataforma",
	"PortalInmobiliario",
	"Economicos",
	"Locanto",
	"Instagram",
	"Facebook",
	"VivaStreet",
	"LIU Propiedades (facebook)",
	"TocToc",
	"YouTube",
	"Yapo"
];

export function platformToText(id){
	return platforms[id];
}