/* countyStr and Addresstr 
are helper variables and 
as such they're not persisted 
to the databse */

export const INIT_STATE = {
	propertyType: '',
	inhouseDescription: '',
	publicDescription: '',
	operationType: '',
	price: '',
	stage: '',
	smallSurface: '',
	bigSurface: '',
	bedrooms: '',
	bathrooms: '',
	singleSpace: '',
	neighborhood: '',
	streetName: '',
	streetNumber: '',
	buildingNumber: '',
	fakeFullAddress: '',
	storageRooms: '',
	parkingSpots: '',
	serviceRoom: '',
	rolNumber: '',
	permalink: '',
	title: '',
	approximateLocation: '',
	salesLeadBy: '',
	numberOfUnits: '',
	numberOfGuests: '',
	featured: false,
	stateId: '',
	cityId: '',
	countyId: '',
	visitingHrs: '',
	pricingUnit: '',
	publishedOn: '',
	uf: '',
	rightToKey: false,
	tradeable: false,
	videoUrl: '',
	countyStr: '',
	addressStr: '',
	latitude: '',
	longitude: ''
}
