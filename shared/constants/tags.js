const Tags = [
	"Busca Propiedad",
	"Cliente Cerro Negocio con Nosotros",
	"Dueño de una Propiedad",
	"Cliente Exclusivo",
	"Necesita información",
	"Visitó Propiedad",
	"Comerciante",
	"Tiene Familia",
	"Inversionista",
	"Casado", 
	"Divorciado"
]

export default Tags;
