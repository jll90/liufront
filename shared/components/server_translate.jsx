import React from 'react';
import { render } from 'react-dom';

import translate from '../../shared/services/translation.js';

import { currentLanguage } from '../../app/helpers/i18n.js';

const toLang = currentLanguage(); 

class ServerTranslate extends React.Component {

	constructor(){
		super();
		this.state = {
			translating: false,
			translatedTxt: '',
			fallback: false
		}
	}

	componentDidMount(){
		const { srcTxt } = this.props;		
		this.loadData(srcTxt );		
	}

	componentWillReceiveProps(){
		const { srcTxt } = this.props;	
		this.loadData(toLang, srcTxt );		
	}

	loadData = ( srcTxt ) => {
		const self = this;
		if (toLang !== "es"){
		
			self.setState({translating: true})
			
			const successFn = (data) => {
				const translatedTxt = data['data']['translations'][0]['translatedText'];
				self.setState({
					translatedTxt, translating: false
				});
			}
			const errorFn = () => {
				self.setState({
					fallback: true
				})
			}
			const config = {
				target: toLang,
				str: srcTxt
			}
			translate(config)
				.then(data=>{
					successFn(data);
				})
				.catch(err=>{
					errorFn();
				})
		}
	}

	render(){
		const {translating, translatedTxt} = this.state;
		const { srcTxt }  = this.props;
		let txt = null;
		if (toLang === "es"){
			txt = srcTxt;
		} else {
			txt = (translating === true) ? "Translating..." : translatedTxt;	
		}
		return <span>{txt}</span>;
	}
}

export default ServerTranslate;
