import React from 'react';

import { 
	uploadFile as uploadService,
	deleteFile as deleteService	
} from '../../services/files.js';

function WithFileUploader(Component){

return class FileUploader extends React.Component {
	constructor(){
		super();
		this.state = {
			files: [],
		}
	}
	
	componentWillReceiveProps(newProps){
		const files = newProps.files;
		this.addFiles(files);
	}

	getFiles = () => {
		const files = [...this.state.files];
		return files;
	}

	setFiles = (files, cb) => {
		this.setState({files}, cb);
	}

	getFile = (name) => {
		const files = this.getFiles();
		const index = this.getFileIndexByName(name);
		return files[index];
	}

	getFileIndexByName = (name) => {
		const files = this.getFiles();
		const length = files.length;
		let index = null;
		for (var i = 0; i < length; i++){
			const file = files[i];
			if (file.file.name === name){
				index = i;
			}
		}
		return index;
	}

	fireNextPendingUpload = () => {
		const nextUpload = this.findNextUpload();
		if (nextUpload !== false){
			this.uploadFile(nextUpload);
		}
	}

	findNextUpload = () => {
		const files = this.getFiles();
		for (let [index, file] of files.entries() ){
			if (file.uploaded === false && file.retries < 4){
			  return file.file.name;	
			}
		}
		return false;
	}

	updateFile = (name, updateObj = {}, cb) => {
		const files = this.getFiles();
		const filesWithUpdate = files.map((f, i) => {
			if (name === f.file.name){
				const nf = Object.assign({}, f, updateObj);
				return nf;
			}
			return f;
		});	
		this.setFiles(filesWithUpdate, cb);
	}

	uploadFile = (name) => {
		const self = this;
		const file = (this.getFile(name)).file;
		const data = new FormData();
		data.append(0, file);
		const onUploadProgress = (progressEvent) => {
			const { loaded, total } = progressEvent;
			const progress = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
			self.updateFile(name, {progress});	
		}

		const successFn = (data) => {
			this.onUploadSuccess(data, name);
		}

		const errorFn = () => {
			this.onUploadError(name);
		} 

		const config = {
			data, successFn, onUploadProgress, errorFn
		};

		uploadService(config);
	}

	deleteOnServer = (name) => {
		const file = this.getFile(name);
		const filename = file.nameOnServer; 
		const successFn = () => {
		}

		const config = {
			filename, successFn
		};

		deleteService(config);
	}

	onUploadSuccess = (data, name) => {
		const nameOnServer = data.filename;
		this.updateFile(name, {uploaded: true, failed: false, nameOnServer}, this.fireNextPendingUpload);
	}

	onUploadError = (name) => {
		const file = this.getFile(name);
		const retries = file.retries + 1;
		this.updateFile(name, {progress: 0, failed: true, retries}, this.fireNextPendingUpload);
	}

	fileAlreadyExists = (filename) => {
		const files = this.getFiles(); 
		const length = files.length;

		for (var i=0; i<length; i++){
			if (files[i].file.name === filename){
				return true;
			}
		}
		return false;
	}

	parseAndFilterFiles = (files) => {
		const newFiles = [];
		for (let file of files){
			if ( !this.fileAlreadyExists(file.name) ){
				newFiles.push(
					{
						file: file,
						uploaded: false,
						progress: 0,
						uploading: false,
						failed: false,
						retries: 0,
						nameOnServer: ''
					}
				);
			}
		}
		return newFiles;
	}

	addFiles = (files) => {
		const selectedFiles = this.parseAndFilterFiles(files);
		const oldFiles = this.getFiles();

		const newFiles = [...selectedFiles, ...oldFiles];
		this.setFiles(newFiles);
		setTimeout(()=> {
			this.fireNextPendingUpload();
		}, 100);
	}

	deleteFile = (filename) => {
		const currentFiles = this.getFiles(); 

		const files = currentFiles.filter((f, i)=> {
			const filenameMatch = (f.file.name === filename);
			if (f.uploaded === true && filenameMatch){
				this.deleteOnServer(filename);
			}
			return !filenameMatch;
		});

	  this.setFiles(files);	
	}

	render(){
		const files = this.state.files;
		return (
			<div>
				{
					files.map((f, i) => {
						return (
							<Component
								key={f.file.name}
								file={f.file}
								progress={f.progress}
								failed={f.failed}
								uploaded={f.uploaded}
								deleteFile={this.deleteFile}	
							/>
						);
					})
				}
			</div>
		);
	}

}
}

export default WithFileUploader;
