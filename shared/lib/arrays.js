export const cutToNewArray = (arr0, arr1, el) => {
	const nArr0 = removeFromArr(arr0, el);
	const nArr1 = addToArr(arr1, el);
	
	return [nArr0, arr1];
}

export const copyArray = (arr) => {
	const newArr = [...arr];
	return newArr;
}

const findInArr = (arr, el) => {
  return arr.indexOf(el);
}

const removeFromArr = (arr, el) => {
	const i = findInArr(arr, el); 
    
	if (i !== -1) {
			arr.splice(i, 1);
	}

	return arr;
}

const addToArr = (arr, el) => {
	arr.push(el)
	return arr;
}
