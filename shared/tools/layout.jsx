export function disableBodyScroll(){
	document.body.style.overflowY = "hidden";
}

export function enableBodyScroll(){
	document.body.style.overflowY = "visible";
}

