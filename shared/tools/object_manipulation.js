function jsonTransformKeys(json, fn){
	if (json instanceof Array){
		var newJSON = [];
		for (var prop in json){
			var val = json[prop];
			if (isObject(val)){
				val = jsonTransformKeys(val, fn);
			}
			newJSON.push(val);
		}
	} else {
    var newJSON = {};
		for (var prop in json){
			var newCaseProp = fn(prop);
			newJSON[newCaseProp] = json[prop];	
			if (isObject(json[prop])){
				newJSON[newCaseProp] = jsonTransformKeys(json[prop], fn);
			} 
		}
	}
	return newJSON;
}

function jsonTransformData(json, fn){
	if (json instanceof Array){
		var newJSON = [];
		for (var prop in json){
			var val = json[prop];
			if (isObject(val)){
				val = jsonTransformData(val, fn);
			}
			newJSON.push(val);
		}
	} else {
		var newJSON = {};
		for (var prop in json){
			newJSON[prop] = fn(json[prop]);
			if (isObject(json[prop])){
				newJSON[prop] = jsonTransformData(json[prop], fn);
			} 
		}
	}
	return newJSON;
}

function strCamelCaseToSnakeCase(str){	
	var underscoredStr = str.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
	return underscoredStr;
}

function strSnakeCaseToCamelCase(s){
    return s.replace(/(\_\w)/g, function(m){return m[1].toUpperCase();});
}

function isObject(o){
	return (Object(o) === o);
}

function nullToEmptyStr(val){
	return val === null ? "": val;
}

export function jsonCamelCaseToSnakeCase(json){
	return jsonTransformKeys(json, strCamelCaseToSnakeCase);
}

export function jsonSnakeCaseToCamelCase(json){
	return jsonTransformKeys(json, strSnakeCaseToCamelCase);
}

export function clearNulls(json){
	return jsonTransformData(json, nullToEmptyStr);
}

export function nestObject(modelName, data){
	var json = {};
	json[modelName] = data;
	return json;	
}

export function isEmptyObject(o){
	return (Object.keys(o).length === 0 && o.constructor === Object);
}
