import numeral from 'numeral';

numeral.register('locale', 'es', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'mm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function (number) {
        var b = number % 10;
        return (b === 1 || b === 3) ? 'er' :
            (b === 2) ? 'do' :
            (b === 7 || b === 0) ? 'mo' :
    (b === 8) ? 'vo' :
    (b === 9) ? 'no' : 'to';
    },
    currency: {
        symbol: '$'
    }
});

numeral.locale('es');

export function numberFormatter(number){
    return numeral(number).format('$0,0[.]00');
}

export function ufFormatter(uf){
    return numeral(uf).format('0,0[.]00') + " UF";
}

export function approximatePrice(priceInUf, todaysUf){
    const approxPrice = priceInUf * todaysUf;
    const formattedApprox = numeral(approxPrice).format('$0,0');
    return ["(", formattedApprox,")"].join("");
}

const trimDotsAndDashes = function (rut){
	var newRut = rut.replace(/-/g, "");
	newRut = newRut.replace(/\./g, "");
	return newRut;
}

const splitRutAndDv = function (rut){
		var cValue = rut;
		if(cValue.length === 0) { return [null, null]; }
		if(cValue.length === 1) { return [cValue, null]; }
		var cDv = cValue.charAt(cValue.length - 1);
		var cRut = cValue.substring(0, cValue.length - 1);
		return [cRut, cDv];
}

export const rutFormatter = function(rut){
		var rutAndDv = splitRutAndDv(trimDotsAndDashes(rut));
		var cRut = rutAndDv[0]; var cDv = rutAndDv[1];
		if(!(cRut && cDv)) { return cRut || rut; }
		var rutF = "";
		var thousandsSeparator = ".";
		while(cRut.length > 3) {
			rutF = thousandsSeparator + cRut.substr(cRut.length - 3) + rutF;
			cRut = cRut.substring(0, cRut.length - 3);
		}
		return (cRut + rutF + "-" + cDv);
}
