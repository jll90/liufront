export function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return toUpperCase(txt)});
}

export function toUpperCase(str){
	return (str.charAt(0).toUpperCase() + str.substr(1).toLowerCase());
}

export function hasStr(fullStr, testStr){
	return (fullStr.indexOf(testStr) > -1);
}