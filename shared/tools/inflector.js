import dict from '../i18n/dict.js';

function inflector(number, word, lang){

	if (lang === "es"){
		if (number === 1){
			return word;
		} else {
			var lastChar = word.substr(word.length - 1);
			if (lastChar !== "a" && lastChar !== "o"){
				return word + "es";
			} else {
				return word + "s";	
			}
		}
	}

	if (lang === "en"){
		if (number === 1){
			return word;
		} else {
			return word + "s";
		}
	}

	if (lang === "zh"){
		return word;
	}
}

export default inflector;