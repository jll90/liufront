const clientStatus = [
	"Otro estado",
	"Sin contacto",
	"Rechazo servicio",
	"Debe firmar documentacion",
	"Hay que subir propiedad/propiedades",
	"Esperando oferta",
	"Se firmo promesa",
	"Se firmo compraventa",
	"Vendio o arrendo con nosotros",
	"Busca propiedad",
	"Quiere mas informacion",
	"Esperando a visitar propiedades"
]

const propertyStages = [
	"Sin Contactar",
	"Cliente rechazo - Informacion Pendiente",
	"Cliente Acepto - Visita Pendiente"			,
	"Subiendo al portal -recopilando informacion",
	"Marketing",
	"En negociacion",
	"Operacion completada",
	"Existe como registro",
]

const operationTypes = [
	"Venta",
	"Arriendo",
	"Temporada"
];

const operationTypesAsNotOwner = [
	"Comprar",
	"Arrendar",
	"Temporada"
];

const propertyTypes = [
	"casa",
	"departamento",
	"oficina",
	"comercial",
	"industrial",
	"bodega",
	"sitio",
	"terreno",
	"estacionamiento"
]

const states = [
	"I de Arica y Parinacota",
	"II de Tarapacá",
	"III de Antofagasta",
	"IV de Atacama",
	"V de Coquimbo",
	"VI de Valparaíso",
	"VII de O'Higgins",
	"VIII del Maule",
	"IX del Biobío",
	"X de la Araucanía",
	"XI de los Ríos",
	"XII de los Lagos",
	"XIII de Aisén",
	"XIV de Magallanes",
	"XV Metropolitana",
]

const platforms = [
	{
		name: "ECONOMICOS",
		website: "http://www.economicos.cl",
		daysToUpdate: 4
	},
	{
		name: "VivaStreet",
		website: "http://www.vivastreet.cl",
		daysToUpdate: 12
	},
	{
		name: "Portal Inmobiliario",
		website: "http://www.portalinmobiliario.com",
		daysToUpdate: 10
	},
	{
		name: "LoCanto",
		website: "http://www.locanto.cl/",
		daysToUpdate: 21
	},
	{
		name: "TOCTOC",
		website: "http://intranet.toctoc.com",
		daysToUpdate: 21
	},
	{
		name: "Instagram",
		website: "http://intranet.toctoc.com",
		daysToUpdate: 4
	},
	{
		name: "Facebook",
		website: "http://www.facebook.com",
		daysToUpdate: 5
	}
]

export function platformToText(id){
	
}

export function operationTypeToText(id){
	if (id > -1){
		return operationTypes[id];
	} else {
		return "Seleccionar Operacion";
	}
}

export function propertyTypeToText(id){
	if (id > -1){
		return propertyTypes[id];
	} else {
		return "Seleccionar Tipo de Propiedad";
	}
}

export function clientStatusToText(id){
	if (id > -1){
		return clientStatus[id];	
	} else {
		return "Select a status";
	}
}

export function propertyStageToText(id){
	if (id > -1){
		return propertyStages[id];
	} else {
		return "Seleccionar Etapa";
	}	
}

export function stateToText(id){
	if (id > -1){
		return states[id];
	} else {
		return "Seleccionar Region";
	}
}

export function operationTypeAsNotOwnerToText(id){
	if (id > -1){
		return operationTypeAsNotOwner[id]
	}	else {
		return "Selecciona una operacion"
	}
}
