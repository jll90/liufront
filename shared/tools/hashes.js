export function getSellingPointNameAndId(id){
	var errName = "category name could not be found with the supplied id";
	var hash = [
		{
			id: 0,
			category: "Vivienda"
		},
		{
			id: 1,
			category: "Instalaciones"
		},
		{
			id: 2, 
			category: "Ubicación"
		},
		{
			id: 3,
			category: "Terreno"
		}
	];

	var length = hash.length;

	

	for (var i = 0; i < length; i ++){
		var obj = hash[i];
		if (obj.id === parseInt(id)){
			return {
				id: obj.id,
				name: obj.category
			};
		} 
	}

	throw (errName);
}

export function getPropertyTypeName(id){
	var errName = "property type name could not be found with the supplied id";
	var hash = [
		{
			id: 0,
			name: "Casa"
		},
		{
			id: 1,
			name: "Departamento",
		},
		{
			id: 2,
			name: "Oficina",
		},
		{
			id: 3,
			name: "Comercial",
		},
		{
			id: 4,
			name: "Industrial",
		},
		{
			id: 5,
			name: "Bodega",
		},		
		{
			id: 6,
			name: "Sitio",
		},		
		{
			id: 7,
			name: "Terreno",
		},
		{
			id: 8,
			name: "Estacionamiento"
		}
	];

	var length = hash.length;

	for (var i = 0; i < length; i ++){
		var obj = hash[i];
		if (obj.id === parseInt(id)){
			return obj.name;
		} 
	}

	throw (errName);
}