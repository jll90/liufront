export const propertyWalletConfig = (data) => {
	const config = {
		chart: {
			type: 'column'
		}, 
		title: {
				text: null 
    },

		xAxis: {
			type: 'datetime'
		},

    yAxis: {
        title: {
            text: 'Numero de Captaciones'
        }
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
						dataGrouping: {
							approximation: "sum",
							enabled: true, 
							units: [['week'], [1]]
						}
        }
    },

    series: [{
        name: 'Captaciones',
        data: data
   }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
	};

	return config;
}
