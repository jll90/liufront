var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter((x) => {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach((mod) => {
    nodeModules[mod] = 'commonjs ' + mod;
  });

//shared by both configurations
var APP_DIR = path.resolve(__dirname, './');

function buildAppConfig(env){

  var BUILD_DIR = path.resolve(__dirname, 'build/');
  var SASS_DIR = path.resolve(__dirname, 'scss/');

  var LiveReloadPlugin = require('webpack-livereload-plugin');
  var ExtractTextPlugin = require('extract-text-webpack-plugin');

  var devConfig = {
    entry: [APP_DIR + '/app.jsx', SASS_DIR + '/application.scss'],
    output: {
      path: BUILD_DIR,
      filename: 'react-dev.js'
    },
    resolve: {
      extensions: [".jsx", ".js", "scss", ".json"]
    },
    module: {
      loaders: [
        {
          test : /\.jsx?/,
            include : APP_DIR,
            loader : 'babel-loader',
            exclude: /node_modules/
        },
        {
          test: /\.json$/,
          loader: 'json-loader'
        },
      	{ // sass / scss loader for webpack
        	test: /\.(sass|scss)$/,
	        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
  	    }
      ]
    },
		plugins: [
    	new LiveReloadPlugin(),
			new ExtractTextPlugin({ // define where to save the file
      filename: 'styles-bundle.css', allChunks: true
    	}),
	  ]
  };

  var prodConfig = {
    entry: APP_DIR + '/app.jsx',
    output: {
      path: BUILD_DIR,
      filename: 'react-bundle.js',
      sourceMapFilename: '[name].map'
    },
		resolve: {
			extensions: [".jsx", ".js", "scss", "json"]
		},
    module: {
      loaders: [
        {
          test : /\.jsx?/,
            include : APP_DIR,
            loader : 'babel-loader',
            exclude: /node_modules/
        },
        {
          test: /\.json$/,
          loader: 'json-loader'
        }
      ]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
          minimize: true,
          beautify: false,
          mangle: {
              screw_ie8: true,
              keep_fnames: true
          },
          compress: {
              screw_ie8: true
          },
          comments: false
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: "'production'"
        }
      }),
    ]
  }

  if (env === "development" || env === undefined){
    return devConfig;
  }

  if (env === "production"){
    return prodConfig;
  }
}

function buildServerConfig(){

  var BUILD_DIR = path.resolve(__dirname, './server_app/');

  var config = {
    entry: APP_DIR + '/server_app/server.js',
    target: 'node',
    node: {
      __dirname: false,
      __filename: false
    },
    output: {
      path: BUILD_DIR,
      filename: 'prod_server.js',
      sourceMapFilename: '[name].map'
    },
    externals: nodeModules,
    resolve: {
      extensions: [".jsx", ".js", "scss", "json"]
    },
    module: {
      loaders: [
        {
          test : /\.jsx?/,
            include : APP_DIR,
            loader : 'babel-loader'
        },
        {
          test: /\.json$/,
          loader: 'json-loader'
        }
      ]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
          minimize: true,
          beautify: false,
          mangle: {
              screw_ie8: true,
              keep_fnames: true
          },
          compress: {
              screw_ie8: true
          },
          comments: false
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: "'production'"
        }
      }),
    ]
  }

  return config;
}

var env = process.env.NODE_ENV;
var app = process.env.NODE_APP;

if (app === "server" && env === "production"){
  module.exports = buildServerConfig(env);
} else {
  module.exports = buildAppConfig(env);
}

