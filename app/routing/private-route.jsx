import React from 'react';
import { render } from 'react-dom';

import { Route, Redirect } from 'react-router-dom';

import { frontEndRouter } from '../../app/routing/router.js';

import { isAuthenticated } from '../../app/providers/authentication_provider.js';

export const PrivateRoute = ({ component: Component, ...rest}) => {
	if (isAuthenticated()){
		return (
			<Route 
				{...rest}
				render={props => 
					<Component {...props} />
				}
			/>
		);
	} else {
		return (
			<Route
				{...rest}
				render={props =>
					<Redirect to={{pathname: frontEndRouter('logInPath'), state: {from: props.location}}} />
				}
			/>
		);
	}
}
