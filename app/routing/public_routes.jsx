import React from 'react';
import { render } from 'react-dom';

import { connect } from 'react-redux';

import { Switch } from 'react-router-dom';

import { PublicRoute } from './public-route.jsx';

import PropertyShow from '../views/properties/show.jsx';

import PropertyIndex from '../views/properties/index.jsx';

import QuoteNew from '../views/quotes/new.jsx';
import SessionNew from '../views/sessions/new.jsx';

import Services from '../views/services.jsx';
import ArticleIndex from '../views/articles/index.jsx';
import ArticleShow from '../views/articles/show.jsx';

import { frontEndRouter } from './router.js';
import Home from '../views/home.jsx';
import LanguageSelector from '../views/language-selector.jsx';
import FileUpload from '../views/fileupload.jsx';
import { MainLayout, EmptyLayout } from '../views/layouts/layouts.jsx';

const ConnectedSwitch = connect(state => ({
	location: state.routerReducer.location
}))(Switch);

class PublicRoutes extends React.Component {
	render(){
		return (
			<ConnectedSwitch>
				<PublicRoute
					exact
					path={frontEndRouter('servicesPath')}
					component={ props =>
						<MainLayout>
							<Services {...props} />
						</MainLayout>
					}
				/>

				<PublicRoute
					exact
					path={frontEndRouter('logInPath')}
					component={ props =>
						<EmptyLayout>
							<SessionNew {...props} />
						</EmptyLayout>
					}
				/>
				<PublicRoute

					exact
					path='/en/fileupload'
					component={ props =>
						<EmptyLayout>
							<FileUpload {...props} />
						</EmptyLayout>
					}
				/>
				<PublicRoute
					exact
					path={frontEndRouter('homePath')}
					component={props =>
						<MainLayout classes={["home"]}>
							<Home />
						</MainLayout>
					}
				/>
				
				<PublicRoute
					exact
					path={frontEndRouter('articlesPath')}
					component={ props =>
						<MainLayout>
							<ArticleIndex {...props} />
						</MainLayout>
					}
				/>
				<PublicRoute
					exact
					path={frontEndRouter('articlePathWithArguments')}
					component={ props =>
						<MainLayout>
							<ArticleShow {...props} />
						</MainLayout>
					}
				/>
				<PublicRoute
					exact
					path={frontEndRouter('rootPath')}
					component={props =>
						<EmptyLayout>
							<LanguageSelector />
						</EmptyLayout>
					}
				/>
				<PublicRoute
					exact
					path={frontEndRouter('newQuotePath')}
					component={props =>
						<MainLayout>
							<QuoteNew {...props}/>
						</MainLayout>
					}
				/>
				<PublicRoute
					path={frontEndRouter('forSalePathWithArguments')}
					component={props => 
						<MainLayout>
							<PropertyIndex {...props}/>
						</MainLayout>
					}
				/>
				<PublicRoute
					path={frontEndRouter('forRentPathWithArguments')}
					component={props => 
						<MainLayout>
							<PropertyIndex {...props}/>
						</MainLayout>
					}
				/>
				<PublicRoute
					path={frontEndRouter('seasonPathWithArguments')}
					component={props => 
						<MainLayout>
							<PropertyIndex {...props}/>
						</MainLayout>
					}
				/>
				<PublicRoute
					exact
					path={frontEndRouter('propertyPathWithArguments')}
					component={props => 
						<MainLayout>
							<PropertyShow {...props}/>
						</MainLayout>
					}
				/>
		</ConnectedSwitch>
		);		
	}
}

export default PublicRoutes;
