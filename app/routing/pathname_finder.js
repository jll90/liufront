import { routesWithLang } from './routes';

export const pathnameFinder = (lang, route) => {
	const universalPathname = findPathnameKeyInRoutes(lang, route);
	return universalPathname;
}

const getNestedRoutes = lang => {
	return routesWithLang[lang];
}

const findPathnameKeyInRoutes = (lang, route) => {
	const nestedRoutes = getNestedRoutes(lang);

	for (let key in nestedRoutes){
		if (nestedRoutes[key] === route){
			return key;
		}
	}

	throw ("Could not match route to pathname in routes");
}
