export const routesWithLang = {
	rootPath: "/",
	es: {
		homePath: "/es",
		newQuotePath: "/solicitudes/nuevo",
		quotesPath: "/solicitudes",

		dashboardPath: "/panel_de_control",
		emailDashboardPath: "/central_de_correos",

		logInPath: "/iniciar_sesion",

		newPropertyPath: "/nueva_propiedad",
		propertyPath: function (config){
			return "/propiedades/"+ config.id;
		},
		privatePropertyPath: function(config){
			return "/propiedades/" + config.id + "/perfil_privado";
		},
		editPropertyPath: function(config){
			return "/propiedades/" + config.id + "/editar";	
		},
		photosPrivatePropertyPath: function(config){
			return "/propiedades/" + config.id + "/fotos";
		},
		publicationsPrivatePropertyPath: function(config){
			return "/propiedades/" + config.id + "/publicaciones";
		},
		clientsPrivatePropertyPath: function(config){
			return "/propiedades/" + config.id + "/clientes";
		},
		statsPrivatePropertyPath: function(config){
			return "/propiedades/" + config.id + "/estadisticas";
		},
		newClientPath: "/clientes/nuevo",
		clientsPath: "/clientes",

		servicesPath: "/servicios",

		propertiesPath: "/propiedades",
		propertyPathWithArguments: "/propiedades/:propertyId",
		editPropertyPathWithArguments: "/propiedades/:propertyId/editar",
		privatePropertyPathWithArguments: "/propiedades/:propertyId/perfil_privado",
		photosPrivatePropertyPathWithArguments: "/propiedades/:propertyId/fotos",
		publicationsPrivatePropertyPathWithArguments: "/propiedades/:propertyId/publicaciones",	
		statsPrivatePropertyPathWithArguments: "/propiedades/:propertyId/estadisticas",	

		apartmentsForSalePath: "/venta/departamento",
		housesForSalePath: "/venta/casa",
		officesForSalePath: "/venta/oficina",
		commercialForSalePath: "/venta/comercial",
		forSalePathWithArguments: "/venta/:propertyType",

		apartmentsForRentPath: "/arriendo/departamento",
		housesForRentPath: "/arriendo/casa",
		officesForRentPath: "/arriendo/oficina",
		commercialForRentPath: "/arriendo/comercial",
		forRentPathWithArguments: "/arriendo/:propertyType",

		apartmentsSeasonPath: "/temporada/departamento",
		housesSeasonPath: "/temporada/casa",
		commercialSeasonPath: "/temporada/comercial",
		seasonPathWithArguments: "/venta/:propertyType",

		publicationsPath: "/publicaciones",
		publicationPath: function(config){
			return "/publicaciones/" + config.id;	
		},

		newArticlePath: "/nuevo_articulo",
		articlesPath: "/blog",
		articlePathWithArguments: "/blog/:id",
		articlePath: function(config){
			return "/blog/" + config.id;	
		},

		inboxPath: "/inbox",
		usersPath: "/usuarios",
	},
	en: {
		homePath: "/en",
		newQuotePath: "/quotes/new",
		quotesPath: "/quotes",

		dashboardPath: "/main_dashboard",
		emailDashboardPath: "/mailer_dashboard",

		logInPath: "/login",

		newPropertyPath: "/new_property",
		propertyPath: function (config){
			return "/properties/"+ config.id;
		},
		privatePropertyPath: function(config){
			return "/properties/" + confid.id + "/private_profile";
		},
		editPropertyPath: function(config){
			return "/properties/" + config.id + "/edit";	
		},

		newClientPath: "/clients/new",
		clientsPath: "/clients",

		propertiesPath: "/properties",
		propertyPathWithArguments: "/properties/:propertyId",
		editPropertyPathWithArguments: "/properties/:propertyId/edit",
		privatePropertyPathWithArguments: "/properties/:propertyId/private_profile",
		photosPrivatePropertyPathWithArguments: "/properties/:propertyId/photos",	

		apartmentsForSalePath: "/sale/apartment",
		housesForSalePath: "/sale/house",
		commercialForSalePath: "/sale/commercial",
		officesForSalePath: "/sale/office",
		forSalePathWithArguments: "/sale/:propertyType",

		apartmentsForRentPath: "/rent/apartment",
		housesForRentPath: "/rent/house",
		officesForRentPath: "/rent/office",
		commercialForRentPath: "/rent/commercial",
		forRentPathWithArguments: "/rent/:propertyType",

		apartmentsSeasonPath: "/season/apartment",
		housesSeasonPath: "/season/house",
		commercialSeasonPath: "/season/commercial",
		seasonPathWithArguments: "/sale/:propertyType",

		publicationsPath: "/publications",
		publicationPath: function(config){
			return "/publications/" + config.id;	
		},

		newArticlePath: "/new_article",
		articlesPath: "/blog",
		articlePathWithArguments: "/blog/:id",
		articlePath: function(config){
			return "/blog/" + config.id;	
		},

		inboxPath: "/inbox",
		usersPath: "/users",
	}
}

const routes = (lang, pathname, config) => {
	const baseLang = (lang === null || lang === undefined) ? 'es' : lang;
	if (pathname === 'rootPath'){
		return routesWithLang['rootPath'];
	}

	if (pathname === 'homePath'){
		return routesWithLang[baseLang]['homePath'];
	}
	
	let route = routesWithLang[baseLang][pathname];
	if (typeof route === "function"){
		route = route(config);
	} 
	
	route = ("/" + baseLang + route);
	return route;
};

export default routes;
