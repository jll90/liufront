import React from 'react';
import { render } from 'react-dom';

import { Switch } from 'react-router-dom';

import { connect } from 'react-redux';

import { PrivateRoute } from './private-route.jsx';

import PrivatePropertyShow from '../views/properties/private_show.jsx';
import PropertyNewEdit from '../views/properties/new_edit.jsx';
import PropertyStats from '../views/properties/stats.jsx';

import EmailDashboard from '../views/email_dashboard.jsx';
import MainDashboard from '../views/main_dashboard.jsx'

import ClientIndex from '../views/clients/index.jsx';
import ClientNew from '../views/clients/new.jsx';

import PublicationIndex from '../views/publications/index.jsx';
import PrivatePropertiesIndex from '../views/properties/private_index.jsx';

import QuoteIndex from '../views/quotes/index.jsx';
import ArticleNew from '../views/articles/new.jsx';

import PhotoIndex from '../views/photos/index.jsx';

import { frontEndRouter } from './router.js';

import { PrivateLayout, PropertyProfileLayout } from '../views/layouts/layouts.jsx';

const ConnectedSwitch = connect(state => ({location: state.routerReducer.location }))(Switch);

class PrivateRoutes extends React.Component {

	render(){
		return (
			<ConnectedSwitch>
				<PrivateRoute
					exact
					path={frontEndRouter('statsPrivatePropertyPathWithArguments', {})}
					component={props =>
						<PropertyProfileLayout>
							<PropertyStats {...props} />
						</PropertyProfileLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('publicationsPrivatePropertyPathWithArguments', {})}
					component={props =>
						<PropertyProfileLayout>
							<PublicationIndex {...props} />
						</PropertyProfileLayout>
					}
				/>

				<PrivateRoute
					exact
					path={frontEndRouter('photosPrivatePropertyPathWithArguments', {})}
					component={props =>
						<PropertyProfileLayout>
							<PhotoIndex {...props} />
						</PropertyProfileLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('propertiesPath', {})}
					component={props =>
						<PrivateLayout>
							<PrivatePropertiesIndex {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('privatePropertyPathWithArguments')}
					component={props => 
						<PropertyProfileLayout>
							<PrivatePropertyShow {...props} />
						</PropertyProfileLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('editPropertyPathWithArguments')}
					component={props => 
						<PropertyProfileLayout>
							<PropertyNewEdit {...props} />
						</PropertyProfileLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('newPropertyPath')}
					component={props => 
						<PrivateLayout>
							<PropertyNewEdit {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('dashboardPath')}
					component={props => 
						<PrivateLayout>
							<MainDashboard {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('clientsPath')}
					component={props => 
						<PrivateLayout>
							<ClientIndex {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('newClientPath')}
					component={props => 
						<PrivateLayout>
							<ClientNew {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('emailDashboardPath')}
					component={props => 
						<PrivateLayout>
							<EmailDashboard {...props} />
						</PrivateLayout>
					}
				/>
				<PrivateRoute
					exact
					path={frontEndRouter('quotesPath')}
					component={props => 
						<PrivateLayout>
							<QuoteIndex {...props} />
						</PrivateLayout>
					}
				/>
				
				<PrivateRoute
					exact
					path={frontEndRouter('newArticlePath')}
					component={props => 
						<PrivateLayout>
							<ArticleNew {...props} />
						</PrivateLayout>
					}
				/>
			</ConnectedSwitch>
		);
	}
}

export default PrivateRoutes;
