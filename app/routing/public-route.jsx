import React from 'react';
import { render } from 'react-dom';

import { Route } from 'react-router-dom';

export const PublicRoute = ({ component: Component, ...rest}) => {	
	return (
		<Route 
			{...rest}
			render={props => 
				<Component {...props} />
			}
		/>
	);
}
