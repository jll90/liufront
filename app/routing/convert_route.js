import { routes } from './routes.js';
import { pathnameFinder } from './pathname_finder.js';

import { frontEndRouter } from './router.js';

export const convertRoute = (route, newLang, resource) => {
	const lang = route.split("/")[1];

	const IS_HOME_PATH = (["/es", "/en"].indexOf(route) > -1);

	if (IS_HOME_PATH){
		return "/" + newLang;
	}

	const propertyPathRegex = /^\/{1}(es|en)\/{1}(propiedades|properties)\/(\d+|[A-za-z\-0-9]+)\/?$/gi;

	const IS_PROPERTY_PATH = route.match(propertyPathRegex);

	if (IS_PROPERTY_PATH){
		let newRoute = route;
		let newRouteSegments = newRoute.split("/");

		newRouteSegments[1] = newLang;
		newRouteSegments[2] = ( lang === "en" ) ? "propiedades" : "properties"; 
		newRouteSegments[3] = resource.id;

		newRoute = newRouteSegments.join("/");

		return newRoute;
	}	
	let trimmedRoute = route.split("/");
	trimmedRoute.shift();
	trimmedRoute.shift();

	trimmedRoute = "/" + trimmedRoute.join("/");

	const pathname = pathnameFinder(lang, trimmedRoute);

	return frontEndRouter(pathname, null, newLang);
}
