import routes from './routes.js';
import { currentLanguage } from '../helpers/i18n.js';

export function frontEndRouter(pathname, config, lang = (currentLanguage()) ){
	let route = routes(lang, pathname, config);
	return route;	
}
