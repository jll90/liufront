var storage = localStorage;

export function setItem(key, val){
	storage.setItem(key, val);
}

export function getItem(key){
	return storage.getItem(key);
}

export function removeItem(key){
	storage.removeItem(key)
}
