import { setItem, getItem, removeItem } from './storage_provider.js';

const TOKEN = "token";

export function isAuthenticated(){
	return (getItem(TOKEN) !== null);
}

export function saveToken(token){
	setItem(TOKEN, token);
}

export function getToken(){
 	return getItem(TOKEN);
}

export function destroyToken(){
	removeItem(TOKEN);
}
