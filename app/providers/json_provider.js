import axios from 'axios';

import { jsonCamelCaseToSnakeCase, jsonSnakeCaseToCamelCase } from '../../shared/tools/object_manipulation.js';

let jsonProvider = {};

function authorizationHeaders(token){
	return { Authorization: token };
}

function isValidMethod(method){
	var validMethodsRegexp = /GET|POST|PATCH|PUT|DELETE/gi;
	return validMethodsRegexp.test(method);
}

function buildConfig(c, method){

	if (!c.hasOwnProperty("url")){
		throw("A url is required");
	}

	if (!isValidMethod(method)){
		throw("A valid method is required");
	}

	return {
		token: c.token,
		url: c.url,
		data: c.hasOwnProperty("data") ? c.data : null,
		postProcessor: c.hasOwnProperty("postProcessor") ? c.postProcessor : function (d) { return d}, 
		method: method,
		noHeaders: c.hasOwnProperty("noHeaders") ? c.noHeaders : false,
		onUploadProgress: c.hasOwnProperty("onUploadProgress") ? c.onUploadProgress : null,
		noPreProcessing: c.hasOwnProperty("noPreProcessing") ? c.noPreProcessing : false,
		noData: c.hasOwnProperty("noData") ? c.noData: null
	}
}

function get(config){
	const ajaxCallConfig = buildConfig(config, "GET");

	return	ajaxCall(ajaxCallConfig);	
}

function patch(config){
	const ajaxCallConfig = buildConfig(config, "PATCH");

	return ajaxCall(ajaxCallConfig);
}

function post(config){
	const ajaxCallConfig = buildConfig(config, "POST");

	return ajaxCall(ajaxCallConfig);
}

//del acts as alias for delete which is a reserved word
function del(config){	
	const ajaxCallConfig = buildConfig(config, "DELETE");

	return ajaxCall(ajaxCallConfig);
}

function ajaxCall(config){
	const { url, method, noHeaders, postProcessor, onUploadProgress, noPreProcessing, noData, token } = config;

	let data = config.data;
	data = (noPreProcessing === true) ? data : jsonCamelCaseToSnakeCase(data); /*changes case for RoR */

	let headers = null;
	
	if ( token ){
		headers = authorizationHeaders(token);
	} else {
		/* Hack for google services */
		if ( noHeaders === true ){
			delete axios.defaults.headers.common["Content-Type"];
		}
	}
	if (noData === true){
		data = null;
	} /* Hack for google services */
	

	return axios({
		url: url,
		method: method,
		headers: headers,
		data: data, 
		onUploadProgress
	})
	.then((res, err) => {
		/* Changes ruby case to JS case */
		const formattedRes = jsonSnakeCaseToCamelCase(res.data);
		return Promise.resolve(postProcessor(formattedRes));
	})	
	.catch(err => {
		return Promise.reject(err);
	});
}

jsonProvider = {
	get: get,
	patch: patch,	
	del: del,
	post: post
}

export default jsonProvider;
