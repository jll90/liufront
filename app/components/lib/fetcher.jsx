import React from 'react'
import { render } from 'react-dom';

import DefaultLoading from '../misc/default_loading.jsx';

function withFetcher (DataRenderer){
	return class Fetcher extends React.Component {

		constructor(){
			super();
			this.state = {
				data: null,
				dataHasLoaded: false,
				dataLoading: true
			}
		}

		componentWillMount(){
			this.loadData();
		}	

		loadData = () => {

			const successCb = this.props.successCb ? this.props.successCb : () => {};
			const errorCb = this.props.errorCb ? this.props.errorCb : () => {};
			const params = this.props.params ? this.props.params : {};
			const self = this;
			const successFn = (data) => {
				self.setData(data);
				self.setDataLoaded();
				successCb();
			}
			const errorFn = (err) => {
				console.error(err);
				errorCb();
			}

			const config = {
				successFn, errorFn, ...params
			}

			this.props.loadFn(config);
		}

		setData = (data) => {
			this.setState({data});
		}

		setDataLoaded = () => {
			this.setState(
				{
					dataHasLoaded: true,
					dataLoading: false
				}
			);
		}

		isThereData = (data) => {
			if (data.constructor === Array){
				return (data.length > 0);
			}

			return !(Object.keys(data).length === 0 && data.constructor === Object);
		}

		render(){
			const { data, dataHasLoaded, dataLoading } = this.state;
			if ( dataLoading === true ){
				return (<DefaultLoading />);
			} else {
				if ( this.isThereData(data) ){
					return (
						<DataRenderer data={data} />
					);
				} else {
					return <div>No hay datos para cargar</div>;
				}
			}
		}
	}
}

export default withFetcher;
