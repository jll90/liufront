import React from 'react';
import { render } from 'react-dom';

/* This component needs a successFn and a errorFn
for the ajax callbacks. It also needs the data
that will be submit. It doesn't care how the data is submit. */

/* It works with the architecture of defined services
	The config object needs:
		> data
	It optionally can be passed:
		> successFn
		> errorFn
 */

const withSubmit = (Component, submitFn) => {
	return class Submitable extends React.Component {
		constructor(){
			super();
			this.state = {
				submitting: false,
				success: false,
				error: false
			}
		}

		submit = (data) => {

			const successFn = () => {
				let msg = this.props.successMsg
				if (msg){
		
				} 		
			}

			const errorFn = () => {
		
			}

			const config = { 
				successFn,
				errorFn,
				data
			}

			submitFn(config);
		}
	
		render(){
			const props = this.props;
			return (
				<Component submit={this.submit} {...props}/>
			)
		}
	}
}

export default withSubmit;
