import React from 'react';
import { render } from 'react-dom';

import { toTitleCase, toUpperCase } from '../../../shared/tools/string_manipulation.js';

import StaticTranslate from '../../../shared/components/static_translate.jsx';

const TextTransform = props => {
	const { txt, toUpperCase, toTitleCase } = props;
	let formattedTxt = null;
	if (toUpperCase){
		formattedTxt = toUpperCase(txt);
	}

	if (toTitleCase){
		formattedTxt = toTitleCase(txt);
	}

	return formattedTxt;
}

export default TextTransform;
