import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';

/* Component built to work with redux */

class BasicAlert extends React.Component {
	render(){
		const { shouldRender, message, mode} = this.props;
		const classes = ["alert-container", mode].join(" ");
		if ( shouldRender ){ 
			return (
				<div className={classes}>
					{ message }
				</div>
			);
		}
		return null;
	}
}

const mapStateToProps = state => {
	const { shouldRender, message, mode } = state.alertReducer;
	return {
		shouldRender, message, mode
	}
}

const Alert = connect(mapStateToProps)(BasicAlert);

export default Alert;
