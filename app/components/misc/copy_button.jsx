import React from 'react';
import { render } from 'react-dom';

import CopyToClipboard from 'react-copy-to-clipboard';

class CopyButton extends React.Component {
	render(){
		const text = this.props.text;
		return (
			<CopyToClipboard text={text} >
				{this.props.children}	
			</CopyToClipboard>
		);
	}
}

export default class CopyButtonWithIcon extends React.Component {
	render(){
		const text = this.props.text;
		return (
			<CopyButton text={text} >
				<div className="copy-btn">
					<i className="fa fa-clipboard" aria-hidden="true"></i> 
				</div>
			</CopyButton>
		);	
	}
}
