import React from 'react';
import { render } from 'react-dom';

class WrappedSelect extends React.Component {
	onSelect = (e) => {
		const { name, value } = e.target;
		this.props.onSelect(value);
	}

	labelFn = (item) => {
		return this.props.labelFn(item); 
	}

	valueFn = (item) => {
		return this.props.valueFn(item);
	}

	render(){
		const { name, value, items, defaultTxt } = this.props;
		return (
			<select onChange={this.onSelect} value={value}>
				<option key={-1} value={-1}>{defaultTxt}</option>
				{
					items.map((item, j) => {
						return (<option key={j} value={this.valueFn(item)}>{this.labelFn(item)}</option>);
					})
				}
			</select>
		);
	}
}

export default WrappedSelect;
