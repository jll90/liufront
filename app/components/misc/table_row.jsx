import React from 'react';
import { render } from 'react-dom';

class TableRow extends React.Component {
	render(){
		const { rowHeading, rowContent, rowMenu } = this.props;
		return (
			<div className="row expanded">
				<div className="small-2 columns">
					{rowHeading}
				</div>
				<div className="small-9 columns">
					{rowContent}
				</div>
				<div className="small-1 columns">
					{rowMenu}
				</div>
			</div>
		);
	}
}

export default TableRow;
