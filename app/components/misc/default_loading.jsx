import React from 'react';
import { render } from 'react-dom';

class DefaultLoading extends React.Component {
	render(){
		return (<div>Cargando...</div>);
	}
}

export default DefaultLoading;
