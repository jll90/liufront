import React from 'react';
import { render } from 'react-dom';

import { T } from '../../helpers/i18n.js';

import { connect } from 'react-redux';

const Translate = props => {
	const { lang, dictKey } = props;
	const txt = T(lang, dictKey);
	return <span>{txt}</span>;
}

const mapStateToProps = state => {
	return { lang: state.langReducer.lang }
}

const StaticTranslate = connect(mapStateToProps)(Translate);

export default StaticTranslate;
