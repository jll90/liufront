import React from 'react';
import { render } from 'react-dom';

import Client from './client.jsx';

class ClientList extends React.Component {

	render(){
		const clients = this.props.clients;
		return (
			<div className="client-list">
				{
					clients.map((client) => {
						return (<Client key={client.id} client={client} />);
					})
				}
			</div>
		);
	}
}

export default ClientList;
