import React from 'react';
import { render } from 'react-dom';

import { clientStatusToText } from '../../../shared/tools/dictionaries.js';

class Client extends React.Component {
	render(){

		const {
			fullname,
			phoneNumber,
			rut,
			email,
			id,
			wechat,
			whatsapp,
			description,
		} = this.props.client;


		return (
			<div className="client">
				<div><span>Nombre:</span> {fullname}</div>
				<div><span>Identificador:</span> {id}</div>
				<div><span>Telefono:</span> {phoneNumber}</div>
				<div><span>Rut:</span> {rut}</div>
				<div><span>Email:</span> {email}</div>
				<div><span>Wechat:</span> {wechat}</div>
				<div><span>Whatsapp:</span> {whatsapp}</div>
				<div><span>Anotaciones:</span> {description}</div>
			</div>
		)
	}
}

export default Client;
