import React from 'react';
import { render } from 'react-dom';

import { create } from '../../../shared/services/clients';

import LabelSelector from '../labels/label_selector.jsx';

import Tags from '../../../shared/constants/tags.js';

const createClient = create;

const initState = {
	fullname: '',
	email: '',
	rut: '',
	phoneNumber: '',
	description: '',
	wechat: '',
}

const cleanState = initState;

class ClientNew extends React.Component {

	constructor(){
		super();
		this.state = initState;
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	submitForm = (e) => {
		e.preventDefault();
		const data = this.state;
		const successFn = this.successCallback;

		const config = { 
			successFn, data
		}
		createClient(config);
	}


	clearAllFields = () => {
		this.setState(cleanState);
	}

	successCallback = () => {
		this.clearAllFields();
	}

	render (){
		const {
			fullname,
			email,
			rut,
			phoneNumber,
			description,
			wechat,
		} = this.state;	

		const clientTags = Tags;

		return (
				<div> 
					<form className="row columns" onSubmit={this.submitForm}>
						<input type="text" placeholder="Nombre Completo" name="fullname" onChange={this.handleChange} value={fullname}/>
						<input type="text" placeholder="Email" name="email" onChange={this.handleChange} value={email}/>
						<input type="text" placeholder="Rut" name="rut" onChange={this.handleChange} value={rut}/>
						<input type="text" placeholder="Telefono" name="phoneNumber" onChange={this.handleChange} value={phoneNumber}/>
						<input type="text" placeholder="Wechat" name="wechat" onChange={this.handleChange} value={wechat}/>
						<textarea placeholder="Descripcion" name="description" onChange={this.handleChange} value={description} />
						<p>Agregar Etiquetas:</p>
						<LabelSelector 
							unselectedItems={clientTags}
							selectedItems={[]}
						/>
						<input type="submit" className="button expand" value="Ingresar"/>
					</form>
				</div>
		);
	}
}


export default ClientNew;
