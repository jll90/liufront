import React from 'react';
import { render } from 'react-dom';

import TimePicker from 'rc-time-picker';

import DatePicker from 'react-datepicker';

import moment from 'moment';

class VisitForm extends React.Component {

	constructor(){
		super();
		this.state = {
			dateMoment: null,
			timeMoment: null,
			day: null,
			month: null,
			year: null,
			min: null,
			hr: null,
			timestamp: null,
			comment: '',
		};
	}

	parseDate = () => {
		const dMoment = this.state.dateMoment;
		const day = dMoment.date();
		const month = dMoment.month() + 1;
		const year = dMoment.year();
		this.setState({ day, month, year });
	}

	parseTime = () => {
		const tMoment = this.state.timeMoment;
		const min = tMoment.minute();
		const hr = tMoment.hour();
		this.setState({ min, hr }, this.buildTimestamp);
	}

	buildTimestamp = () => {
		const { year, month, day, min, hr } = this.state;
		const str = `${year}-${month}-${day} ${hr}:${min}`;
		const timestamp = moment(str, "YYYY-MM-DD HH:mm").valueOf();
		this.setState({ timestamp });
	}

	handleDateChange = (dateMoment) => {
		this.setState({ dateMoment }, this.parseDate);
	}

	handleTimeChange = (timeMoment) => {
		this.setState({ timeMoment }, this.parseTime);
	}

	handleChange = (e) => {
		const { value, name } = e.target;
		this.setState({[name]: value});
	}

	onSubmit = (e) => {
		e.preventDefault();
		const data = {
			timestamp: this.state.timestamp,
			propertyId: this.props.propertyId,
			comment: this.state.comment
		}
		this.props.submit(data);	
	}

	render(){
		const { dateMoment, timeMoment } = this.state;
		return (
			<form onSubmit={this.onSubmit}>
				<div>
					<p>Seleccionar Fecha</p>
					<DatePicker
						selected={dateMoment}
						onChange={this.handleDateChange}
					/>
				</div>
				<div>
					<p>Seleccionar Hora (no cerrar con la X)</p>
					<TimePicker
						showSecond={false}
						value={timeMoment}
						onChange={this.handleTimeChange} 
					/>
				</div>
				<div>
					<p>Escribir comentario</p>
					<textarea rows="4" onChange={this.handleChange} name="comment" />
				</div>
				<div>
					<button type="submit" className="button">
						Enviar
					</button>
				</div>
			</form>	
		);
	}
}

export default VisitForm;
