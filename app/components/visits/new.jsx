import React from 'react';
import { render } from 'react-dom';

import withSubmit from '../lib/submitable.jsx';

import VisitForm from './form.jsx';

import { create } from '../../../shared/services/visits.js';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

const VisitNewForm = withSubmit(VisitForm, create);

class VisitNew extends React.Component {
	render(){
		return (
			<div>
				<h3>Crear Visita</h3>
				<VisitNewForm 
					propertyId={getPermalinkFromRoute()}
				/>
			</div>
		);
	}
}

export default VisitNew;
