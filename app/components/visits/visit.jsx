import React from 'react';
import { render } from 'react-dom';

import moment from 'moment';

import 'moment/locale/es';

moment.locale('es');

class Visit extends React.Component {
	render(){
		const { timestamp, comment } = this.props.visit;
		return (
			<div>
				<div>Hora: {moment(timestamp).format("LLLL")}</div>
				<div>Comment: {comment}</div>
			</div>
		);
	}
}

export default Visit;
