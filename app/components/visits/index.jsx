import React from 'react';
import { render } from 'react-dom';

import withFetcher from '../lib/fetcher.jsx';

import VisitList from './list.jsx';

import { indexByProperty } from '../../../shared/services/visits.js';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

const IndexRenderer = withFetcher(VisitList);

class VisitIndex extends React.Component {
	render(){
		return (
			<IndexRenderer 
				loadFn={indexByProperty}
				params={{id: getPermalinkFromRoute()}}
			/>
		);
	}
}

export default VisitIndex;
