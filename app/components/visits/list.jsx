import React from 'react';
import { render } from 'react-dom';

import Visit from './visit.jsx';

class VisitList extends React.Component {
	render(){
		const visits = this.props.data;
		return (
			<div>
				{
					visits.map((v, i) => {
						return ( <Visit key={i} visit={v} /> );
					})
				}
			</div>
		);
	}
}

export default VisitList;
