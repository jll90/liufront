import React from 'react';
import { render } from 'react-dom';

import WrappedSelect from '../../misc/wrapped_select.jsx';

/* 
This component as a proxy between
WrappedSelect and withSplitArray so
elements can be removed an added to the
other array
*/

class SelectAsDelete extends React.Component {
	render(){
		const onSelect = this.props.onEvent;
		const items = this.props.items;
		return (
			<WrappedSelect 
				onSelect={onSelect}
				valueFn={(el) => {return el}}
				labelFn={(el) => {return el}}
				items={items}
				name={undefined}
				value={-1}
				defaultTxt={"Seleccionar etiqueta"}
			/>
		)	
	}
}

export default SelectAsDelete;
