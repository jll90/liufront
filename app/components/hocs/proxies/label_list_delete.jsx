import React from 'react';
import { render } from 'react-dom';

import { LabelList } from '../../labels/labels.jsx';

/*
Proxy component between withSplitArray 
& LabelList
*/

class LabelListDelete extends React.Component {
	render(){
		const items = this.props.items;
		const onClick = this.props.onEvent;
		return (
		  <LabelList 
				onClick={onClick}
				items={items}
			/>	
		)
	}
}

export default LabelListDelete;
