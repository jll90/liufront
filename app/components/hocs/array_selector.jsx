import React from 'react';
import { render } from 'react-dom';

import { cutToNewArray, copyArray } from '../../../shared/lib/arrays.js';

export function withArraySelector(Comp1, Comp2){
	return class ArraySelector extends React.Component {
		constructor(props){
			super(props);
			this.state = {
				unselected: this.props.unselectedItems, 
				selected: this.props.selectedItems
			}
		}

		getUnselected = () => {
			return copyArray(this.state.unselected);
		}

		setUnselected = (unselected) => {
			this.setState({unselected});
		}

		getSelected = () => {
			return copyArray(this.state.selected);
		}

		setSelected = (selected) => {
			this.setState({selected});
		}

		selectElement = (el) => {
			const result = cutToNewArray(
				this.getUnselected(),
				this.getSelected(),
				el
			);
			const unselected = result[0];
			const selected = result[1];
			
			this.setUnselected(unselected);
			this.setSelected(selected);
		}

		unselectElement = (el) => {
			console.log(el);
			const result = cutToNewArray(
				this.getSelected(),
				this.getUnselected(),
				el
			);
			const selected = result[0];
			const unselected = result[1];
			
			this.setUnselected(unselected);
			this.setSelected(selected);
		}

		render(){
			const { unselected, selected } = this.state;
			return (
				<div>
					<Comp1 items={unselected} onEvent={this.selectElement} />
					<Comp2 items={selected} onEvent={this.unselectElement} />
				</div>
			)
		}
	}
}
