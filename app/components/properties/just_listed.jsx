import React from 'react';
import { render } from 'react-dom';

import { createStyleObject } from '../../../shared/helpers/photos.js';

import Price from '../../views/partials/price.jsx';
import CustomDate from '../../views/partials/custom_date.jsx';

import { currentLanguage, T } from '../../helpers/i18n.js';

import { toUpperCase } from '../../../shared/tools/string_manipulation.js';

import { isSale } from '../../../shared/helpers/properties.js';

import { Link } from 'react-router-dom';

import { frontEndRouter } from '../../routing/router.js';

import ServerTranslate from '../../../shared/components/server_translate.jsx';
import StaticTranslate from '../../components/misc/static_translate.jsx';

import { DEFAULT_PHOTO_URL } from '../../../shared/config/photos.js';

export class JustListed extends React.Component {

	render(){
		const { properties } = this.props; 
		return (
			<div>
				{	
					properties.map((p)=> {
						return (<ListingPreview key={p.id} property={p} />);
					})
				}
			</div>
		);
	}
}

class ListingPreview extends React.Component {
	render(){
		const { property } = this.props;
		const { operationType, permalink, id, photos } = property;
		let mainPhotoUrl = null;
		if (photos.length > 0){
			mainPhotoUrl = photos[0].url;
		} else {
			mainPhotoUrl = DEFAULT_PHOTO_URL;
		}
		const style = createStyleObject(mainPhotoUrl);
		let operationTxt = null;

		const approximateLocation = property.approximateLocation;
		style.height = "400px";
		return (
			<div className="small-12 medium-6 columns no-padding preview-container">
				<Link className="listing-preview" style={style} to={frontEndRouter('propertyPath', {id})}>
					<div className="summary row">
						<div className="small-8 columns">
							<div className="operation-type">{isSale(operationType) ? <StaticTranslate dictKey={'operationAsNoun.sale'} /> : <StaticTranslate dictKey={'operationAsNoun.rent'} /> }</div>
							<div className="address">
								<ServerTranslate 
									srcTxt={approximateLocation} 
								/>
							</div>
							<div className="listed-date">
								<StaticTranslate dictKey={'date.listed'} />{": "}
								<CustomDate timestamp={property.createdAt} />
							</div>
						</div>
						<div className="small-4 columns">
							<Price property={property} />
						</div>
					</div>
				</Link>
			</div>
		);
	}
}
