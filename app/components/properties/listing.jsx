import React from 'react';
import { render } from 'react-dom';

import { currentLanguage } from '../../helpers/i18n.js';

import { Link } from 'react-router-dom';

import { frontEndRouter } from '../../routing/router.js';

import ServerTranslate from '../../../shared/components/server_translate.jsx';

import ApproximateLocation from './approximate_location.jsx';

import SpaceConfiguration from './space_configuration.jsx';

import Price from '../../views/partials/price.jsx';

import StaticTranslate from '../misc/static_translate.jsx';

import CustomDate from '../../views/partials/custom_date.jsx';

import { DEFAULT_PHOTO_URL } from '../../../shared/config/photos.js';

class PropertyListing extends React.Component {
	
	render (){
		const property = this.props.property;

		const {
			id, 
			photos, 
			propertyType, 
			operationType, 
			permalink,
			city, 
			approximateLocation,
			title, 
			noComission,
			createdAt,
			county,
			price, 
			uf,
			bedrooms,
			bathrooms,
			numberOfGuests,
			singleSpace,
			numberOfUnits,
      		state,
			pricingUnit
		} = property;


     let url = null;

      	if (photos.length === 0){
      		url = DEFAULT_PHOTO_URL;
      	} else {
      		url = photos[0].url;
      	}

      	const style = {
			"backgroundImage": ("url("+url+")"),
			"backgroundSize": 'contain',
			"backgroundPosition": 'center center',
			"backgroundRepeat": 'no-repeat'
		};

		const propertyIdentifier = currentLanguage() === "es" ? permalink: id;
		return (
				<div className="property columns row">
					<div className="slider-container small-12 medium-5 columns">
						<div className="slide">
							<div className="background-image" style={style} />
						</div>
					</div>
					<div className="brief-description small-12 medium-7 columns">						
						<div className="title navigation-menu">
							<Link to={frontEndRouter('propertyPath', {id: propertyIdentifier})} className="truncate">
								<ServerTranslate 
									srcTxt={title} 
								/>
							</Link>
						</div>
						<ApproximateLocation property={property} />
						<SpaceConfiguration property={property} />
						<Price property={property} />
						<div className="date">
							<StaticTranslate dictKey={'date.postedOn'}/> {" "}
							<CustomDate 
								timestamp={createdAt}
							/>
						</div>
					</div>
				</div>
			);
	}
}

export default PropertyListing;
