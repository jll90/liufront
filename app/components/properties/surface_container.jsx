import React from 'react';
import { render } from 'react-dom';

import { T, currentLanguage } from '../../helpers/i18n.js';

import {
	hasParkingSpots,
	isApartment,
	isHouse,
	isCommercial,
	isLand, 
	isOffice
} from '../../../shared/helpers/properties.js';

import { toUpperCase } from '../../../shared/tools/string_manipulation.js';

class SurfaceContainer extends React.Component {
	render () {
		const property = this.props.property;
		const propertyType = property.propertyType;
		const lang = currentLanguage();

		const totalArea = toUpperCase(T(lang, 'property.area.total_area'));
		const builtUpArea = toUpperCase(T(lang, 'property.area.built_up_area'));
		const landArea = toUpperCase(T(lang, 'property.area.land_area'));
		
		if (isApartment(propertyType) || isCommercial(propertyType)){
			return (
				<div className="fake-wrapper">
					<div className="usable-surface">{totalArea}: {property.bigSurface}m²</div>
      				<div className="rentable-surface">{builtUpArea} {property.smallSurface}m²</div>
      			</div>
			);
		}
		if (isHouse(propertyType)){
			return (
				<div className="fake-wrapper">
					<div className="rentable-surface">{landArea}: {property.bigSurface}m²</div>
					<div className="usable-surface">{builtUpArea}: {property.smallSurface}m²</div>
      			</div>
			);
		}
		if (isOffice(propertyType)){
			return <div className="rentable-surface">{totalArea}: {property.bigSurface}m²</div>			
		}
		if (isLand(propertyType)){
			return <div className="rentable-surface">{landArea}: {property.smallSurface}m²</div>			
		}
		
		return undefined;
	}
}

export default SurfaceContainer;
