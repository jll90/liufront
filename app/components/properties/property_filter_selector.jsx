import React from 'react';
import { render } from 'react-dom';

import { T, currentLanguage } from '../../helpers/i18n.js';

import { toTitleCase, toUpperCase } from '../../../shared/tools/string_manipulation.js';


class PropertyFilterSelector extends React.Component {

	tabClick = () => {
		this.props.changeMenu("code");
	}
				
	render(){
		const lang = currentLanguage();
		const { operation, propertyType, onChange, submitCb, changeMenu } = this.props;
		const findTxt = toUpperCase(T(lang, 'navigation.buttons.search'));
		const buyTxt = toUpperCase(T(lang, 'operationAsVerb.sale'));
		const rentTxt = toUpperCase(T(lang, 'operationAsVerb.rent'));
		const apartmentTxt = toUpperCase(T(lang, 'property.type.apartment'));
		const houseTxt = toUpperCase(T(lang, 'property.type.house'));
		const commercialTxt = toUpperCase(T(lang, 'property.type.commercial'));
		const officeTxt = toUpperCase(T(lang, 'property.type.office'));
		const findByCodeTxt = toUpperCase(T(lang, 'navigation.buttons.findByCode'));
			return (
				<div className="main-finder">
					<div className="find-menu operation">
						<div className="row">
							<div className="find-by-btn" onClick={this.tabClick}>{findByCodeTxt}</div>
						</div>
						<div className="row">
							<div className="small-12 medium-4 columns no-padding">
								<select onChange={onChange} value={operation} name="operation">
									<option value={0}>{buyTxt}</option>
									<option value={1}>{rentTxt}</option>
								</select>
							</div>
							<div className="small-12 medium-4 columns no-padding">
								<select onChange={onChange} value={propertyType} name="propertyType">
									<option value={0}>{apartmentTxt}</option>
									<option value={1}>{houseTxt}</option>
									<option value={2}>{officeTxt}</option>
									<option value={3}>{commercialTxt}</option>
								</select>
							</div>
							<div className="small-12 medium-4 columns no-padding">
								<button 
									className="button" 
									type="button"
									onClick={submitCb}
								>
									{findTxt}
								</button>
							</div>
						</div>
					</div>
				</div>
			);
	}
}

export default PropertyFilterSelector;
