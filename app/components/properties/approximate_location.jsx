import React from 'react';
import { render } from 'react-dom';

import ServerTranslate from '../../../shared/components/server_translate.jsx';

class ApproximateLocation extends React.Component {
	render(){
		const property = this.props.property;
		const {approximateLocation, county, city} = property;
		return (
			<div className="approximate-location">
				<span>
					<ServerTranslate 
						srcTxt={approximateLocation}
					/>
					, {county.name}, {city.name}
				</span>
			</div>
		);
	}
}

export default ApproximateLocation;
