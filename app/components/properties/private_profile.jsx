import React from 'react';
import { render } from 'react-dom';

import ProfileTable from './profile_table.jsx';

export default class PrivateProfile extends React.Component {

	render(){
		const property = this.props.property;
		return (
			<div className="property-private-profile">
				<ProfileTable property={property} />
			</div>
		)	
	}
}
