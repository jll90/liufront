import React from 'react';
import { render } from 'react-dom';

import withFetcher from '../lib/fetcher.jsx';

class StatsContainer extends React.Component {
	render(){
		const data = this.props.data;
		const { visits, calls } = data;

		return (
			<p>Stats Container</p>
		);
	}
}

const StatsRenderer = withFetcher(StatsContainer);

export default StatsRenderer;
