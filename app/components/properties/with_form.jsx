import React from 'react';
import { render } from 'react-dom';

import Form from './form.jsx';

import { Route, Redirect } from 'react-router-dom';

import { frontEndRouter } from '../../../app/routing/router.js';

import { getStateIdFromCityId, getNameFromId } from '../../../shared/helpers/places.js';

class PropertyForm extends React.Component {

	handleChange = (e) => {
		const { name, value } = e.target;
		const validNames = ["isOwner", "rightToKey", "serviceRoom", "tradeable", "singleSpace"];
		if (validNames.indexOf(name) > -1){
			this.props.updateFormData({[name]: !(value === "true")});
		} else {
			this.props.updateFormData({[name]: value});	
		}
	}

	handleCountyAtcSelect = (val, item) => {
		const countyId = item.id;
		const cityId = item.cityId;
		const stateId = getStateIdFromCityId(cityId, this.props.places.cities);
		const addressStr = this.buildAddressSuggestion(countyId, cityId);
		this.props.updateFormData({
			countyId, cityId, stateId, addressStr
		});
	}
	/* This function should be private (another file)
		because it does not belong at the same level  */
	/* Handles the suggestion to be sent to google maps */
	buildAddressSuggestion = (countyId, cityId) => {
		const { streetName, streetNumber } = this.props.data;
		const { counties, cities } = this.props.places; 
		const countyName = getNameFromId(countyId, counties);
		const cityName = getNameFromId(cityId, cities);
		const addressStr = 
		`${streetName} ${streetNumber}, ${countyName}, ${cityName}`;
		return addressStr;
	}

	formSubmit = (e) => {
		e.preventDefault();
		const data = this.props.data; 
		this.props.submitProperty({ data });
	}

	render(){
		const { submitSuccess, redirectPathname, places, buttonTxt, retrieveAddressData, reverseGeocode } = this.props;
		console.log('submit', submitSuccess);
		const data = this.props.data;
		if (submitSuccess === false){
			return (
				<Form
					{...this.props} 
					handleChange={this.handleChange}
					data={data} 
					places={places} 
					formSubmit={this.formSubmit}
					setPlacesId={this.setPlacesId}
					handleCountyAtcSelect={this.handleCountyAtcSelect}
				/>
			);
		} else {
			return (	
				<Route>
					<Redirect push to={ { pathname: redirectPathname } } />
				</Route>
			);
		}
	}
}

export default PropertyForm;  
