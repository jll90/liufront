import React from 'react';
import { render } from 'react-dom';

import inflector from '../../../shared/tools/inflector.js';

import { currentLanguage, T } from '../../helpers/i18n.js';
import { toUpperCase } from '../../../shared/tools/string_manipulation.js';

import {
	hasParkingSpots,
	isApartment,
	isHouse,
	isParking,
	isOffice
} from '../../../shared/helpers/properties.js';

class ParkingSpots extends React.Component {
	render(){
		const property = this.props.property;
    const { parkingSpots, propertyType } = property;
    const lang = currentLanguage();
    const spotsTxt = T(lang, 'property.configuration.parking_spot')
    if ( (isHouse(propertyType) || isApartment(propertyType) || isOffice(propertyType)) && hasParkingSpots(parkingSpots)){
    	return (
	   		<div className="parking-spots">{parkingSpots} {toUpperCase(inflector(parkingSpots, spotsTxt, lang))}</div>
			);	
    }
    return null;
  }
}

export default ParkingSpots;
