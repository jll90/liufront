import React from 'react';
import { render } from 'react-dom';

import { Route, Redirect } from 'react-router-dom';

import WrappedSelect from '../misc/wrapped_select.jsx';

import { frontEndRouter } from '../../routing/router.js';
 
class PropertySelector extends React.Component {

	constructor(){
		super();
		this.state = {
			propertyId: ""
		}
	}

	onSelect = (id) => {
		this.setPropertyId(id);
	}

	setPropertyId = (propertyId) => {
		this.setState({propertyId});
	}

	propertyIsSelected = () => {
		const propertyId = this.state.propertyId;
		return (propertyId.length > 0);
	}

	render(){
		const properties = this.props.data;
		const { propertyId } = this.state;

		if (this.propertyIsSelected() !== true){		
			return (
				<div className="dashboard-element">
					<p>Ir a una propiedad</p>
					<WrappedSelect
						onSelect={this.onSelect}
						items={properties}
						name={undefined}
						value={-1}
						defaultTxt={"Seleccionar Propiedad"}
						labelFn={(property) => {return(["LIU" + property.id, property.streetName, property.streetNumber, property.county.name].join(" "))}}
						valueFn={(property) => {return(property.id)}}
					/>
					<div>
						<button type="button" className="button">
							Ver Perfil
						</button>
					</div>
				</div>
			);
		} else {
			const pathname = frontEndRouter('privatePropertyPath', {id: propertyId});
			return (
				<Route>
					<Redirect push to={{pathname: pathname, state: {}}} />
				</Route>
			);
		}
	}
}

export default PropertySelector;
