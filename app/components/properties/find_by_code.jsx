import React from 'react';
import { render } from 'react-dom';

import { toTitleCase, toUpperCase } from '../../../shared/tools/string_manipulation.js';

import { currentLanguage, T } from '../../helpers/i18n.js';

import { Route, Redirect } from 'react-router-dom';

import { frontEndRouter } from '../../../app/routing/router.js';


export class FindByCode extends React.Component {
	
	findPropertyByCode = (e) => {
		e.preventDefault();
		const code = this.props.inputCode;;
		this.props.findByCode(code);
	}

	onChange = (e) => {
		const code = e.target.value;
		this.props.updateCode(code);
	}

	render(){
		const lang = currentLanguage();
		const { findPropertyByCode, onChange } = this;
		const { changeMenu, foundCode, inputCode, propertyId } = this.props;
		const findTxt = toUpperCase(T(lang, 'navigation.buttons.search'));
		const findByOperationTxt = toUpperCase(T(lang, 'navigation.buttons.findByOperation'));
		const typeInCodeTxt = toUpperCase(T(lang, 'fields.typeInCode'));
		if ( foundCode === false ){
			return (
				<form onSubmit={findPropertyByCode}>
					<div className="main-finder">
						<div className="find-menu code">
							<div className="row">
								<div className="find-by-btn" onClick={()=> {changeMenu("menu")}}>{findByOperationTxt}</div>
								</div>
							<div className="row">
								<div className="small-12 medium-8 columns no-padding">
									<input type="text" placeholder={typeInCodeTxt} value={inputCode} name="typedInCode" onChange={onChange} />
								</div>
								<div className="small-12 medium-4 columns no-padding">
									<button className="submit" type="button" onClick={findPropertyByCode} >{findTxt}</button>
								</div>
							</div>	
						</div>
					</div>
				</form>
			);
		} else {
			const propertyId = this.props.propertyId;
			this.props.resetCode(); //this triggers warning should move somewhere else
			const pathname = frontEndRouter('propertyPath', {id: propertyId });
			return (
				<Route>
					<Redirect push to={ {pathname} } />
				</Route>
			)
		}
	}
}
