import React from 'react';
import { render } from 'react-dom';

import { propertyStageToText, operationTypeToText, propertyTypeToText, stateToText } from '../../../shared/tools/dictionaries.js';

import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

import withScriptjs from "react-google-maps/lib/async/withScriptjs";

import Autocomplete from 'react-autocomplete';

import { getStateIdFromCityId, getNameFromId } from '../../../shared/helpers/places.js';

/* Anti pattern - this logic must be moved to redux */
/* Map component must be refactored */

const DEFAULT_POS = { lat: -33.4600259, lng: -70.6956273 };
const DEFAULT_ZOOM = 16;

const LocationMap = withScriptjs(
  withGoogleMap(
    props => {
    	var position = null;
    	if (parseFloat(props.position.lat)){
    		position = props.position;
    	} else {
    		position = DEFAULT_POS;
    	}
    	return (
	      <GoogleMap
	        defaultZoom={DEFAULT_ZOOM}
	        defaultCenter={DEFAULT_POS}
	        center={position}
	      >
	      	<Marker 
	      		position={position} 
	      		draggable={true} 
	      		onDragEnd={val => {
	      			const lat = val.latLng.lat();
					const lng = val.latLng.lng();
				
					props.dragEndCb(lat, lng);
					}
	      		}
	      	/>
	      </GoogleMap>
	    );
    }
    	
  )
);

export default class PropertyForm extends React.Component{

	/* Special handlers to handle different types
	of data */

	/* Most data are handled by handleChange
	passed by parent component */

	/* called with lat, lng of google maps, when
	marker is dragged */

	reverseGeocodeAddress = (lat, lng) => {
		const params = { lat, lng };
		this.props.reverseGeocode(params);	
	}

	/* called when address is to be retrieve with maps API*/
	retrieveAddressData = () => {
		const address = this.props.data.addressStr;
		this.props.retrieveAddressData({ address });
	}

	formSubmit = (e) => {
		e.preventDefault();
		this.props.formSubmit(e);
	}

	onSelect = (val, item) => {
		this.props.handleCountyAtcSelect(val, item);
	}

	render(){
		const {
			inhouseDescription,
			stage,
			streetName,
			streetNumber,
			buildingNumber,
			fullAddress,
			rolNumber,
			salesLeadBy,
			visitingHrs,
			publicationPermissions,
			publishedOn,
			operationType,
			propertyType,
			rightToKey,
			publicDescription,
			price,
			uf,
			bigSurface,
			smallSurface,
			bedrooms,
			bathrooms,
			numberOfGuests,
			singleSpace,
			storageRooms,
			parkingSpots,
			serviceRoom,
			comune,
			neighborhood,
			city,
			title,
			permalink,
			approximateLocation,
			numberOfUnits,
			tradeable,
			countyId,
			cityId,
			stateId,
			pricingUnit,
			videoUrl,
			countyStr,
			addressStr,
			latitude,
			longitude,
			fakeFullAddress
		} = this.props.data;

		const { counties, cities, states } = this.props.places;
		const { handleChange, buttonTxt } = this.props;
		return (
			<div className="first-wrapper">
				<div className="row">

					  <h2>Informacion Interna Propiedad</h2>
						<p>Activar Publicacion - Seleccionar Marketing para activar. Seleccionar Existe como registro para desactivar</p>
					  <select name="stage" onChange={handleChange} value={stage}>
					  		{
					  			[-1, 4, 7].map((index) =>
					  				<option key={index} value={index}>{propertyStageToText(index)}</option>		
					  			)
					  		}
					  </select>
					  <p>Descripcion interna - Datos para el equipo. No se muestra al publico
					  	<textarea placeholder="Descripcion privada" name="inhouseDescription" onChange={handleChange} value={inhouseDescription} rows="5"/>
					  </p>
					  <p>
					  	Calle o Camino:
					  	<input type="text" name="streetName" value={streetName} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Numeracion:
					  	<input type="text" name="streetNumber" value={streetNumber} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Numero Depto o Casa (Casas: llenar si es casa numerada en un pasaje, ej casa 3. Deptos llenar siempre):
					  	<input type="text" name="buildingNumber" value={buildingNumber} onChange={handleChange} /> 
					  </p>

            <div>
							Seleccionar Comuna, escribir solo primeras letras y seleccionar. Ciudad y Region se cargaran automaticamente.
							<br />
              <Autocomplete
								getItemValue={ item => item.name }
								items={counties}
			  				shouldItemRender={(item, value) => {
							  	if (item){
							  		return ((item.name.toLowerCase().indexOf(value.toLowerCase()) > -1) ? true : false	);
				  					}
			  					}
			  				}			 
			  				renderItem={(item, isHighlighted) =>
							    <div 
			  				    style={{ background: isHighlighted ? 'lightgray' : 'white' }}
			      				key={item.id} >
							      {item.name}
			  				  </div>
			 					 }
			  				value={countyStr}
							  onChange={handleChange}
			  				onSelect={this.onSelect}
								inputProps={ {name: 'countyStr'} }
              /> 
            </div>
					  <p>
					    Barrio o Poblacion (casi siempre se deja en blanco):
					    <input type="text" name="neighborhood" value={neighborhood} /> 
					  </p>
					  <p>
					    Comuna:
					    <input type="text" name="countyId" value={getNameFromId(countyId, counties)}  disabled/> 
					  </p>
					  <p>
					    Ciudad:
					    <input type="text" name="cityId" value={getNameFromId(cityId, cities)} disabled/> 
					  </p>
					  <p>
					    Region:
					    <input type="text" name="stateId" value={getNameFromId(stateId, states)} disabled/> 
					  </p>

					  <p>
					  	Ubicar direccion en el mapa:
					  	Ingresar direccion: Por ejemplo: San Juan de Luz 4060, Las Condes, Santiago
					  	<br />
					  	El marcador rojo representa la direccion falsa de la propiedad, que se muestra a los clientes y que en varios casos debe ser corrida
					  	para que la propiedad no sea descubierta. El marcador puede ser arrastrado haciendo click.
					  	<input type="text" name="addressStr" value={addressStr} onChange={handleChange} /> 
					  	<button type="button" className="button" onClick={this.retrieveAddressData}>Ubicar en mapa</button>
					  </p>

					   <LocationMap
						    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsJh4-kNKuyZzwcAJkFDEFMxETHHtEbfM"
						    loadingElement={
						      <div style={{ height: `400px` }}>
						        
						      </div>
						    }
						    containerElement={
						      <div style={{ height: `400px` }} />
						    }
						    mapElement={
						      <div style={{ height: `400px` }} />
						    }
						    dragEndCb={(lat, lng) => { 
						    	this.reverseGeocodeAddress(lat,lng)
						    }
								}
						    position={{lat: latitude, lng: longitude}}
						/>

						
					  <p>
					    Direccion Google maps (falsa):
					    Esta direccion se utiliza para distraer un poco al publico de la direccion real
					    <input type="text" name="fakeFullAddress" value={fakeFullAddress} disabled/> 
					  </p>
					  <p>
					    Latitud:
					    <input type="text" name="latitude" value={latitude} disabled/> 
					  </p>
					  <p>
					    Longitud:
					    <input type="text" name="longitude" value={longitude} disabled/> 
					  </p>

					  <p>
					  	Numero de Rol (SII)
					  	Formato: 00990-12343
					  	No llenar si no se sabe.
					  	<input type="text" name="rolNumber" value={rolNumber} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Captada por: (Corredor que capto la propiedad, a quien la persona le dio la propiedad)
					  	<input type="text" name="salesLeadBy" value={salesLeadBy} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Horarios de Visita: (ejemplo: flexible, despues de las 7)
					  	<input type="text" name="visitingHrs" value={visitingHrs} onChange={handleChange} /> 
					  </p>

					  <h2>Informacion Publica Propiedad</h2>
					  <p>
					  	Tipo Operacion:
						<select name="operationType" onChange={handleChange} value={operationType}>
					  		{
					  			[-1, 0, 1, 2].map((index) =>
					  				<option key={index} value={index}>{operationTypeToText(index)}</option>		
					  			)
					  		}
					  	</select>
					  </p>
					  <p>
					  	Tipo Propiedad:
						<select name="propertyType" onChange={handleChange} value={propertyType}>
					  		{
					  			[-1, 0, 1, 2, 3, 4, 5, 6, 7, 8].map((index) =>
					  				<option key={index} value={index}>{propertyTypeToText(index)}</option>		
					  			)
					  		}
					  	</select>
					  </p>
					  <p>
					  	Derecho de Llaves (solo ventas de locales comerciales - marcar en caso de que sea un derecho):
					  	<br />
						<input type="checkbox" value={rightToKey} checked={rightToKey===true} onChange={handleChange} name="rightToKey"/>
					  </p>
					  <p>Descripcion publica: (descripcion de cara a los clientes) 
					  	<textarea placeholder="Descripcion publica" name="publicDescription" onChange={handleChange} value={publicDescription} rows="5"/>
					  </p>
						<p>
							Unidad del precio:
							<select name="pricingUnit" onChange={handleChange} value={pricingUnit}>
								{
									["Seleccionar unidad", "CLP", "UF"].map((index) => {
										return (<option key={index} value={index}>{index}</option>)
									})
								}
							</select>
						</p>
					  <p>
					  	Precio en pesos (llenar si seleccionaste CLP arriba, si no en blanco):
					  	<input type="text" name="price" value={price} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Precio en UF (llenar si seleccionaste UF arriba, si no en blanco):
					  	<input type="text" name="uf" value={uf} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Superficie Mayor: (Denota superficie terreno para terrenos y sitios, y casas. Denota superficie de metros construidos para departamentos)
					  	<input type="text" name="bigSurface" value={bigSurface} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Superficie Menor: (Denota metros construidos para casas (superficie interior de la casa) y metraje util de los departamentos - sin contar terraza y logia)
					  	<input type="text" name="smallSurface" value={smallSurface} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Número de Habitaciones (residencial) o espacios (oficinas) (dejar en blanco si la propiedad no tiene o no aplica):
					  	<input type="text" name="bedrooms" value={bedrooms} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Número de Baños: (dejar en blanco si la propiedad no tiene baños)
					  	<input type="text" name="bathrooms" value={bathrooms} onChange={handleChange} /> 
					  </p>
					  <p>
					  	Número de Huéspedes: (solo arriendos por temporada, si no dejar en blanco)
					  	<input type="text" name="numberOfGuests" value={numberOfGuests} onChange={handleChange} /> 
					  </p>
					  <p>
					    Es de un ambiente: 
					    <input type="checkbox" value={singleSpace} checked={singleSpace===true} onChange={handleChange} name="singleSpace"/>
					  </p>
					  
					  <p>
					    Bodegas (dejar en blanco si no tiene):
					    <input type="text" name="storageRooms" value={storageRooms} onChange={handleChange} /> 
					  </p>
					  <p>
					    Estacionamientos (dejar en blanco si no tiene):
					    <input type="text" name="parkingSpots" value={parkingSpots} onChange={handleChange} /> 
					  </p>
					  <p>
					    Cuarto de Servicio:
					    <input type="checkbox" value={serviceRoom} checked={serviceRoom===true} onChange={handleChange} name="serviceRoom"/>
					  </p>
					  
					  <p>
					    Titulo:
					    <input type="text" name="title" value={title} onChange={handleChange} /> 
					  </p>
					  <p>
					    Permalink: identificador unico para motores de busqueda.
					    <br />
					    Ej: venta-departamento-las-condes-avenida-apoquindo-tres-dormitorios-dos-banos-estacionamiento-hermosa-vista-metro-escuela-militar
					    <input type="text" name="permalink" value={permalink} onChange={handleChange} /> 
					  </p>
					  <p>
					    Ubicacion Aproximada: (da un indicio de donde esta ubicada la propiedad)
					    <br />
					    Ejemplo: Santo Domingo con Miraflores
					    <br />
					    Ejemplo: Metro Universidad de Chile
					    <br />
					    Ejemplo: Plaza Italia
					    <input type="text" name="approximateLocation" value={approximateLocation} onChange={handleChange} /> 
					  </p>
					  <p>
					    Numero de unidades: (solo para sitios que se venden por varias unidades)
					    <input type="text" name="numberOfUnits" value={numberOfUnits} onChange={handleChange} /> 
					  </p>
					  <p>
					    Propiedad en canje:
					    <input type="checkbox" value={tradeable} checked={tradeable===true} onChange={handleChange} name="tradeable"/>
					  </p>
					  <p>
					    Enlace video 
					    <input type="text" name="videoUrl" value={videoUrl} onChange={handleChange} /> 
					  </p>
					  <p>
					  	<button type="button" className="button" onClick={this.formSubmit}>{buttonTxt}</button>
					  </p>
				</div>
			</div>
		);
	}
}
