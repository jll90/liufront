import React from 'react';
import { render } from 'react-dom';

import inflector from '../../../shared/tools/inflector.js';

import {
	hasStorageRooms,
	isApartment,
	isHouse,
	isStorage,
	isOffice
} from '../../../shared/helpers/properties.js';

import { currentLanguage, T } from '../../helpers/i18n.js';
import { toUpperCase } from '../../../shared/tools/string_manipulation.js';

class StorageRooms extends React.Component {
  render(){
	const property = this.props.property;
	const { storageRooms, propertyType } = property;
	const lang = currentLanguage();
	const storageTxt = T(lang, 'property.configuration.storage_room');
	if ( (isHouse(propertyType) || isApartment(propertyType) || isOffice(propertyType)) && hasStorageRooms(storageRooms)){
	    return (
			<div className="storage-rooms">{storageRooms} {toUpperCase(inflector(storageRooms, storageTxt, lang))}</div>
		);
	}
	return null;
	}		
}

export default StorageRooms;
