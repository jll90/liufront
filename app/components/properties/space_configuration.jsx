import React from 'react';
import { render } from 'react-dom';
import inflector from '../../../shared/tools/inflector.js';

import { 
	isHouse,
	isOffice, 
	isApartment,
	isLot,
	isCommercial,
	hasBedrooms,
	hasBathrooms,
	isStudio,
	canHouseGuests
} from '../../../shared/helpers/properties.js';

import { currentLanguage, T } from '../../helpers/i18n.js';
import { toUpperCase } from '../../../shared/tools/string_manipulation.js';

class SpaceConfiguration extends React.Component {
	render (){
		const { bedrooms, bathrooms, singleSpace, numberOfGuests, propertyType, numberOfUnits } = this.props.property;
		const lang = currentLanguage();
		const bedroomTxt = T(lang, 'property.configuration.bedroom');
		const bathroomTxt = T(lang, 'property.configuration.bathroom');
		const guestTxt = T(lang, 'property.configuration.guest');
		const unitTxt = T(lang, 'property.configuration.unit');
		const spaceTxt = T(lang, 'property.configuration.space');
		const studioTxt = toUpperCase(T(lang, 'property.configuration.studio'));

		let configuration = "";
		
		if (isHouse(propertyType) || isApartment(propertyType)){		
			if (hasBedrooms(bedrooms)){
				configuration += bedrooms + " " + inflector(bedrooms, bedroomTxt, lang);
			} else {
				configuration += studioTxt;
			}

			if ( hasBedrooms(bedrooms) && hasBathrooms(bathrooms) || isStudio(singleSpace) ){
				configuration += " | ";
			}

			if (hasBathrooms(bathrooms)){
				configuration += bathrooms + " " + inflector(bathrooms, bathroomTxt, lang);
			}

			if (canHouseGuests(numberOfGuests)){
				configuration += (" | " + numberOfGuests + " " + inflector(numberOfGuests, guestTxt, lang));
			}
		}

		if (isOffice(propertyType)){
			configuration += bedrooms + " " + inflector(bedrooms, spaceTxt, lang) + " | " + bathrooms + " " +  inflector(bathrooms, bathroomTxt, lang);
		}

		if (isCommercial(propertyType)){
			configuration = bathrooms + " " + inflector(bathrooms, bathroomTxt, lang);
		}

		if (isLot(propertyType)){
			configuration = numberOfUnits + " "  + inflector(numberOfUnits, unitTxt, lang);
		}

		return (
			<div className="space-configuration">
				{configuration}		
			</div>
		);
	}
}

export default SpaceConfiguration;
