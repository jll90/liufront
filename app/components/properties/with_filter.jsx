import React from 'react';
import render from 'react-dom';

import { Route, Redirect } from 'react-router-dom';

import { frontEndRouter } from '../../../app/routing/router.js';

import { pathnameWithOperationAndPropertyType } from '../../../shared/helpers/properties.js';

export function withPropertyFilter(Component){
	return class PropertyFilter extends React.Component {

		constructor(props){
			super(props);
			this.state = {
				operation: this.props.searchParams.operation, 
				propertyType: this.props.searchParams.propertyType,
				appliedSelection: false,
			}
		}

		getRedirectPathname = () => {
			return pathnameWithOperationAndPropertyType(
				parseInt(this.state.operation),
				parseInt(this.state.propertyType)
			);
		}

		onChange = (e) => {
			this.setState({[e.target.name]: e.target.value});
		}

		applySelectionOnClick = () => {
			const { operation, propertyType } = this.state;
			this.props.updateSearchParams(
				{operation: parseInt(operation), propertyType: parseInt(propertyType)}
			);
			this.setState({ appliedSelection: true});
		}

		render(){
			const { operation, propertyType, appliedSelection } = this.state;
			const pathname = this.getRedirectPathname(operation, propertyType);

			if (appliedSelection === true){
				return (
					<Route>
						<Redirect push to={ {pathname} } />					
					</Route>
				);
			} else {
				return(
					<Component
						{...this.props}
						onChange={this.onChange}
						operation={operation}
						propertyType={propertyType}
						submitCb={this.applySelectionOnClick}
					/>
				);
			}
		}
	}
}
