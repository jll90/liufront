import React from 'react';
import { render } from 'react-dom';

import { Link } from 'react-router-dom';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

import { frontEndRouter } from '../../../app/routing/router.js';

class PropertyNavbar extends React.Component {

	render(){
		const id = getPermalinkFromRoute();
		return (
			<div className="property-navbar row expanded">
				<div><Link to={frontEndRouter('privatePropertyPath', { id })}>Perfil Privado</Link></div>
				<div><Link to={frontEndRouter('propertyPath', { id })}>Perfil Cliente</Link></div>
				<div><Link to={frontEndRouter('publicationsPrivatePropertyPath', { id })}>Publicaciones</Link></div>
{/*				<div><Link to={frontEndRouter('clientsPrivatePropertyPath', { id })}>Clientes</Link></div> */}
				<div><Link to={frontEndRouter('photosPrivatePropertyPath', { id })}>Fotos</Link></div>
				<div><Link to={frontEndRouter('editPropertyPath', { id })}>Editar</Link></div>
				<div><Link to={frontEndRouter('statsPrivatePropertyPath', { id })}>Estadisticas</Link></div>
			</div>
		); 
		
	}	
}

export default PropertyNavbar;
