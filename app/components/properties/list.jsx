import React from 'react';
import { render } from 'react-dom';

import PropertyListing from './listing.jsx';

import Notice from '../../views/partials/notice.jsx'; 

class PropertiesList extends React.Component {

	render (){
		const NO_PROPERTIES_MESSAGE = "Lo sentimos. No hemos encontrado propiedades con estos criterios de búsqueda.";
		const { properties, resultsLoaded } = this.props;

		if (properties.length === 0 && resultsLoaded === true){
			return (<Notice message={NO_PROPERTIES_MESSAGE} />);
		} else {
			return (
				<div className="properties-list">
					{
				        properties.map((property) => {
				        	return (<PropertyListing key={property.id} property={property} />);
			        	})
				    }
				</div>
			);
		}			
	}
}

export default PropertiesList;
