import React from 'react';
import { render } from 'react-dom';

import { Link } from 'react-router-dom';

import CopyButtonWithIcon from '../../components/misc/copy_button.jsx';

import { frontEndRouter } from '../../routing/router.js';

import TextTruncate from 'react-text-truncate';

import { isSale } from '../../../shared/helpers/properties.js';

import TableRow from '../misc/table_row.jsx';

class ProfileTable extends React.Component {

	generateRowParams = () => {
		const { 
			id, 
			title, 
			photos, 
			permalink, 
			featured, 
			stage, 
			county,
			city,
			price,
			uf,
			state,
			publications, 
			publishedOn, 
			visitingHrs,
			inhouseDescription,
			streetName,
			streetNumber,
			buildingNumber,
			fakeFullAddress,
			operationType,
			propertyType,
			publicDescription,
			pricingUnit,
			approximateLocation,
			bedrooms,
			bathrooms,
			parkingSpots,
			storageRooms,
			smallSurface,
			bigSurface
		} = this.props.property;
		
		const saleTxt = "Venta";
		const rentTxt = "Arriendo"; 

		if (!buildingNumber){
			var realAddress = `${streetName} ${streetNumber}, ${county.name}, ${city.name}, ${state.name}`;
		} else {
			var realAddress = `${streetName} ${streetNumber} ${buildingNumber}, ${county.name}, ${city.name}, ${state.name}`;
		}

		let gMapAddr = `${streetName} ${streetNumber} ${county.name} ${city.name}`;

		const showPropertyUrlPermalink = frontEndRouter('propertyPath', {id: permalink});
		const showPropertyUrlId = frontEndRouter('propertyPath', {id: id});
		const editPropertyUrl = frontEndRouter('editPropertyPath', {id: id});
		const { hostname, port, protocol } = window.location;
		
		const basePath = (port.length > 0) ? [protocol, "//", hostname, ":", port].join("") : [protocol, "//", hostname].join("");

		const shortUri = basePath + showPropertyUrlId;
		const longUri = basePath + showPropertyUrlPermalink;

		//sets the correct price based on pricing unit
		const correctPrice = (pricingUnit === "UF") ? uf : price;

		return [
			["Titulo",title],
			["Operacion",isSale(operationType) ? saleTxt : rentTxt],
			["Precio", correctPrice + " " + pricingUnit],
			["Dormitorios", bedrooms],
			["Baños", bathrooms],
			["Estacionamientos", parkingSpots],
			["Bodegas", storageRooms],
			["Metros", bigSurface + "/" + smallSurface + "m2"],
			["Descripcion", publicDescription],
			["Direccion Verdadera", realAddress],
			["Ubicacion Referencial", approximateLocation],
			["Calle", streetName],
			["Numeracion", streetNumber],
			["Numero Casa/Depto/Ofic", buildingNumber],
			["Comuna", county.name],
			["Ciudad", city.name],
			["Region", state.name],
			["Direccion Falsa", fakeFullAddress],
			["Enlace Largo", longUri],
			["Enlace Corto", shortUri]
		];
	}

	render(){
		const property = this.props.property;
		if ( property !== null && property !== undefined){
			const id = property.id;
			const rowParams = this.generateRowParams();
			return (
				<div className="property-table">
					{
						rowParams.map((param, i) => {
							return (
								<PropertyTableRow
									key={i}
									rowHeading={param[0]}
									rowContent={param[1]}
									copyText={param[1]}
								/>
							)
						})
					}
				</div>
		);
		}
		return null;
	}
}

class PropertyTableRow extends React.Component {
	render(){
		const { rowHeading, rowContent, copyText } = this.props;
		return (
			<TableRow 
				rowHeading={rowHeading}
				rowContent={rowContent}
				rowMenu={<CopyButtonWithIcon text={copyText} />}
			/>
		);
	}	
}

export default ProfileTable;
