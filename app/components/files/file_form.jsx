import React from 'react';
import { render } from 'react-dom';

import WithFileUploader from '../../../shared/components/files/uploader.jsx';

import FilePreview from './file_preview.jsx';

const WebUploader = WithFileUploader(FilePreview); 

class FileForm extends React.Component {
	constructor(){
		super();
		this.state = { files: [] };
	}

	handleChange = (e) => {
		const files = e.target.files;
		this.setState({files});
	}

	render(){
		const files = this.state.files;
		return (
			<div>
				<input type="file" id="file" multiple onChange={this.handleChange.bind(this)}/>
				<WebUploader files={files} />
			</div>
		)
	}
}

export default FileForm;
