import React from 'react';
import { render } from 'react-dom';

class FilePreview extends React.Component {
	constructor(){
		super();
		this.state = {
			preview: null
		}
	}

	componentDidMount(){
		this.loadPreview();
	}


	loadPreview = () => {
		const file = this.props.file;
		const reader = new FileReader();
		reader.onload = () => {
			this.setState({preview: reader.result});
		}
		reader.readAsDataURL(file);
	}

	deleteFile = () => {
		this.props.deleteFile(this.props.file.name);
	}

	render(){
		const { uploaded, progress, failed } = this.props;
		const { preview } = this.state;
		return (
			<div>
				<p>progress: {progress}</p>
				<p>failed: {failed === true ? 'failed' : '' }</p>
				<p>uploaded: {uploaded === true ? 'done' : 'no'}</p>
				<img style={{width: 200, height: 200 }} src={preview} />
				{ (uploaded === true ) ? <button className="button" onClick={this.deleteFile} >Delete</button> : null }
			</div>
		);
	}

}

export default FilePreview;
