import React from 'react';
import { render } from 'react-dom';

import { Redirect, Route } from 'react-router-dom'; 

import LanguageSelector from './selector.jsx';

import { connect } from 'react-redux';

import { convertRoute } from '../../routing/convert_route.js';

import { setLang } from '../../actions/language.js'; 

/* This component is in charge of redirecting the user to the same page he is on whenever he chooses a new language */ 
/* Can be improved by asserting that the action has completed and
then redirecting to avoid race conditions */
class LangRedirect extends React.Component {

	constructor(){
		super();
		this.state = {
			langSelected: false
		};
	}

	/* Obtains an equivalent path in the new language */

	obtainUrlNewLang = () => {
		const lang = this.state.langSelected;
		const route = this.props.pathname;
		const property = this.props.property;
		return convertRoute(route, lang, property);
	}

	/* Gets the lang from child component */
	getLang = lang => {
		this.setState({langSelected: lang}, this.setNewLang);	
	}

	/* sets the language in store */

	setNewLang = () => {
		const lang = this.state.langSelected;
		this.props.setLang(lang);
	}

	render(){
		const langSelected = this.state.langSelected;
		if ( langSelected === false ){
			return (
				<LanguageSelector onSelectCb={this.getLang}/>
			);
		} else {
			const pathname = this.obtainUrlNewLang();
			window.location.href = pathname;
		}
	}
}

const mapStateToProps = state => {
	return {
		property: state.propertiesReducer.show,
		pathname: state.routerReducer.location.pathname
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setLang: lang => {
			dispatch(setLang(lang))
		}
	}
}

const toStoreLangRedirect = connect(mapStateToProps, mapDispatchToProps)(LangRedirect);

export default toStoreLangRedirect;
