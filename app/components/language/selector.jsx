import React from 'react';
import { render } from 'react-dom';

class LanguageSelector extends React.Component {

	selectLang = lang => {
		this.props.onSelectCb(lang);
	}

	render(){
		return (
			<div>
				<div>
					<div onClick={() => {this.selectLang("es")}}>Español</div>
				</div>
				<div>
					<div onClick={() => {this.selectLang("en")}}>English</div>
				</div>
			</div>
		);
	}
}

export default LanguageSelector;
