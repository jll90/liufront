import React from 'react';
import { render } from 'react-dom';

import PhotoDestroy from './destroy.jsx';

class PhotoThumbnail extends React.Component {

	render(){
		const photo = this.props.photo;
		return (
			<div className="photo-thumbnail"> 
			    <img src={photo.url} />
				<PhotoDestroy {...this.props} />
			</div>
		);
	}
}

export default PhotoThumbnail;