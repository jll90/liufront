import React from 'react';
import { render } from 'react-dom';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

import PhotoThumbnail from './photo_thumbnail.jsx';

class Sortable extends React.Component {

  	onSortEnd = ({oldIndex, newIndex}) => {
  		const photos = this.props.photos;
    	
      	const sortedPhotos = arrayMove(photos, oldIndex, newIndex);
    	
      	this.props.sortPhotos(sortedPhotos);
  	};

  	saveSort = () => {
  		const propertyId = getPermalinkFromRoute(this.props.pathname);
  		const photos = this.props.photos;
  		const data = [];
  		for (let [index, photo] of Object.entries(photos)){
  			data.push({id: photo.id, order: Number.parseInt(index)});
  		}
  		this.props.saveSort({propertyId, data});
  	}
	
	render() {
		const photos = this.props.photos;
		if (photos.length === 0){
			return (<div>No hay fotos para mostrar</div>);
		}	else {
			return (
				<div>
					<button type="button" onClick={this.saveSort} className="button">Guardar Orden</button>			
					<SortableList items={photos} onSortEnd={this.onSortEnd}  deletePhoto={this.props.deletePhoto} distance={2}/>
					{/*needs distance params to let onclick event to propagate through*/}
				</div>
			);
		}
	}
}

const SortablePhoto = SortableElement(({photo, deletePhoto}) => <PhotoThumbnail photo={photo} deletePhoto={deletePhoto}/>);

const SortableList = SortableContainer(({items, deletePhoto}) => {
	return (
		<div>
			{
				items.map((photo, index) => {
					return (<SortablePhoto key={index} index={index} photo={photo} deletePhoto={deletePhoto} />)
				})
			}
		</div>
	);
});

export default Sortable;