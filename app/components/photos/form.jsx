import React from 'react';
import { render } from 'react-dom';

export default class PhotoForm extends React.Component {

	addPhotoToProperty = () => {
		const data = this.props.data;
		this.props.createPhoto({data});
	}

	onTypingChange = (e) => {
		e.preventDefault();
		const { name, value } = e.target;
		this.props.updateFormData({[name]: value});
	}

	render (){
		const url = this.props.data.url;
		return(
			<div>
				<input value={url} onChange={this.onTypingChange} name="url" />
				<button className="button" onClick={this.addPhotoToProperty}>Agregar Foto</button>
			</div>
		);
	}
}
