import React from 'react';
import { render } from 'react-dom';

export default class PhotoDestroy extends React.Component {

	deletePhoto = () => {
		const photo = this.props.photo;
		const id = photo.id;
		this.props.deletePhoto({id});
	}

	render(){
		return (
			<div className="remove-photo" onClick={this.deletePhoto}><i className="fa fa-trash-o"></i></div>
		);
	}
}
