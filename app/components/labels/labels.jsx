import React from 'react';
import { render } from 'react-dom';

class Label extends React.Component {
	onClick = () => {
		this.props.onClick(this.props.txt);
	}

	render(){
		const { txt } = this.props;
		return (
			<span onClick={this.onClick} className="label primary">{txt}</span>
		);
	}
}

export class LabelList extends React.Component {
	render(){
		const { onClick, items } = this.props;
		
		if ( items.length === 0){
			return <p>No hay etiquetas seleccionadas</p>;
		} else {
			return (
				<div>
					{	
						items.map((el, i) => {
							return (
								<Label
									onClick={onClick}
									txt={el}
									key={i} 
								/>
							);
						})
					}
				</div>
			);
		}
	}
}
