import React from 'react';
import { render } from 'react-dom';

import { withArraySelector } from '../hocs/array_selector.jsx';

import SelectAsDelete from '../hocs/proxies/select_as_delete.jsx';

import LabelListDelete from '../hocs/proxies/label_list_delete.jsx';

const LabelAdder = withArraySelector(SelectAsDelete, LabelListDelete);

class LabelSelector extends React.Component {
	render(){
		const { unselectedItems, selectedItems } = this.props; 
		return (
			<LabelAdder
				unselectedItems={unselectedItems}
				selectedItems={selectedItems}
			/>
		);
	}
}

export default LabelSelector;
