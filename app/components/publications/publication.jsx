import React from 'react';
import { render } from 'react-dom';

import TextTruncate from 'react-text-truncate';

import { frontEndRouter } from '../../../app/routing/router.js';

import { Link } from 'react-router-dom';

import CustomDate from '../../views/partials/custom_date.jsx';

import { update, destroy } from '../../../shared/services/publications';



import { shouldUpdatePublication } from '../../../shared/helpers/publications.js';

import CopyButtonWithIcon from '../misc/copy_button.jsx';

export default class Publication extends React.Component {

	dispatchCall = (action) => { 
		const id = this.props.publication.id;
		const self = this;
		var successFn = null;
		var config = null;

		if (action === "update"){
			successFn = function(){
				self.props.updateCallback(id);

			}

			config = {successFn, id};
			update(config);
		} 

		if (action === "delete"){
			successFn = function(){
				self.props.destroyCallback(id);

			}

			config = { successFn, id };
			destroy(config);
		}
	}

	render(){
		const p = this.props.publication;
		const { url } = p;
		return (
			<div className="publication row">
				<div className="columns medium-8">
					<div>
						<strong>{p.platform.name}</strong>
					</div>
					<div className="copy-btn row">
						<div className="small-11 columns">
						<TextTruncate
							line={1}
							truncateText="..."
							text={url}
						/>
						</div>
						<div className="small-1 columns">
						<CopyButtonWithIcon text={url} />
						</div>
					</div>
					<a href={url} target="_blank">Ver Enlace</a>
					<br />
					<CustomDate string="Fecha de ultima actualizacion" timestamp={p.updatedAt} />


				</div>
				<div className="columns medium-4">
					<button className="button alert" type="button" onClick={()=> {this.dispatchCall("delete")}}>Eliminar</button>
				</div>
			</div>
		);
	}
}

