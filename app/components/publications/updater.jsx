import React from 'react';
import { render } from 'react-dom';

import PublicationList from '../../components/publications/publication.jsx';

import { index } from '../../../shared/services/publications';

class PublicationIndex extends React.Component {

	constructor(){
		super();
		this.state = {
			publications: []
		};
	}

	componentDidMount(){
		const self = this;
		const successFn = function(data){
			self.setPublications(data);
		}

		const config = { successFn };

		index(config);
	}


	/* returns the index */
	findPublicationByIndex = (id) => {
		const publications = [].concat(this.state.publications);
		return publications.findIndex(function(el){
			return (el.id === id);
		});
	}

	/*id is passed so as to know what element to update*/
	update = (id) => {
		const publications = [].concat(this.state.publications);
		const index = this.findPublicationByIndex(id);
		const publication = publications[index];
		publications.splice(index, 1);
		publications.push(publication);
		this.setPublications(publications);
	}

	destroy = (id) => {
		const publications = [].concat(this.state.publications);
		const index = this.findPublicationByIndex(id);
		publications.splice(index, 1);
		this.setPublications(publications);
	}

	setPublications = (publications) => {
		this.setState({
			publications
		});
		document.body.scrollTop = 0;
	}

	render(){
		const publications = this.state.publications;
		return (
			<div className="publications-view row">
				<h1>Publicaciones</h1>
				<PublicationList 
					publications={publications} 
					updateCallback={this.update}
					destroyCallback={this.destroy}
				/>
			</div>
		);
	}	
}

export default PublicationIndex;


