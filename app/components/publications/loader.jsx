import React from 'react';
import { render } from 'react-dom';

import PublicationList from './list.jsx';

import { index } from '../../../shared/services/publications.js';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

class PublicationLoader extends React.Component {

	constructor(){
		super();
		this.state = {
			publications: []
		};
	}

	componentDidMount(){
		this.getPublications();
	}

	getPublications = () => {
		const self = this;
		const successFn = (publications) => {
			self.setState({publications});
		}	

		const propertyId =  getPermalinkFromRoute();

		const config = { successFn, propertyId }

		index(config); 
	}

	render(){
		const publications = this.state.publications;
		return (	
			<PublicationList publications={publications} />	
		);
	}	
}

export default PublicationLoader;
