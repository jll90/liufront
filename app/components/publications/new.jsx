import React from 'react';
import { render } from 'react-dom';

import { platforms, platformToText } from '../../../shared/constants/platforms.js';
import { create } from '../../../shared/services/publications';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

class PublicationNew extends React.Component {

	constructor(){
		super();
		this.state = {
			"platformId": 0,
			"daysToUpdate": 7,
			"url": ''
		}
	}

	submit = () => {
		const self = this;
		const successFn = function(data){
			/*self.props.callback(data);*/

		}
		const errorFn = function(){

		}
		const propertyId = getPermalinkFromRoute(); 
		const { platformId, daysToUpdate, url } = this.state;
		const data = {
			propertyId, platformId, daysToUpdate, url
		};
		const config = {successFn, data, errorFn};
		create(config);
	}

	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	render(){		
		const { platformId, daysToUpdate, url } = this.state;
		return (
			<div className="publication-new">
				<h3>Agregar una publicacion</h3>
				<p>Dias para actualizar</p>
				<input name="daysToUpdate" value={daysToUpdate} onChange={this.onChange} />
				<p>Enlace</p>
				<input name="url" value={url} onChange={this.onChange} />
				<select name="platformId" onChange={this.onChange} value={platformId}>
			  		{
			  			[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((index) =>
			  				<option key={index} value={index}>{platformToText(index)}</option>		
			  			)
			  		}
			    </select>
				<button className="button" onClick={this.submit}>Agregar Publicacion</button>
			</div>
		);
	}
}

export default PublicationNew
