import React from 'react';
import { render } from 'react-dom';

import Publication from './publication.jsx';

class PublicationList extends React.Component {
	render(){
		const publications = this.props.publications;
		return (
			<div className="publication-list">
				{
					publications.map((p) => {
						return (
							<Publication 
								key={p.id} 
								publication={p}
								updateCallback={this.props.updateCallback}
								destroyCallback={this.props.destroyCallback}
							/>
						);
					})
				} 
			</div>
		);
	}
}

export default PublicationList;
