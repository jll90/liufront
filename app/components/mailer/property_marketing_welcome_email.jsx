import React from 'react';
import { render } from 'react-dom';

import { filteredIndex } from '../../../shared/services/properties.js';
import { sendWelcomeEmail } from '../../../shared/services/mailer.js';

import { buildEmailMessage } from '../../helpers/mailer.js';

import { index } from '../../../shared/services/publications';

import ReactMarkdown from 'react-markdown';



class PropertyMarketingWelcomeEmail extends React.Component {

	constructor(){
		super();
		this.state = {
			name: '',
			message: '',
			publications: [],
			properties: [],
			propertyId: '',
			gender: 'M',
			email: ''
		};
	}

	componentDidMount(){
		this.getAvailableProperties();
	}

	updateMessage = () => {
		const { publications, propertyId, gender, name } = this.state;
		const message = buildEmailMessage(propertyId, publications, gender, name);
		this.setMessage(message);
	}

	setMessage(message){
		this.setState({message});
	}

	getAvailableProperties = () => {
		const self = this;
		//to show properties on marketing stage - nothing else
		const filter = "default";
		const successFn = function(properties){
			self.setState({properties})
		}

		const config = { filter, successFn };
		filteredIndex(config);
	}

	getPublications = (propertyId) => {
		const self = this;
		const successFn = function(publications){
			self.setState({publications}, self.updateMessage);
		}

		const config = { propertyId, successFn }
		index(config);
	}

	onChange = (e) => {
		const { name, value } = e.target;
		if (name === "message"){
			this.setState({[name]: value}); // no need to update message when writing to message directly
		} else {
			this.setState({[name]: value}, this.updateMessage);
		}
		
		if (name === "propertyId"){
			this.getPublications(value);
		}
	}

	sendEmail = () => {
		const self = this;
		const successFn = () => {
			
		}
		const errorFn = () => {
			
		}
		const { message, email } = this.state;
		const data = { message, email };
		const config = { data, successFn, errorFn };
		sendWelcomeEmail(config);
	}

	render(){
		const { properties, publications, propertyId, message, name, gender, email } = this.state;
		return(
			<div>
				<h5>Propiedades disponibles</h5>
				<p>Por favor, selecciona una propiedad</p>
				<select name="propertyId" onChange={this.onChange} value={propertyId}>
		  		{
		  			properties.map((p) =>
		  				<option key={p.id} value={p.id}>{"LIU"+p.id}</option>		
		  			)
		  		}
			  </select>
			  <input type="text" placeholder="Correo electronico" name="email" onChange={this.onChange} value={email}/>
			  <input type="text" placeholder="Nombre Cliente" name="name" onChange={this.onChange} value={name}/>
				<input type="radio" value="M" name="gender" checked={gender === "M"} onChange={this.onChange} /> Cliente es Hombre
				<input type="radio" value="F" name="gender" checked={gender === "F"} onChange={this.onChange} /> Cliente es Mujer				
			  <textarea 
	  			rows="10" 
	  			placeholder="Mensaje para el cliente"
	  			value={message} 
	  			onChange={this.onChange}
	  			name="message"
		  	/>
		  	<p>Vista preliminar de como se ve el correo</p>
		  	<ReactMarkdown source={message} />
		  	<button type="button" className="button expand" onClick={this.sendEmail}>Enviar Correo de Bienvenida</button>
			</div>
		);
	}
}

export default PropertyMarketingWelcomeEmail;