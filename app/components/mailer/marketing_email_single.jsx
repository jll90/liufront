import React from 'react';
import { render } from 'react-dom';

const cleanState = {
	clientName: '',
	toEmail: '',
	gender: '',
	emailType: ''
};

export class MarketingEmailSingle extends React.Component {
	constructor(){
		super();
		this.state = cleanState;
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	
	submitForm = (e) => {
		e.preventDefault();
		this.props.sendSingleEmail({data: this.state});
	}

	render(){
		const { clientName, toEmail, gender, emailType } = this.state;
		return (
		<form className="row columns" onSubmit={this.submitForm}>
			<input type="text" placeholder="Nombre Cliente" name="clientName" onChange={this.handleChange} value={clientName}/>
			<input type="text" placeholder="Correo Cliente" name="toEmail" onChange={this.handleChange} value={toEmail}/>
			<p>Indicar si es hombre o mujer</p>
			<div onChange={this.handleChange.bind(this)}>
				<input type="radio" value="M" name="gender" checked={gender === "M"}/> Cliente es Hombre
				<input type="radio" value="F" name="gender" checked={gender === "F"}/> Cliente es Mujer
			</div>
			<br />
			<p>Indicar tipo de correo</p>
			<div onChange={this.handleChange.bind(this)}>
				<input type="radio" value="D" name="emailType" checked={emailType === "D"}/> Por Defecto
			</div>
			<input type="submit" className="button expand" value="Enviar"/>
		</form>
		);
	}
}

export default MarketingEmailSingle;
