import React from 'react';
import { render } from 'react-dom';

import FileInput from 'react-file-input';

import Papa from 'papaparse';


class MarketingMassEmail extends React.Component {
	constructor(){
		super();
		this.state = {
			csvData: []
		}
	}

	handleFileChange = (e) => {
		const file = e.target.files[0];
		const self = this;
		Papa.parse(file, {
			complete: function(results){
				self.setState({
					csvData: results.data
				});
			}
		});
	}

	submitCSV = (e) => {
		e.preventDefault();
		const data = this.state.csvData;
		const config = {data};
		this.props.sendMassEmail(config);
	}

	render(){
		return (
			<form onSubmit={this.submitCSV}>
				<FileInput 
				   name="myImage"
           accept=".csv"
           placeholder="Upload CSV"
           onChange={this.handleFileChange} />
         <input type="submit" className="button expand" value="Process CSV" />
			</form>
		);
	}

}

export default MarketingMassEmail;
