import React from 'react';
import render from 'react-dom';

import { sortSellingPointsByCategory } from '../../../shared/helpers/selling_points.js';

class SellingPoint extends React.Component {
	render (){
		const description = this.props.description;
		return (
			<li><i className="fa fa-check"></i>{description}</li>
		);
	}
}	

class SellingPointList extends React.Component {
	render () {
		const categoryName = this.props.headerName;
		const sellingPoints = this.props.sellingPoints;
		const numberOfCategories = this.props.numberOfCategories;
		var className = "small-12 ";
		if (numberOfCategories === 2){
			className += "medium-6";
		} 
		if (numberOfCategories === 3){
			className += "medium-4";
		}
		className += " columns";
		return (
			<div className={className}>
				<div className="category-title">{categoryName}</div>
				<ul>
					{
						sellingPoints.map((sellingPoint) => 
							<SellingPoint 
								key={sellingPoint.id}
								description={sellingPoint.description}
							/>
						)
					}
				</ul>
			</div>
		);
	}
}

class SellingPointsContainer extends React.Component {
	render (){
		const sellingPoints = this.props.sellingPoints;
		const sortedSellingPoints = sortSellingPointsByCategory(sellingPoints);
		const numberOfCategories = sortedSellingPoints.length;		
		return (
			<div className="small-12 columns pros-container" id="puntos-a-favor">
				{
					sortedSellingPoints.map((sellingPointOrderedTriplet) => 
						<SellingPointList 
							key={sellingPointOrderedTriplet[0]}
							headerName={sellingPointOrderedTriplet[1]}
							sellingPoints={sellingPointOrderedTriplet[2]}
							numberOfCategories={numberOfCategories}
						/>
					)
				}
			</div>
		);
	}
}

export default SellingPointsContainer;
