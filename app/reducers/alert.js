import { TRIGGER_ALERT, CLOSE_ALERT } from '../actions/actionTypes.js';

const defaultState = {
	shouldRender: false,
	message: null,
	type: null
}

export const alertReducer = (state = defaultState, action) => {
	switch (action.type){
		case TRIGGER_ALERT:
			return { shouldRender: true, message: action.message, mode: action.mode }; 
		case CLOSE_ALERT:
			return defaultState;
		default:
			return state;
	}
}
