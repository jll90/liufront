import { SET_LANG } from '../actions/actionTypes.js';

import { currentLanguage } from '../helpers/i18n.js';

const initState = {
	lang: currentLanguage()
}

export const langReducer = (state = initState, action) => {
	switch(action.type) {
		case SET_LANG:
			return { lang: action.lang };
		default:
			return state;
	}
}
