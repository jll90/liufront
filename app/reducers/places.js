import { GET_PLACES } from '../actions/actionTypes.js';

const initState = {
	counties: [],
	cities: [],
	states: []
};

export const placesReducer = (state = initState, action) => {
	switch(action.type){
		case GET_PLACES:
			const { counties, cities, states } = action;
			return {...state, counties, cities, states };
		default:
			return state
	}
}
