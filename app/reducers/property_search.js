import { UPDATE_PROPERTY_SEARCH_PARAMS } from '../actions/actionTypes.js';

const initState = {
	operation: 0,
	propertyType: 0
}

export const propertySearchReducer = (state = initState, action) => {
	switch(action.type){
		case UPDATE_PROPERTY_SEARCH_PARAMS:
			const params = action.params;
			return params;
		default:
			return state;
	}
}
