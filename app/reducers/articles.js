import { GET_INDEX_ARTICLES, GET_ARTICLE } from '../actions/actionTypes.js';

const initState = {
	index: {
		articles: [],
		loading: true,
		finished: false
	},
	show: {
		article: null,
		loading: true,
		finished: false
	}
}

export const articlesReducer = (state = initState, action) => {
	const { loading, finished, articles, article } = action;
	switch (action.type){
		case GET_INDEX_ARTICLES:
			const newIndex = {
				loading, finished, articles
			};
			return {...state, index: newIndex }
		case GET_ARTICLE:
			return {...state, show: { loading, finished, article }}			
		default:
			return state;
	}
}
