import { CREATE_PHOTO, EDIT_PHOTO_FORM, GET_PHOTOS, SORT_PHOTOS, SAVE_SORT, DELETE_PHOTO } from '../actions/actionTypes.js';

const initState = {
	photos: [],
	form: {
		data: { url: ''}
	}
}

export const photosReducer = (state = initState, action) => {
	switch(action.type){
		case DELETE_PHOTO:
			let photos = [...state.photos];
			const indexToDelete = photos.findIndex(photo => {
				return photo.id == action.photo.id;
			});
			photos.splice(indexToDelete, 1);
			return {...state, photos};
		case GET_PHOTOS:
		case SORT_PHOTOS: 
			return {...state, photos: action.photos};
		case CREATE_PHOTO:
			let form = state.form;
			return {photos: [action.photo, ...state.photos], form: {data: {url: ''}} };
		case EDIT_PHOTO_FORM:
			let data = state.form.data;
			let newData = {...data, ...action.data};
			return {...state, form: {data: newData}};
		case SAVE_SORT:
		default:
			return state;
	}
}