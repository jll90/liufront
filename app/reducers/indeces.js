import { GET_UF_VALUE } from '../actions/actionTypes.js';

const initState = {
	uf: {
		latest: null
	}
}

export const indecesReducer = (state = initState, action) => {
	switch(action.type){
		case GET_UF_VALUE:
			return {...state, uf: {latest: action.latestVal}};
		default:
			return state;
	}
}
