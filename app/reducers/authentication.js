import { LOG_IN, LOG_OUT } from '../actions/actionTypes.js';

import { isAuthenticated } from '../providers/authentication_provider.js';

const initState = {
	loggedIn: isAuthenticated()
}

export const authenticationReducer = (state = initState, action) => {
	switch (action.type){
		case LOG_IN:
			return {loggedIn: true}
		case LOG_OUT:
			return {loggedIn: false}
		default:
			return state;
	}
}
