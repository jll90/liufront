import { TOGGLE_FILTER_MENU } from '../actions/actionTypes.js';

const initState = {
	filter: "menu",
}

export const toggleMenu = (state = initState, action) =>{
	switch (action.type){
		case TOGGLE_FILTER_MENU:
			state = {...state, filter: action.filter};
			return state;
		default:
			return state;
	}
}
