import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';

import { toggleMenu } from './navigation.js';
import { propertiesReducer } from './properties.js';

import { propertySearchReducer } from './property_search.js'; 

import { articlesReducer } from './articles.js';

import { alertReducer } from './alert.js';

import { langReducer } from './language.js';

import { indecesReducer } from './indeces.js';

import { clientsReducer } from './clients.js';

import { authenticationReducer } from './authentication.js';

import { placesReducer } from './places.js';

import { photosReducer } from './photos.js';

const rewApp = combineReducers ({
	routerReducer,
	toggleMenu,
	alertReducer,
	propertiesReducer,
	propertySearchReducer,
	articlesReducer,
	langReducer,
	indecesReducer,
	authenticationReducer,
	clientsReducer,
	placesReducer,
	photosReducer
});

export default rewApp;