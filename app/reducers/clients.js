import { GET_CLIENTS } from '../actions/actionTypes.js';

const initState = {
	index: {
		clients: [],
		loading: true,
		finished: false
	}
}

export const clientsReducer = (state = initState, action) => {
	switch (action.type){
		case GET_CLIENTS:
			const { clients, loading, finished } = action;
			return {...state, index: { clients, loading, finished }};
		default:
			return state;
	}
}
