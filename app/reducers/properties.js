import { 
	GET_LATEST_PROPERTIES, 
	GET_INDEX_PROPERTIES, 
	GET_PROPERTY, 
	FOUND_CODE, 
	CLEAR_CODE,
	UPDATE_CODE,
	GET_ALL_PROPERTIES,
	GET_PRIVATE_PROPERTY,
	CREATE_PROPERTY,
	EDIT_PROPERTY_FORM,
	LOAD_PROPERTY_EDIT,
	UPDATE_PROPERTY
} from '../actions/actionTypes.js';

import { INIT_STATE } from '../../shared/constants/properties.js';

const initState = {
	justListed: [],
	index: [],
	show: null,
	code: {
		found: false,
		input: '',
		propertyId: null
	},
	all: [],
	privateProfile: null,
	form: {
		data: INIT_STATE,
		responseSuccess: false
	} 
};

export const propertiesReducer = (state = initState, action) => {
	switch (action.type){
		case LOAD_PROPERTY_EDIT:
			let resSuccess = state.form.responseSuccess;
			return {...state, form: { data: action.property, responseSuccess: resSuccess }};
		case EDIT_PROPERTY_FORM:
			let data = {...state.form.data, ...action.data};
			let responseSuccess = state.form.responseSuccess;
			return {...state, form: { data, responseSuccess }};
		case GET_PRIVATE_PROPERTY:
			return {...state, privateProfile: action.property};
		case GET_ALL_PROPERTIES:
			return {...state, all: action.properties };
		case GET_LATEST_PROPERTIES:
			return {...state, justListed: action.properties};
		case GET_INDEX_PROPERTIES:
			return {...state, index: action.properties};
		case GET_PROPERTY:
			return {...state, show: action.property};
		case FOUND_CODE:
			return {...state, code: { ...state.code, found: true, propertyId: action.code }};
		case CLEAR_CODE:
			return {...state, code: { input: '', found: false, propertyId: null }};
		case UPDATE_CODE:
			return {...state, code: { ...state.code, input: action.code}};
		case CREATE_PROPERTY:
		case UPDATE_PROPERTY:
			return {...state, form: { responseSuccess: true }};
		default: 
			return state
	}
}
