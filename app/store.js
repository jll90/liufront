import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rewApp from './reducers/index.js';

import { routerReducer, routerMiddleware } from 'react-router-redux';

import createHistory from 'history/createBrowserHistory';

export const history = createHistory();

const middlewares = [thunk, routerMiddleware(history)];

if ( process.env.NODE_ENV !== "production" ){
	middlewares.push(createLogger());
}

const store = createStore(
		rewApp,
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
		applyMiddleware( ...middlewares )
);


export default store;