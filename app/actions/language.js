import { SET_LANG } from './actionTypes.js';

export const setLang = lang => {
	return { 
		type: SET_LANG,
		lang
	} 
}
