import { CREATE_PHOTO, EDIT_PHOTO_FORM, GET_PHOTOS, SORT_PHOTOS, SAVE_SORT, DELETE_PHOTO } from './actionTypes.js';
import asyncAction from './handler.js';

import { create, index, updateSort, destroy } from '../../shared/services/photos.js';

const createPhotoSuccess = photo => {
	return {
		type: CREATE_PHOTO,
		photo
	}
}

const getPhotosSuccess = photos => {
	return {
		type: GET_PHOTOS,
		photos
	}
}

const saveSortSuccess = () => {
	return {
		type: SAVE_SORT
	}
}

const deletePhotoSuccess = photo => {
	return {
		type: DELETE_PHOTO,
		photo
	}
}

export const sortPhotos = photos => {
	return {
		type: SORT_PHOTOS,
		photos
	}
}

export const editPhotoForm = data => {
	return {
		type: EDIT_PHOTO_FORM,
		data
	}
}

export const createPhoto = params => {
	return asyncAction(
		{
			apiCall: create,
			params,
			authentication: true,
			successMsg: "Foto creada con éxito",
			errorMsg: "La foto no pudo ser creada",
			action: createPhotoSuccess
		}
	);
}

export const getPropertyPhotos = params => {
	return asyncAction(
		{
			apiCall: index,
			params,
			authentication: true,
			errorMsg: "No se pudo cargar las fotos",
			action: getPhotosSuccess
		}
	);
}

export const saveSort = params => {
	return asyncAction(
		{
			apiCall: updateSort,
			params,
			authentication: true,
			successMsg: "Orden guardado correctamente",
			errorMsg: "No se guardar el orden",
			action: saveSortSuccess
		}
	);	
}

export const deletePhoto = params => {
	return asyncAction(
		{
			apiCall: destroy,
			params,
			authentication: true,
			successMsg: "Foto eliminada correctamente",
			errorMsg: "No se pudo eliminar la foto",
			action: deletePhotoSuccess
		}
	);	
}