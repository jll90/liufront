import asyncAction from './handler.js';
import { sendMultipleEmails, sendEmail } from '../../shared/services/mailer.js';
import { SEND_MASS_EMAIL, SEND_SINGLE_EMAIL } from './actionTypes.js';

export const sendMassEmail = params => {
	return asyncAction(
		{
			apiCall: sendMultipleEmails,
			successMsg: "Correos enviados correctamente",
			errorMsg: "No se pudieron enviar los correos",
			action: () => {
				return {
					type: "SEND_MASS_EMAIL"
				}
			},
			params,
			authentication: true
		}
	);
}

export const sendSingleEmail = params => {
	return asyncAction(
		{
			apiCall: sendEmail,
			successMsg: "Correo enviado correctamente",
			errorMsg: "No se pudo enviar el correo.",
			action: () => {
				return {
					type: SEND_SINGLE_EMAIL
				}
			},
			params,
			authentication: true
		}
	);
}