import { GET_PLACES } from './actionTypes.js';

import { index } from '../../shared/services/places.js';

import asyncAction from './handler.js';

const getPlacesSuccess = data => {
	return {
		type: GET_PLACES,
		counties: data.counties,
		cities: data.cities,
		states: data.states
	}
}

export const getPlaces = () => {
	return asyncAction (
		{
			apiCall: index,
			params: {},
			authentication: true,
			action: getPlacesSuccess
		}
	);
}
