/*Navigation*/

export const TOGGLE_FILTER_MENU = 'toggleFilterMenu';

/*EndNavigation*/

/*Property Search Params*/

export const UPDATE_PROPERTY_SEARCH_PARAMS = 'updatePropertySearchParams';
/*End PSP*/

/*Properties*/

export const GET_LATEST_PROPERTIES = 'getLatestProperties';

export const GET_INDEX_PROPERTIES = 'getIndexProperties';

export const GET_PROPERTY = 'getProperty';

export const FOUND_CODE = 'foundCode';

export const CLEAR_CODE = 'clearCode';

export const UPDATE_CODE = 'updateCode';

export const GET_PROPERTY_BY_CODE = 'getPropertyByCode';

export const GET_ALL_PROPERTIES = 'getAllProperties';

export const GET_PRIVATE_PROPERTY = 'getPrivateProperty';

export const CREATE_PROPERTY = 'createProperty';

export const UPDATE_PROPERTY = 'updateProperty';

export const EDIT_PROPERTY_FORM = 'editPropertyForm';

export const LOAD_PROPERTY_EDIT = 'loadPropertyEdit';

/*End Properties*/

export const GET_INDEX_ARTICLES = 'getIndexArticles';

export const GET_ARTICLE = 'getArticle';

/*Alert*/

export const TRIGGER_ALERT = 'triggerAlert';

export const CLOSE_ALERT = 'closeAlert';

/*EndAlert*/

/*Authentication*/

export const LOG_IN = 'logIn';

export const LOG_OUT = 'logOut';

/*End Authentication*/

/*Language*/

export const SET_LANG = 'setLang';

/*End Language */

/*Indeces*/

export const GET_UF_VALUE = 'getUfValue';

/*EndIndeces*/

/*Quotes*/

export const POST_QUOTE = 'postQuote';

/*EndQuotes */

/* Clients */
export const GET_CLIENTS = 'getClients';

/*endClients */

/* Places */

export const GET_PLACES = 'getPlaces';

/* EndPlaces */

/* Photos */

export const CREATE_PHOTO = 'createPhoto';

export const EDIT_PHOTO_FORM = 'editPhotoForm';

export const GET_PHOTOS = 'getPhotos';

export const SORT_PHOTOS = 'sortPhotos';

export const SAVE_SORT = 'saveSort';

export const DELETE_PHOTO = 'deletePhoto';

/* EndPhotos */

/* Emails */

export const SEND_SINGLE_EMAIL = 'sendSingleEmail';

export const SEND_WELCOME_EMAIL = 'sendWelcomeEmail';

export const SEND_MASS_EMAIL = 'sendMassEmail';


/* EndEmails */