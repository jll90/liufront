import { UPDATE_PROPERTY_SEARCH_PARAMS } from './actionTypes.js';

export const updatePropertySearchParams = params => {
	return {
		type: UPDATE_PROPERTY_SEARCH_PARAMS, params
	}; 
}
