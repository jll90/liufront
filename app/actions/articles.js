import { GET_INDEX_ARTICLES, GET_ARTICLE } from './actionTypes.js';

import { index, show } from '../../shared/services/articles.js';


export const getArticlesSuccess = articles => {
 return	{ type: GET_INDEX_ARTICLES, articles, loading: false,finished: true };
}

export const getArticleSuccess = article => {
	return { type: GET_ARTICLE, article, loading: false, finished: true};
}

export const getArticles = (config) => {
	return dispatch => {
		return index(config).then(articles=> {
			dispatch(getArticlesSuccess(articles));			
		}).catch(() => {
			throw ('Error getting articles');
		})
	} 
}

export const getArticle = config => {
	return dispatch => {
		return show(config).then(article => {
			dispatch(getArticleSuccess(article));
		}).catch(()=> {
			throw ('Could not get article');
		})
	}
}
