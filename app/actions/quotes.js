import asyncAction from './handler.js';

import { create } from '../../shared/services/quotes.js';

export const postQuote = (config, callback) => {
	return asyncAction(
		{
			apiCall: create,
			params: config,
			callback,
			successMsg: "Solicitud enviada con exito" 
		}
	);
}
