import { TOGGLE_FILTER_MENU } from './actionTypes.js';

export const toggleFilter = filter => {
	return {
		type: TOGGLE_FILTER_MENU,
		filter
	};		
}
