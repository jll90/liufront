import { showErrorAlert, showSuccessAlert } from './alert.js';
import { jsonSnakeCaseToCamelCase } from '../../shared/tools/object_manipulation.js';
import { destroySession } from './authentication.js';

import { getToken, destroyToken } from '../providers/authentication_provider.js';

import { history } from '../store.js'; /* Imports history from store to keep app in sync */

import { frontEndRouter } from '../routing/router.js';

const apiCall = config => (dispatch, getState) => {
	if (config.authentication === true){
		config.params = {...config.params, token: getToken()}
	}

	return config.apiCall(config.params)
		.then(data => {
			const formattedData = jsonSnakeCaseToCamelCase(data); /* Formats data to be received from RoR Server */	
			if ( config.callback ){
				config.callback();
			} /* Useful to clear forms after submit */

			if ( config.action ){
				dispatch(config.action(formattedData));
			} /* Calls handler to set data w/ reducers */

			if ( config.hasOwnProperty('successMsg')){
				dispatch(showSuccessAlert(config.successMsg, "success")); /* Show success msg if needed */
			} 
		}).catch(err => {
				console.error(err);
				if (err.response.status === 401){
					const pathname = frontEndRouter('logInPath');
					dispatch(destroySession());
					history.push(pathname); /* Redirects to login Page upon detecting unauthentication */
				}
				if ( config.hasOwnProperty('errorMsg') ){
					dispatch(showErrorAlert(config.errorMsg, "error")); /* Optionally allow for error messages for better user interaction */
				} else {
					console.log(err.response);
				}
		})
}

export default apiCall;
