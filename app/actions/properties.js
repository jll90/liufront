import {
	GET_LATEST_PROPERTIES, 
	GET_INDEX_PROPERTIES, 
	GET_PROPERTY, 
	FOUND_CODE, 
	CLEAR_CODE,
	UPDATE_CODE,
	GET_ALL_PROPERTIES,
	GET_PRIVATE_PROPERTY,
	CREATE_PROPERTY,
	EDIT_PROPERTY_FORM,
	LOAD_PROPERTY_EDIT,
	UPDATE_PROPERTY
} from './actionTypes.js';

import asyncAction from './handler.js'; 

import { index, show, justListedIndex, findCode, filteredIndex, privateShow, create, edit, update } from '../../shared/services/properties.js';

import { getAddressInfo, reverseGeocoding } from '../../shared/services/geolocation.js';

import { parseAddress } from '../../shared/helpers/geolocation.js';

const parseAddressInfo = data => {
	const address = data.results[0];
	const fakeFullAddress = parseAddress(address);
	const { lat, lng } = address.geometry.location;
	return {
		type: EDIT_PROPERTY_FORM,
		data: { 
			latitude: lat,
			longitude: lng,
			fakeFullAddress
		}
	};
}

const createPropertySuccess = () => {
	return {
		type: CREATE_PROPERTY
	}
}

const updatePropertySuccess = () => {
	return {
		type: UPDATE_PROPERTY
	}
}

const getAllPropertiesSuccess = properties => {
	return {
		type: GET_ALL_PROPERTIES,
		properties
	}
}

const getLatestPropertiesSuccess = properties => {
	return { 
		type: GET_LATEST_PROPERTIES,
		properties	
	}
}

const getIndexPropertiesSuccess = properties => {
	return {
		type: GET_INDEX_PROPERTIES,
		properties
	}
}

const getPrivatePropertySuccess = property => {
	return {
		type: GET_PRIVATE_PROPERTY,
		property
	}
}

const getPropertySuccess = property => {
	return {
		type: GET_PROPERTY,
		property
	}
}

const getEditPropertySuccess = property => {
	return {
		type: LOAD_PROPERTY_EDIT,
		property
	}
}

export const unsetCode = () => {
	return {
		type: CLEAR_CODE
	}
}

export const updateCode = code => {
	return {
		type: UPDATE_CODE, code
	}
}

const findByCodeSuccess = property => {
	return {
		type: FOUND_CODE,
		code: property.id
	}
}

export const editPropertyForm = data => {
	return {
		type: EDIT_PROPERTY_FORM,
		data	
	}
}

export const showPropertyProfile = params => {
	return asyncAction (
		{
			apiCall: privateShow,
			params,
			action: getPrivatePropertySuccess,
			authentication: true
		}
	);
}

export const indexAllProperties = params => {
	return asyncAction (
		{
			apiCall: filteredIndex,
			params,
			action: getAllPropertiesSuccess,
			authentication: true
		}
	);
}

export const findByCode = code => {
	return asyncAction (
		{
			apiCall: findCode,
			params: { code },
			action: findByCodeSuccess,
			errorMsg: "No se pudo encontrar la propiedad con ese código"
		}
	);
}

export const getLatestProperties = () => {
	return asyncAction (
		{ 
			apiCall: justListedIndex, 
			params: null, 
			action: getLatestPropertiesSuccess
		}
	);
}

export const indexProperties = params => {
	return asyncAction (
		{
			apiCall: index, 
			params, 
			action: getIndexPropertiesSuccess
		}
	);
}

export const loadEditProperty = params => {
	return asyncAction (
		{
			apiCall: edit,
			params,
			action: getEditPropertySuccess,
			authentication: true
		}
	);
}

export const showProperty = params => {
	return asyncAction (
		{
			apiCall: show, 
			params, 
			action: getPropertySuccess
		}
	);	
}

export const createProperty = params => {
	return asyncAction (
		{
			apiCall: create,
			params,
			action: createPropertySuccess,
			errorMsg: "No se pudo crear la propiedad. Corrige el formulario",
			successMsg: "Propiedad creada correctamente.",
			authentication: true
		}
	);
}

export const updateProperty = params => {
	return asyncAction (
		{
			apiCall: update,
			params,
			action: updatePropertySuccess,
			errorMsg: "No se pudo actualizar la propiedad. Corrige el formulario",
			successMsg: "Propiedad actualizar correctamente.",
			authentication: true
		}
	);
}

export const gAddressInfo = params => {
	return asyncAction (
		{
			apiCall: getAddressInfo,
			params,
			action: parseAddressInfo,
			errorMsg: "No se pudo obtener la dirección"	
		}
	);
}

export const gReverseGeocode = params => {
	return asyncAction (
		{
			apiCall: reverseGeocoding,
			params, 
			action: parseAddressInfo,
			errorMsg: "No se pudo cargar direccion en base a las coordenadas."
		}
	);
}
