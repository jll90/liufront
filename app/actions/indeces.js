import { GET_UF_VALUE } from './actionTypes.js';

import asyncAction from './handler.js';

import { latestUf } from '../../shared/services/indices.js';

const getUfSuccess = index => {
	return {
		type: GET_UF_VALUE,
		latestVal: index.value
	}
}

export const getLatestUf = () => {
	return asyncAction (
		{	
			apiCall: latestUf,
			action: getUfSuccess
		}
	);	
}
