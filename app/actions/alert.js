import { TRIGGER_ALERT, CLOSE_ALERT } from './actionTypes.js';

const triggerAlert = params => {
	return {
		type: TRIGGER_ALERT,
		message: params.message,
		mode: params.mode
	}
}

const closeAlert = () => {
	return {
		type: CLOSE_ALERT
	}
}

const triggerErrorAlert = message => {
	return triggerAlert({ message, mode: "error" });
}

const triggerSuccessAlert = message => {
	return triggerAlert({ message, mode: "success" });
}

export const showErrorAlert = message => {
	return dispatch => {
		dispatch(triggerErrorAlert( message));

		setTimeout(() => {
			dispatch(closeAlert());
		}, 5000)
	}
}

export const showSuccessAlert = message => {
	return dispatch => {
		dispatch(triggerSuccessAlert( message));

		setTimeout(() => {
			dispatch(closeAlert());
		}, 5000)
	}
}
