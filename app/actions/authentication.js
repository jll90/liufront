import { LOG_IN, LOG_OUT } from './actionTypes.js';

import { saveToken, destroyToken } from '../providers/authentication_provider.js';

import { authenticate } from '../../shared/services/authentication.js'; 

import { showErrorAlert } from './alert.js'; 

const logIn = () => {
	return {
		type: LOG_IN
	}
}

const logOut = () => {
	return {
		type: LOG_OUT
	}
}

/* Could move the following function to handler.js 
so it can live with the other handlers */

export const startSession = config => {
	return dispatch => {
		return authenticate(config)
			.then(data => {
				console.log('data', data);
				saveToken(data.authToken);
				dispatch(logIn());
			}).catch(err => {
				dispatch(showErrorAlert("No se pudo iniciar sesion"));
			})
 
	}		
}

export const destroySession = () => {
	destroyToken();
	return logOut;
}
