import { GET_CLIENTS } from './actionTypes.js';

import { index } from '../../shared/services/clients.js';

import asyncAction from './handler.js';

const getClientsSuccess = clients => {
	return {
		type: GET_CLIENTS,
		clients,
		loading: false,
		finished: true
	}
}

export const getClients = config => {
	return asyncAction (
		{
			apiCall: index,
			params: config,
			action: getClientsSuccess,
			authentication: true
		}
	);
}
