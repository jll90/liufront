import React from 'react';
import { render } from 'react-dom';

import PrivateProfile from '../../components/properties/private_profile.jsx'; 

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

import { connect } from 'react-redux';

import { showPropertyProfile } from '../../actions/properties.js';

class PrivatePropertyShow extends React.Component {

	componentDidMount(){
		this.props.getProperty({id: getPermalinkFromRoute()});		
	}

	render(){
		return (
			<PrivateProfile {...this.props}/>
		)
	}
}

const mapStateToProps = state => {
	return {
		property: state.propertiesReducer.privateProfile
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getProperty: params => {
			dispatch(showPropertyProfile(params))
		}	
	}
}

const privatePropertyToStore = connect(mapStateToProps, mapDispatchToProps)(PrivatePropertyShow);

export default privatePropertyToStore;
