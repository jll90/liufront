import React from 'react';
import { render } from 'react-dom';

import CustomSlider from '../../views/partials/slider.jsx'
import ApproximateLocation from '../../components/properties/approximate_location.jsx';
import SpaceConfiguration from '../../components/properties/space_configuration.jsx';
import SurfaceContainer from '../../components/properties/surface_container.jsx';
import ParkingSpots from '../../components/properties/parking_spots.jsx';
import StorageRooms from '../../components/properties/storage_rooms.jsx';
import Price from '../../views/partials/price.jsx';
import CustomDate from '../../views/partials/custom_date.jsx';

import { disableBodyScroll, enableBodyScroll } from '../../../shared/tools/layout.jsx';

import { 
  hasParkingSpots,
  hasStorageRooms,
  isSale,
  isLot,
	pricedInCLP,
	hasVideo
} from '../../../shared/helpers/properties.js';


import SellingPointsContainer from '../../components/selling_points/index.jsx';

import { alignFullScreenSlides } from '../../../shared/helpers/photos.js';

import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import withScriptjs from "react-google-maps/lib/async/withScriptjs";

import ServerTranslate from '../../../shared/components/server_translate.jsx';

import { toUpperCase, toTitleCase } from '../../../shared/tools/string_manipulation.js';

import { showProperty, getPropertyNotFound } from '../../actions/properties.js';

import { connect } from 'react-redux';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

import { showErrorAlert } from '../../actions/alert.js';

import StaticTranslate from '../../components/misc/static_translate.jsx';

const DEFAULT_ZOOM = 16;
const DEFAULT_POS = { lat: -33.4600259, lng: -70.6956273 };


const LocationMap = withScriptjs(
  withGoogleMap(
    props => {
    	var position = null;
    	if (parseFloat(props.position.lat)){
    		position = props.position;
    	} else {
    		position = DEFAULT_POS;
    	}
    	return (
	      <GoogleMap
	        defaultZoom={DEFAULT_ZOOM}
	        defaultCenter={DEFAULT_POS}
	        center={position}
	      >

	      	<Marker 
	      		position={position} 
	      	/>
	      </GoogleMap>
	    );
    }
    	
  )
);

const isNotFound = () => {
	const status = document.querySelector('meta[name="status"]').content;
	return (404 === parseInt(status)); 
}

class PropertyShow extends React.Component {

	constructor(){
		super();
		this.state = {
			fullScreenMode: false,
		};
	}

	componentDidMount(){
		if ( isNotFound()){
			this.props.didNotFindProperty();
		} else {
			this.props.loadProperty();
		}
		window.addEventListener('resize', alignFullScreenSlides, false);
	}

	componentDidUpdate(){
		alignFullScreenSlides();
		document.body.scrollTop = 0;
	}

	enableFullScreenMode = () => {
		this.setState({
			fullScreenMode: true
		});
		disableBodyScroll();
	}

	disableFullScreenMode = () => {
		this.setState({
			fullScreenMode: false
		});
		enableBodyScroll();
	}

	render () {
		const { hover, suggestedProperties, resultsLoaded, fullScreenMode } = this.state;
		const property = this.props.property;
		var fullScreenTriggerStyle = hover ? {display: 'block'} : {display: 'none'};
		var fullScreenModeStyle = fullScreenMode ? {display: 'block'} : {display: 'none'};
		var navbarStyle = fullScreenMode ? {display: 'none'} : {display: 'block'};
		var overflowModeStyle = fullScreenMode ? {overflow: 'hidden'} : {};

		if (property !== null && property !== undefined){

			const { photos, sellingPoints, operationType, parkingSpots, storageRooms, propertyType, publicDescription, county, city, approximateLocation, latitude, longitude, price, pricingUnit, videoUrl } = property;

			const showVideo = hasVideo(videoUrl);
						
			return (
				<div>

					<div className="fullscreen-slider-container" style={fullScreenModeStyle}>
						<a className="fullscreen-close-btn" onClick={this.disableFullScreenMode} href="#">
							X
						</a>
						<CustomSlider>
							{ photos.map((photo) => {
									const style = {
										"backgroundImage": ("url("+photo.url+")"),
										"backgroundSize": 'contain',
										"backgroundPosition": 'center center',
										"backgroundRepeat": 'no-repeat'
									};
									return (
										<div key={photo.id} className="slide">
											<div className="background-image" style={style}></div>
										</div>
									)
								})
							}
						</CustomSlider>
					</div>

					<div className="small-12 columns row property-content properties-content">
						<header className="page-main-header">
							<h1>
								<ServerTranslate 
									srcTxt={property.title}
								/>
							</h1>
							<ApproximateLocation property={property}/>	
						</header>
						<div className="information-container small-12 columns row">
							<div 
								className="small-12 medium-8 columns slider-container"> 
								<CustomSlider>
									{ photos.map((photo) => {
											const style = {
												"backgroundImage": ("url("+photo.url+")"),
												"backgroundSize": 'contain',
												"backgroundPosition": 'center center',
												"backgroundRepeat": 'no-repeat'
											};
											return (
												<div key={photo.id} className="slide">
													<div className="background-image" style={style}></div>
												</div>
											)
										})
									}
								</CustomSlider>
							</div>
							<div className="small-12 medium-4 columns basic-info">
								<Price property={property} />
								<SpaceConfiguration property={property} />
								<SurfaceContainer property={property} />
								<ParkingSpots property={property} />
								<StorageRooms property={property} />
								<div className="date"><CustomDate timestamp={property.createdAt} /></div>
							</div>
							<div className="small-12 columns sub-navigation">
								<a href="#pantalla-completa" onClick={this.enableFullScreenMode}>
									<i className="fa fa-arrows-alt" aria-hidden="true"></i>
									<StaticTranslate dictKey={'navigation.buttons.fullscreen'} />
								</a>
								<a href="#descripcion">
									<i className="fa fa-file-text" aria-hidden="true"></i>
									<StaticTranslate dictKey={'navigation.buttons.description'} />
								</a>
								<a href="#ubicacion">
									<i className="fa fa-map-marker" aria-hidden="true"></i>
									<StaticTranslate dictKey={'navigation.buttons.map'} />
								</a>
								{
									(showVideo) &&
										<a href="#video">
											<i className="fa fa-youtube-play" aria-hidden="true"></i>
										<StaticTranslate dictKey={'navigation.buttons.video'} />
										</a>
								}
							</div>
							<div className="small-12 columns long-description force-line-break" id="descripcion">
								<ServerTranslate 
									srcTxt={publicDescription}
								/>
							</div>
							<SellingPointsContainer sellingPoints={sellingPoints}/>
							<div className="small-12 columns map-container" id="ubicacion">
								
						 <LocationMap
								googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf0sdWmy49rAch5ygvRzexWWE-q7dgqJ8"
								loadingElement={
									<div style={{ height: `400px` }}>
										
									</div>
								}
								containerElement={
									<div style={{ height: `400px` }} />
								}
								mapElement={
									<div style={{ height: `400px` }} />
								}
								position={{lat: latitude, lng: longitude}}
						/>
							</div>
							{
									(showVideo) &&

										<div className="small-12 columns video-container" id="video">
											<iframe src={videoUrl} frameBorder="0" allowFullScreen="true"></iframe>
										</div>
							}
						</div>
					</div>
				</div>
			);
		}
		return null;
	}
}


const mapStateToProps = state => {
	return { property: state.propertiesReducer.show }
}

const mapDispatchToProps = dispatch => {
	return {	
		loadProperty: () => {
			dispatch(showProperty({id: getPermalinkFromRoute()}))
		},
		didNotFindProperty: () =>{
			dispatch(showErrorAlert("La propiedad no pudo ser encontrada"))
		}
	}
}

const showToStore = connect(mapStateToProps, mapDispatchToProps)(PropertyShow);

export default showToStore;
