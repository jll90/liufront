import React from 'react';
import { render } from 'react-dom';

import { frontEndRouter } from '../../routing/router.js';

import { gAddressInfo, createProperty, editPropertyForm, gReverseGeocode, loadEditProperty, updateProperty  } from '../../actions/properties.js';
import { getPlaces } from '../../actions/places.js';

import PropertyForm from '../../components/properties/with_form.jsx';

import { connect } from 'react-redux';

import { getPermalinkFromRoute, isEditingProperty } from '../../../shared/helpers/properties.js';

class PropertyNewEdit extends React.Component{

	componentDidMount(){
		this.props.getPlaces();
		const pathname = this.props.pathname;
		if ( this.isEditPropertyPath() ){
			this.props.loadEditForm({id: getPermalinkFromRoute(pathname)});			
		}
	}

	isEditPropertyPath = () => {
		return isEditingProperty();
	}

	render(){
		const pathname = this.props.pathname;
		const redirectPathname = (this.isEditPropertyPath() === true) ? (frontEndRouter('privatePropertyPath', { id: getPermalinkFromRoute(pathname) })) : (frontEndRouter('dashboardPath'));
		const buttonTxt = (this.isEditPropertyPath() === true) ? "Ingresar" : "Actualizar";
		return (
			<PropertyForm 
				buttonTxt={buttonTxt}
				redirectPathname={redirectPathname}
				{...this.props}	
			/>
		);
	}
}

const mapStateToProps = state => {
	const { counties, cities, states } = state.placesReducer;
	const submitSuccess = state.propertiesReducer.form.responseSuccess;
	const data = state.propertiesReducer.form.data;
	const pathname = state.routerReducer.location.pathname;
	return {
		places: {
			counties, cities, states	
		},
		submitSuccess,
		data,
		pathname	
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getPlaces: () => {
			dispatch(getPlaces());
		},
		retrieveAddressData: params => {
			dispatch(gAddressInfo(params))
		},
		reverseGeocode: params => {
			dispatch(gReverseGeocode(params))
		},
		submitProperty: params => {
			if (!isEditingProperty()){
				dispatch(createProperty(params))
			} else {
				dispatch(updateProperty({...params, id: getPermalinkFromRoute()}))
			}
		},
		updateFormData: params => {
			dispatch(editPropertyForm(params))
		},
		loadEditForm: params => {
			dispatch(loadEditProperty(params))
		}
	}
}

const propertyNewToStore = connect(mapStateToProps, mapDispatchToProps)(PropertyNewEdit);

export default propertyNewToStore;
