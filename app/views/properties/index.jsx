import React from 'react';
import { render } from 'react-dom';

import { connect } from 'react-redux';

import PropertiesList from '../../components/properties/list.jsx';

import { indexProperties } from '../../actions/properties.js';

import { getIndexParamsFromUrl } from '../../../shared/helpers/properties.js';

import StaticTranslate from '../../components/misc/static_translate.jsx';


/* first load sets state */
/* it doesn't matter whether we are redirected from inside the app 
or the request renders the app from zero */

class PropertyIndex extends React.Component { 
	
	componentDidMount(){
		this.props.loadProperties();
	}

	render(){
		const properties = this.props.properties;
		const loading = false;
		
		return (
			<div className="small-12 columns row properties-content">
				<header className="index-title page-main-header">
					<h1>
						<StaticTranslate dictKey={'headers.listings'} />
					</h1>
					{/*<div className="search-criteria">
						<div className="header">
							Criterio de Búsqueda
						</div>
						<div className="list-of-criteria">
							{searchCriteria}
						</div>
					</div>*/}
				</header>
				<PropertiesList 
					properties={properties} 
					resultsLoaded={loading}
				/>
			</div>
		);
	}
}


const mapStateToProps = (state, ownProps) => {
	return {
		properties: state.propertiesReducer.index
	}
}

const mapDispatchToProps = dispatch => {
	return {
		loadProperties: () => {
			dispatch(indexProperties(getIndexParamsFromUrl()))
		}
	}
}



const indexToStore = connect(mapStateToProps, mapDispatchToProps)(PropertyIndex);

export default indexToStore;
