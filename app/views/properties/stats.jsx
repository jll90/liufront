import React from 'react';
import { render } from 'react-dom';

import VisitNew from '../../components/visits/new.jsx';

import VisitIndex from '../../components/visits/index.jsx';

class PropertyStats extends React.Component {
	render(){
		return (
			<div className="row expanded">
				<div className="small-6 columns">
					<h2>Visitas</h2>
					<VisitIndex />
				</div>	
				<div className="small-6 columns">
					<h2>Llamadas</h2>
					<div>Numero Total: </div>
				</div>	
			</div>
		);
	}
}

export default PropertyStats;
