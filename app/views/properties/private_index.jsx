import React from 'react';
import { render } from 'react-dom';

import { propertyWalletConfig } from '../../../shared/charts/properties.js';

import PropertySelector from '../../components/properties/property_selector.jsx';

import moment from 'moment';

import { connect } from 'react-redux';

import { indexAllProperties } from '../../actions/properties.js';

class PrivateIndexRenderer extends React.Component {

	componentDidMount(){
		this.props.getProperties({filter: 'all'});
	}

	parseTimestamps = () => {
		const properties = this.props.properties;
		const timestamps = [];
		for (let p of properties){
			const timestamp = (moment(p.createdAt).valueOf()) ; 
			timestamps.push([timestamp, 1]);
		}
		return timestamps;
	}

	getAllPropertiesCount = () => {
		return this.props.properties.length;
	}

	getActivePropertiesCount = () => {
		const properties = this.props.properties;
		let count = 0;
	 	for(let p of properties ){
			if (p.stage === 4){
				count++;
			}	
		}
		return count;
	}

	getInactivePropertiesCount = () => {
		const properties = this.props.properties;
		let count = 0;
	 	for(let p of properties ){
			if (p.stage === 7){
				count++;
			}	
		}
		return count;
	}
	
	render(){
		const properties = this.props.properties;
		const allP = this.getAllPropertiesCount();
		const activeP = this.getActivePropertiesCount();
		const inactiveP = this.getInactivePropertiesCount();
		return (
			<div className="row">
				<h1>Listado de Propiedades</h1>

				<div className="row">
					<PropertySelector data={properties}/>
				</div>

				<div className="row dashboard-element">
					<p>Propiedades en Cartera</p>
					<div className="small-12 medium-6 large-4 columns dashboard-element">
						<div className="row neutral-element property-wallet-summary-el">
								<div className="small-8 columns">
									Total:	
								</div>
								<div className="small-4 columns">
									{allP}	
								</div>
						</div>	
					</div>					
					<div className="small-12 medium-6 large-4 columns dashboard-element">
						<div className="row positive-element property-wallet-summary-el">
								<div className="small-8 columns">
									Activas:	
								</div>
								<div className="small-4 columns">
									{activeP}	
								</div>
						</div>	
					</div>		
					<div className="small-12 medium-6 large-4 columns dashboard-element">
						<div className="row negative-element property-wallet-summary-el">
								<div className="small-8 columns">
									No activas:	
								</div>
								<div className="small-4 columns">
									{inactiveP}
								</div>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		properties: state.propertiesReducer.all
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getProperties: params => {
			dispatch(indexAllProperties(params))
		} 
	}
}

const privateIndexToStore = connect(mapStateToProps, mapDispatchToProps)(PrivateIndexRenderer);

export default privateIndexToStore;
