import React from 'react';
import { render } from 'react-dom';

import { Route, Redirect } from 'react-router-dom';

import { startSession } from '../../actions/authentication.js';
import { frontEndRouter } from '../../routing/router.js';

import { connect } from 'react-redux';

const INIT_STATE = {
	email: '',
	password: '',
	loggedIn: false
};

class SessionNew extends React.Component {

	constructor(){
		super();
		this.state = INIT_STATE;
	}

	onChange = (e) => {
		const { name, value } = e.target;
	  this.setState({[name]: value});	
	}

	onSubmit = (e) => {
		e.preventDefault();
		const data = this.state;
		const config = { data	};

		this.props.logIn(config);
	}

	render(){
		const { email, password, rememberMe } = this.state;
		const { loggedIn } = this.props;
		let pathname = undefined;
		if (this.props.location.state !== undefined){
			pathname = this.props.location.state.from.pathname;
		} else {
			pathname = frontEndRouter('dashboardPath');
		}
		if (!loggedIn){
			return (
				<div className="row">
					<div className="small-8 small-offset-2 columns log-in-container">
						<h1>Iniciar Sesión</h1>
						<form noValidate="novalidate" onSubmit={this.onSubmit}>
						  <p>
						    <input placeholder="Email" type="email" name="email" onChange={this.onChange} />
						    <input placeholder="Contraseña" type="password" name="password" onChange={this.onChange} />
						  </p>
						  <p>
						  	<button 
						  		type="submit" 
						  		className="button"
						  	>
						  		Iniciar Sesion
						  	</button>
						  </p>
						</form>
					</div>
				</div>
			);	
		} else {
			return (
				<Route>
					<Redirect to={{pathname: pathname, state: {}}} />
				</Route>
			);
		}
	}
}

const mapStateToProps = state => {
	return {
		loggedIn: state.authenticationReducer.loggedIn 
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logIn: config => {
			dispatch(startSession(config))
		}
	}
}

const newSessionToStore = connect(mapStateToProps, mapDispatchToProps)(SessionNew);

export default newSessionToStore;
