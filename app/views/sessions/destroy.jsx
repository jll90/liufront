import React from 'react';
import { render } from 'react-dom';



import { Route, Redirect } from 'react-router-dom';

import { frontEndRouter } from '../../routing/router.js';

import { destroyToken } from '../../providers/authentication_provider.js';

export default class SessionDestroy extends React.Component {

	constructor(){
		super();
		this.state = {
			loggedIn: true
		}
	}

	onClick = () => {
		const self = this;
		self.setState({
			loggedIn: false
		});
		destroyToken();
	}

	render(){
		const loggedIn = this.state.loggedIn;
		const props = this.props;
		if (loggedIn){
			return (
		    	<button onClick={this.onClick} >Cerrar Sesion</button>
			);	
		} else {
			return(
				<Route>
					<Redirect to={{pathname: frontEndRouter('homePath'), state: {from: props.location}}} />
				</Route>
			);
		}
		
	}
}
