import React from 'react';
import { render } from 'react-dom';

import Article from './article.jsx';

import { create } from '../../../shared/services/articles';



const INIT_STATE = {
	permalink: '',
	content: '',
	title: '',
	imageUrl: '',
}

class ArticleNew extends React.Component {

	constructor(){
		super();
		this.state = INIT_STATE;
	}

	handleTypingChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	submitForm = (e) => {
		e.preventDefault();
		
		const self = this;
		const successFn = function(){
			self.setState(INIT_STATE);

		}
		const errorFn = function(){

		}
		const data = this.state;
		const config = { data, successFn, errorFn };
		create(config);


	}

	render (){
		const { 
			permalink,
			title,
			content,
			imageUrl,
			input
		} = this.state;
		return (
			<div>
				<form className="row columns" onSubmit={this.submitForm}>
					<input type="text" placeholder="Permalink" name="permalink" onChange={this.handleTypingChange} value={permalink}/>
					<input type="text" placeholder="title" name="title" onChange={this.handleTypingChange} value={title}/>
					<input type="text" placeholder="image url" name="imageUrl" onChange={this.handleTypingChange} value={imageUrl}/>
					<textarea placeholder="Article content" name="content" onChange={this.handleTypingChange} value={content}></textarea>
					<input type="submit" className="button expand" value="Create"/>
				</form>
				<Article title={title} content={content}/>
			</div>
		);
	}
}

export default ArticleNew;
