import React from 'react';
import { render } from 'react-dom';

import Article from './article.jsx';

import { getPermalink } from '../../../shared/helpers/articles.js';

import { show } from '../../../shared/services/articles';

import { connect } from 'react-redux';

import store from '../../store.js';

import { getArticle } from '../../actions/articles.js';

class ArticleShow extends React.Component {

	componentDidMount(){
		store.dispatch(getArticle({id: getPermalink()}));
	}

	render (){
		const { loading, finished, article } = this.props;
		var articleComponent = null;
		if (article !== null){
			const { title, content, imageUrl } = article;
			articleComponent = <Article title={title} content={content} imageUrl={imageUrl} />
		} 
		return (
			<div className="article-content">
				{articleComponent}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { loading, finished, article } = state.articlesReducer.show;
	return {
		loading, finished, article 
	}
}

const articleToStore = connect(mapStateToProps)(ArticleShow);

export default articleToStore;
