import React from 'react';
import { render } from 'react-dom';

import ReactMarkdown from 'react-markdown';

class Article extends React.Component {
	constructor(){
		super();
	}

	render (){
		const { title, content, imageUrl }  = this.props;
		const style = {
				"marginTop": "60px",
		    "backgroundImage": "url("+ imageUrl +")",
		    "width": '100%',
		    "backgroundSize": "cover",
		    "backgroundRepeat": "no-repeat",
				"backgroundPosition": "center center",
				"height": "300px"
		};
		return (
			<article className="article-content small-12 columns row">
				<header className="page-main-header">
					<h1>{title}</h1>
					<div className="main-image" style={style} ></div>
				</header>
				<section className="content">
					<ReactMarkdown source={content} />
				</section>
			</article>
	);
	}
}

export default Article;
