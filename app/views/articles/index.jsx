import React from 'react';
import { render } from 'react-dom';

import removeMd from 'remove-markdown';
import TextTruncate from 'react-text-truncate';

import { Link } from 'react-router-dom';

import { frontEndRouter } from '../../routing/router.js';

import { connect } from 'react-redux';

import CustomDate from '../partials/custom_date.jsx';

import { currentLanguage } from '../../helpers/i18n.js';

import Notice from '../partials/notice.jsx';

import { getArticles } from '../../actions/articles.js';

import StaticTranslate from '../../components/misc/static_translate.jsx';

class ArticleIndex extends React.Component {

	componentDidMount(){
		this.props.getArticles();
	}

	render (){
		const { articles, loading, finished } = this.props; 
		const msg = "Sorry. We couldn't find any articles in this language.";
		if (articles.length === 0 && finished === true){
			return (
				<div className="small-12 columns">
					<Notice message={msg}/>
				</div>
			);
		} else {
			return (
				<div className="small-12 columns row articles-content">
					<header className="index-title page-main-header">
						<h1><StaticTranslate dictKey={'headers.articles'} /></h1>
					</header>
					<ArticleList articles={articles}/>
				</div>
			);	
		}
		
	}
}

class ArticleList extends React.Component {
	render (){
		const articles = this.props.articles;
		return (
			<div className="article-list row">
				{
					articles.map((article) => {
						return (<ArticleSnippet key={article.id} article={article} />);
					})
				}
			</div>
		);
	}
}

class ArticleSnippet extends React.Component {
	render (){
		const article = this.props.article;
		const articlePathWithPermalink = frontEndRouter('articlePath', {id: article.permalink});
		const content = removeMd(article.content);
		var imgUrl = undefined;
		if (article.imageUrl === null){
			imgUrl = "http://i.imgur.com/lQgM4eM.jpg";
		} else {
			imgUrl = article.imageUrl;
		}

		const style = {
		    "backgroundImage": "url("+ imgUrl +")",
		    "width": '100%',
		    "backgroundSize": "cover",
		    "backgroundRepeat": "no-repeat",
		    "backgroundPosition": "center center"
		}
		return (
				<div className="small-12 medium-6 large-4 columns article end">
					<div className="article-image" style={style}></div>
					<div className="text-container">
						<h3><Link to={articlePathWithPermalink} >{article.title}</Link></h3>
						<TextTruncate 
							line={3}
							text={content}
							textTruncateChild={<Link to={articlePathWithPermalink}>Seguir leyendo</Link>}
							className="text-intro"
						/>
						<div className="date">
							<CustomDate timestamp={article.createdAt} />
						</div>
					
					</div>
					
				</div>
		);
	}
}

const mapStateToProps = state => {
	const { articles, loading, finished } = state.articlesReducer.index;
	return { articles, loading, finished };
}

const mapDispatchToProps = dispatch => {
	return {
		getArticles: () => {
			const lang = currentLanguage();
			dispatch(getArticles({language: lang}));
		}
	}
}

const toStoreArticles = connect(mapStateToProps, mapDispatchToProps)(ArticleIndex);

export default toStoreArticles;