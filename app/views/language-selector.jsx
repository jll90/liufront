import React from 'react';

class LanguageSelector extends React.Component {
	render(){
		return (
			<div className="language-selector">
				<div className="header">
					<img src="http://i.imgur.com/1H8TwxD.png" alt="logo" />		
					<p>INTERNATIONAL REALTY</p>
				</div>
				<div className="languages">
					<a href={"/es"}>Español</a>
					<a href={"/en"}>English</a>
				</div>
			</div>
		);
	}
}

export default LanguageSelector;
