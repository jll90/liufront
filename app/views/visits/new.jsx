import React from 'react';
import { render } from 'react-dom';

import VisitNew as NewVisitForm from '../../components/visits.jsx'; 

class VisitNew extends React.Component {
	render(){
		return <NewVisitForm />	
	}
}

export default VisitNew;
