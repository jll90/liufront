import React from 'react';
import { render } from 'react-dom';

import { index } from '../../../shared/services/quotes';

import Quote from './quotes.jsx';

class QuoteIndex extends React.Component {

	constructor(){
		super();
		this.state = {
			quotes: []
		}
	}

	componentDidMount(){
		const self = this;
		const successFn = function(data){
			self.setState({quotes: data});
		}

		const config = {
			successFn
		};

		index(config);
	}

	render(){
		const quotes = this.state.quotes;
		return(
			<div>
				<h1>Lista de solicitudes</h1>
				<div className="quotes-container">
					{
						quotes.map((quote) => {
							return (<Quote quote={quote} key={quote.id} />);
						})
					}
				</div>
			</div>
		);
	}
}

export default QuoteIndex;