import React from 'react';
import { render } from 'react-dom';

class Quote extends React.Component {
	render(){
		const { fullname, message, email, phoneNumber } = this.props.quote;
		return (
			<div className="quote">
				<p className="fullname">
					{fullname}
				</p>
				<p className="message">
					{message}
				</p>
				<p className="email">
					{email}
				</p>
				<p className="phone-number">
					{phoneNumber}
				</p>
			</div>
		);
	}
}

export default Quote;