import React from 'react';
import { render } from 'react-dom';

import { create } from '../../../shared/services/quotes';

import { T, currentLanguage } from '../../helpers/i18n.js';

import { connect } from 'react-redux';

import { postQuote } from '../../actions/quotes.js';

class QuoteNew extends React.Component {

	constructor (){
		super();
		this.state = {
			email: '',
			phoneNumber: '',
			message: '',
			fullname: ''
		}
	}

	handleTypingChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	submitForm = (e) => {
		var self = this;
		e.preventDefault();

		const data = self.state;

		const config = {
			data
		};
		
		this.props.submitQuote(config, this.successfulSubmissionCallback);
	}


	successfulSubmissionCallback = () => {
		this.clearAllFields();
	}

	clearAllFields = () => {
		this.setState({
			email: '',
			phoneNumber: '',
			message: '',
			fullname: ''
		});
	}

	render (){
		const { email, phoneNumber, fullname, message } = this.state;
		const lang = currentLanguage();
		const header = T(lang, 'headers.quotes');
		const instructions = T(lang, 'instructions.quotes');

		//placeholders
		const fullnameP = T(lang, 'fields.fullname');
		const emailP = T(lang, 'fields.email');
		const phoneNumberP = T(lang, 'fields.phoneNumber');
		const dreamPropertyP = T(lang, 'fields.dreamProperty');

		const send = T(lang, 'navigation.buttons.send');
		return (
			<div className="first-wrapper">
				<div className="form-wrapper small-12 medium-8 medium-offset-2 columns row">
					<div className="marketing-header">
						{header}
					</div>
					<div className="explanation">
						{instructions}
					</div>
					<form className="row columns" onSubmit={this.submitForm}>
						<div className="step-1 small-12 medium-6 columns">
							<div className="step-number">1</div>
							<textarea placeholder={dreamPropertyP + "..."} name="message" onChange={this.handleTypingChange} value={message}></textarea>
						</div>
						<div className="step-2 small-12 medium-6 columns">
							<div className="step-number">2</div>
							<input type="text" placeholder={fullnameP} name="fullname" onChange={this.handleTypingChange} value={fullname}/>
							<input type="text" placeholder={emailP} name="email" onChange={this.handleTypingChange} value={email}/>
							<input type="text" placeholder={phoneNumberP} name="phoneNumber" onChange={this.handleTypingChange} value={phoneNumber}/>
						</div>
						<input type="submit" className="button expand" value={send}/>
					</form>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		submitQuote: (config, cb) => {
			dispatch(postQuote(config, cb));
		}
	}
}

const quoteNewToStore = connect(null, mapDispatchToProps)(QuoteNew);

export default quoteNewToStore;
