import React from 'react';
import { render } from 'react-dom';
import Slider from 'react-slick';

export class CustomNextArrow extends React.Component {
	render (){
		return (
			<span className="next-arrow" onClick={this.props.onClick}><i className="fa fa-long-arrow-right"></i></span>
		);
	}
}

export class CustomPreviousArrow extends React.Component {
	render (){
		return (
			<span className="prev-arrow" onClick={this.props.onClick}><i className="fa fa-long-arrow-left"></i></span>
		);
	}
}

const defaultSliderSettings = {
	infinite: false,      		
	speed: 600,
	slidesToShow: 1,
	slidesToScroll: 1,
	nextArrow: <CustomNextArrow />,
	prevArrow: <CustomPreviousArrow />
};

/*This component encapsulates the Slider component that's imported from npm*/
class CustomSlider extends React.Component {
	render (){
		const sliderSettings = defaultSliderSettings;
		const children = this.props.children;
		if (children.length === 0){
			return <div>Cargando...</div>;
		} else {
			return (
				<Slider {...sliderSettings}>
					{children}
				</Slider>
			);
		}
		
	}
}


export default CustomSlider;
