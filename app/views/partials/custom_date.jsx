import React from 'react';
import { render } from 'react-dom'
import moment from 'moment';

import 'moment/locale/es';

import { currentLanguage } from '../../helpers/i18n.js';

const locale = currentLanguage();

moment.locale(locale);

class CustomDate extends React.Component {
	render (){
		const timestamp = this.props.timestamp;
		const formattedDate = moment(timestamp).format("LL");
		return (
			<span>
				{formattedDate}
			</span>
		);
	}
}

export default CustomDate;
