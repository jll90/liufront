import React from 'react';
import { render } from 'react-dom';

class Notice extends React.Component {
	render(){
		const message = this.props.message;
		return (
			<div className="notice">
				{message}
			</div>
		);
	}
}

export default Notice;