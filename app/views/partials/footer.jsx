import React from 'react';
import { render } from 'react-dom';

import StaticTranslate from '../../components/misc/static_translate.jsx'; 

class Footer extends React.Component {
	render (){
		return (
			<footer className="small-12 columns">
				<div className="address">
					Huérfanos 1160, Oficina 1208, Santiago, Chile
				</div>
				<div className="copyright">
					© 2017 LIU-PROPIEDADES.COM					
				</div>
				<div className="disclaimer">
					<StaticTranslate dictKey={'disclaimer.footer'} />
				</div>
			</footer>
		);
	}
}

export default Footer;
