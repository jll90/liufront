import React from 'react';
import { render } from 'react-dom';

import { numberFormatter, ufFormatter, approximatePrice } from '../../../shared/tools/formatters.js';

import { pricedInCLP } from '../../../shared/helpers/properties.js';

import { connect } from 'react-redux';

const Price = props => { 

	const { property, latestUf } = props;
	if (property){
		const { pricingUnit, price, uf } = property;
		const formattedPrice = numberFormatter(price);
		const formattedUf = ufFormatter(uf);
		const formattedApproximation = approximatePrice(uf, latestUf);

		if (pricedInCLP(pricingUnit)){
			return (
				<div className="price">
					{formattedPrice}
				</div>
			);	
		} else {
			return (
				<div className="price">
					{formattedUf} <span className="approx">{formattedApproximation}</span>
				</div>
			);
		}
	}
	return null;
}

const mapStateToProps = state => {
	return { latestUf: state.indecesReducer.uf.latest }
}

const priceToStore = connect(mapStateToProps)(Price);

export default priceToStore;
