import React from 'react';
import { render } from 'react-dom';

import { frontEndRouter } from '../../../app/routing/router.js';

import SessionDestroy from '../sessions/destroy.jsx';

import { Link } from 'react-router-dom';

class PrivateNavbar extends React.Component{
	render(){
		return (
					<div className="private-navbar">
						<div className="link-btn">
							<Link to={frontEndRouter('dashboardPath')}>Panel de Control</Link>
						</div>
						<div className="link-btn">
							<Link to={frontEndRouter('clientsPath')}>Clientes</Link>
						</div>
						<div className="link-btn">
							<Link to={frontEndRouter('quotesPath')}>Solicitudes</Link>
						</div>
						<div className="link-btn">
							<Link to={frontEndRouter('emailDashboardPath')}>Central de Correos</Link>
						</div>
						<div className="link-btn">
							<Link to={frontEndRouter('propertiesPath')}>Propiedades</Link>
						</div>
						<div className="link-btn">
							<Link to={frontEndRouter('rootPath')}>Ir a pagina principal</Link>
						</div>
						<div className="link-btn">
							<SessionDestroy />
						</div>
					</div>
		);
	}
}

export default PrivateNavbar;
