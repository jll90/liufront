import React from 'react';
import { render } from 'react-dom';

import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';

import { frontEndRouter } from '../../../app/routing/router.js';
import { Link } from 'react-router-dom';

import { disableBodyScroll, enableBodyScroll } from '../../../shared/tools/layout.jsx';

const phoneNumber = "9-85972570";
const phoneHref = "tel:+56985972570";

import StaticTranslate from '../../components/misc/static_translate.jsx';

import LangRedirect from '../../components/language/lang_redirect.jsx';

class AlignLeftMenu extends React.Component {
	render (){
		return (
			<ul className="menu">
		      <li>
		      	<Link to={frontEndRouter('homePath')} className="home-link">
							<StaticTranslate dictKey={'navigation.navbar.main'} />
		      	</Link>
		      </li>
		      <li className="hoverable">
		        <Link to={frontEndRouter('articlesPath')}>
							<StaticTranslate dictKey={'navigation.navbar.blog'} />
						</Link>
		      </li>
		      <li className="hoverable">
						<Link to={frontEndRouter('servicesPath')}>
							<StaticTranslate dictKey={'navigation.navbar.services'} />
						</Link>
		      </li>
		    </ul>
		);
	}
}

class AlignRightMenu extends React.Component {
	render() {
		return (
			<ul className="menu">
				<li className="dropdownable">
					<LanguageDropdown />
				</li>
				<li className="phone-number">
					<i className="fa fa-whatsapp" aria-hidden="true"></i>
					{phoneNumber}
				</li>
			</ul>
		);
	}
}

class Navbar extends React.Component {

	constructor(){
		super();
		this.state = {
			mobileNavbarDepth: -1,
		};
	}

	increaseLevel = (operation) => {
		this.updateLevel(1);
	}

	decreaseLevel = () => {
		this.updateLevel(-1)
	}

	updateLevel = (update) => {
		const nextLevel = this.state.mobileNavbarDepth + update;
		this.setState({mobileNavbarDepth: nextLevel});
	}


	render (){
		const navbarDepth = this.state.mobileNavbarDepth;
		var levelZeroStr = "fullscreen-navbar level-zero";

		if (navbarDepth === 0){
			levelZeroStr += " engaged";
		}

		return (
			<div className="top-bar">
				<div className="top-bar-left desktop-only">
					<AlignLeftMenu />
				</div>
				<div className="top-bar-right desktop-only">
					<AlignRightMenu />
				</div>
				<div className="top-bar-left mobile-only">
					<ul className="menu">
						<li onClick={this.increaseLevel}>
							<i className="fa fa-bars" aria-hidden="true"></i>
						</li>
					</ul>
				</div>
				<div className={levelZeroStr}>
					<ul>
						<li>
							<Link to={frontEndRouter('homePath')}><i className="fa fa-home"></i>
								<StaticTranslate dictKey={'navigation.navbar.main'} />
							</Link>
						</li>
						<li>
							<Link to={frontEndRouter('articlesPath')}><i className="fa fa-rss"></i>
								<StaticTranslate dictKey={'navigation.navbar.blog'} />
							</Link>
						</li>
						<li>
							<Link to={frontEndRouter('servicesPath')}><i className="fa fa-heart"></i>
								<StaticTranslate dictKey={'navigation.navbar.services'} />
							</Link>
						</li>
						<li className="languages">
							<div>
								<i className="fa fa-language"></i>
								<div>
									<a href="/es">Español</a>|<a href="/en">English</a>
								</div>
							</div>
						</li>
						<li><a href={phoneHref}><i className="fa fa-whatsapp" aria-hidden="true"></i>{phoneNumber}</a></li>
						<span onClick={this.decreaseLevel} className="back-button"><i className="fa fa-arrow-left"></i></span>
					</ul>
				</div>
			</div>
		);
	}
}

export default Navbar;

class LanguageDropdown extends React.Component {
	render(){
		return (
			<Dropdown ref="dropdown">
				<DropdownTrigger>
					<div><StaticTranslate dictKey={'navigation.language'} /><i className="fa fa-caret-down"></i></div>
				</DropdownTrigger>
				<DropdownContent>
					<LangRedirect />					
				</DropdownContent>
			</Dropdown>
		);		
	}
}
