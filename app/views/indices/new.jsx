import React from 'react';
import { render } from 'react-dom';

import { saveIndexValue } from '../../../shared/services/indices';

import DatePicker from 'react-datepicker';

import moment from 'moment';

export default class IndexNew extends React.Component {
	constructor(){
		super();
		this.state = {
			readingTime: '',
			value: ''
		}
	}

	handleDateChange = (date) => {
		this.setState({
			readingTime: date
		});
	}

	handleTypingChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	}

	saveToDB = () => {
		const self = this;
		const successFn = function(){

			self.clearValues();
		}

		const readingTime = this.state.readingTime._d;
		const value = this.state.value;
		const name = "UF";

		const config = {
			data: { name, value, readingTime }
		}

		saveIndexValue(config);
	}

	clearValues = () => {
		this.setState({
			readingTime: '',
			value: ''
		})
	}

	render (){
		const { value, readingTime } = this.state;
	
		return (
			<div>
				<span>Valor: </span>
				<input value={value} name="value" onChange={this.handleTypingChange} />
				<span>Fecha: </span>
				<DatePicker selected={readingTime} onChange={this.handleDateChange} />
				<button type="button" className="button" onClick={this.saveToDB} >Guardar</button>
			</div>
		);
	}
}
