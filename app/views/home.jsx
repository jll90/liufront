import React from 'react';
import { render } from 'react-dom';

import { connect } from 'react-redux';

import { toggleFilter } from '../actions/navigation.js';
import { updatePropertySearchParams } from '../actions/property_search.js';
import { getLatestProperties, findByCode, unsetCode, updateCode } from '../actions/properties.js'; 

import { Link } from 'react-router-dom';

import { frontEndRouter } from '../routing/router.js';

import { withPropertyFilter } from '../components/properties/with_filter.jsx';

import { FindByCode } from '../components/properties/find_by_code.jsx';
import PropertyFilterSelector from '../components/properties/property_filter_selector.jsx';
import { JustListed } from '../components/properties/just_listed.jsx';

import { toTitleCase, toUpperCase } from '../../shared/tools/string_manipulation.js';

import StaticTranslate from '../components/misc/static_translate.jsx';

const HomeFilter = withPropertyFilter(PropertyFilterSelector);

class Home extends React.Component {

	componentDidMount(){
		this.props.getLatestProperties();
	}

	render(){
		const { changeSearchMenu, updateSearchParams, findByCode, resetCode, updateCode } = this.props;
		
		const { filter, justListedProperties, searchParams, foundCode, inputCode, propertyId } = this.props;

		const searchMenu = (filter === "menu") ? 
			<HomeFilter changeMenu={changeSearchMenu} updateSearchParams={updateSearchParams} searchParams={searchParams} /> : 
			<FindByCode changeMenu={changeSearchMenu} findByCode={findByCode} resetCode={resetCode} foundCode={foundCode} updateCode={updateCode} inputCode={inputCode} propertyId={propertyId} />;
		return (
			<div>
				<div className="row expanded logo-and-intro-row">
					<h1><StaticTranslate dictKey={'headers.main'} /></h1>
					<div className="small-12 medium-10 medium-offset-1 large-8 large-offset-2 end columns">
						 {searchMenu}
					</div>
				</div>
				<div className="row just-listed-row">
					<div className="title"><StaticTranslate dictKey={'meta.justListedTitle'} /></div>
					<div className="sub-title"><StaticTranslate dictKey={'meta.justListedSubtitle'} /></div>
					<JustListed properties={justListedProperties} /> 
				</div>
				<div className="row get-quote-row expanded">
					<h2>Solicitudes</h2>
					<div>Te ayudamos a buscar la propiedad que desees sea para inversion o para comenzar una nueva etapa de tu vida.</div>
					<Link className="button" to={frontEndRouter('newQuotePath')}>Solicitar</Link>
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		filter: state.toggleMenu.filter,
		justListedProperties: state.propertiesReducer.justListed,
		searchParams: state.propertySearchReducer,
		foundCode: state.propertiesReducer.code.found,
		inputCode: state.propertiesReducer.code.input,
		propertyId: state.propertiesReducer.code.propertyId
	}
}

const mapDispatchToProps = dispatch => {
	return {
		changeSearchMenu: (filter) => {
			dispatch(toggleFilter(filter))
		},
		updateSearchParams: (params) => {
			dispatch(updatePropertySearchParams(params))
		},
		getLatestProperties: () => {
			dispatch(getLatestProperties())
		},
		findByCode: code => {
			dispatch(findByCode(code))
		},
		resetCode: () => {
			dispatch(unsetCode())
		},
		updateCode: code => {
			dispatch(updateCode(code));
		}
	}
}

const homeToStore = connect(mapStateToProps, mapDispatchToProps)(Home);

export default homeToStore;
