import React from 'react';
import { render } from 'react-dom';


class Services extends React.Component {

	render(){
		return (
			<div className="services-container"> 
				<div className="row">
					<h1>Servicios</h1>
					<hr />
					<div>
						<h2>Análisis Legal</h2>
						<p>Verificamos que toda la documentación de la propiedad esté en orden y que no haya trámites que completar para evitar contratiempos que compliquen el negocio.</p>
					</div>
					<div>
						<h2>Análisis Comercial</h2>
<p>Realizamos un estudio de valor comercial basado en la Ley de la Oferta y la Demanda para determinar precisión el precio óptimo de venta o arriendo de su propiedad.</p>
					</div>
					<div>
						<h2>Levantamiento Técnico</h2>
						<p>Realizamos un expediente técnico para conocer todos los detalles de su propiedad, como sus fortalezas, para que destaque en el mercado. Añadido a esto, realizamos una sesión fotográfica para destacar los mejores puntos de la misma (con las herramientas adecuadas para este tipo de fotografía), lo que permite a cada uno de los interesados apreciarla en toda su dimensión. Cabe destacar que nuestras fotografías incluso son mejoradas digitalmente para causar la mejor impresión. (En algunas ocasiones añadimos video).</p>
					</div>
					<div>
						<h2>Publicidad</h2>
						<p>Colocamos letreros publicitarios en los alrededores en caso de ser necesario, e incluso desarrollamos cartas y folletos dirigidos a segmentos específicos. También hacemos promoción en los principales portales de internet que tienen el mayor número de visitas, y contamos con una fuerte presencia en redes sociales e internet lo que nos permite llegar a un mayor número de clientes</p>
					</div>
					<div>
						<h2>Atención a prospectos</h2>
						<p>Contamos con un equipo dedicado y debidamente entrenado para proporcionar información y hacer una demostración profesional de su propiedad.  Calificamos a los prospectos para evitarle molestias innecesarias, y además siempre acompañamos a todo prospecto que visita su propiedad.</p>
					</div>	
				</div>

			</div>
		);
	}
}

export default Services;
