import React from 'react';
import { Socket } from 'phoenix-socket';

let socket = new Socket("ws://localhost:4000/socket");

socket.connect();

let channel = socket.channel("room:lobby");

class Inbox extends React.Component {
	constructor(){
		super();
		this.state = {
			messages: [],
			message: ""
		}
	}

	componentDidMount(){
		channel.join()
			.receive("ok", resp => { console.log(resp) })
			.receive("error", resp => { console.error(resp) });

		channel.on("new_msg", msg => {
			console.log(msg);
			const messages = this.state.messages;
			messages.push(msg.body);
			this.setState( {messages});
		});
	}

	onChange = (e) => {
		const { name, value } = e.target;
		this.setState({[name]: value});	
	}

	clearMessage = () => {
		const message = "";
		this.setState({ message });
	}

	sendMessage = () => {
		const message = this.state.message;
		channel.push("new_msg", {body: message});
		this.clearMessage();
	}

	render(){
		const value = this.state.message;
		const messages = this.state.messages;
		return (
			<div>
				<p>This is the inbox</p>
				<input type="text" value={value} onChange={this.onChange} name="message"  />
				<button type="button" className="button default" onClick={this.sendMessage}>Send</button>
				<h2>Messages</h2>
				{ messages.map((m, i) => {
					return (<p key={i}>{m}</p>);
				})
				}
			</div>
		);
	}
}

export default Inbox;
