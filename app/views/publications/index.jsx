import React from 'react';
import { render } from 'react-dom';

import PublicationLoader from '../../components/publications/loader.jsx';

import PublicationNew from '../../components/publications/new.jsx';

class PublicationIndex extends React.Component {
	render(){
		return (
			<div>
				<h1>Publicaciones</h1>
				<hr />
				<PublicationNew />
				<hr />
				<PublicationLoader />
			</div>
		);
	}	
}

export default PublicationIndex;
