import React from 'react';
import { render } from 'react-dom';

import { 
	uploadFile as uploadService,
	removeFile as deleteService
 } from '../../shared/services/files.js';

import FileForm from '../components/files/file_form.jsx';

class FileUpload extends React.Component {
	render(){
		return (
			<div>
				<FileForm />;
			</div>
		) 
	}
}

export default FileUpload;
