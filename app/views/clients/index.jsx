import React from 'react';
import { render } from 'react-dom';

import { connect } from 'react-redux';

import { getClients } from '../../actions/clients.js';

import ClientList from '../../components/clients/list.jsx';


class ClientIndex extends React.Component {

	componentDidMount(){
		this.props.getClients();
	}

	render(){
		const { clients, loading, finished } = this.props;
		return (
			<div className="row">
				<h1>Clientes</h1>
				<div className="row dashboard-element">
					<ClientList clients={clients} />
				</div>
			</div>
		);	
	}
}
	
const mapStateToProps = state => {
	const { loading, clients, finished } = 	state.clientsReducer.index;
	return {
		loading, clients, finished 
	}	
}

const mapDispatchToProps = dispatch => {
	return {
		getClients: () => {
			dispatch(getClients());
		}
	}
}

const clientIndexToStore = connect(mapStateToProps, mapDispatchToProps)(ClientIndex);

export default clientIndexToStore;
