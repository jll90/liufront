import React from 'react';
import { render } from 'react-dom';

import CreateClient from '../../components/clients/new.jsx';

class ClientNew extends React.Component {
	render(){
		return(
			<div>
				<h1>Crear Cliente</h1>
				<CreateClient />
			</div>
		);	
	}
}

export default ClientNew;
