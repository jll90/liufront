import React from 'react';
import { render } from 'react-dom';

import MarketingEmailSingle from '../components/mailer/marketing_email_single.jsx';
import MarketingMassEmail from '../components/mailer/marketing_mass_email.jsx';
import PropertyMarketingWelcomeEmail from '../components/mailer/property_marketing_welcome_email.jsx';

import { sendMassEmail, sendSingleEmail } from '../actions/email.js';

import { connect } from 'react-redux';

class EmailDashboard extends React.Component {
	render (){
		return (
			<div className="first-wrapper dashboard">
				<div className="small-12 columns">
					<div className="row">
						<h3>Correo de Captacion Individual</h3>
						<MarketingEmailSingle sendSingleEmail={this.props.sendSingleEmail} />
					</div>
					<hr />
					<div className="row">
						<h3>Correo Masivo</h3>
						<MarketingMassEmail sendMassEmail={this.props.sendMassEmail} />
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {

	}
}

const mapDispatchToProps = dispatch => {
	return {
		sendSingleEmail: params => {
			dispatch(sendSingleEmail(params));
		},
		sendMassEmail: params => {
			dispatch(sendMassEmail(params));
		}
	}
}

const emailDashboardToStore = connect(mapStateToProps, mapDispatchToProps)(EmailDashboard)

export default emailDashboardToStore;