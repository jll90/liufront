import React from 'react';
import { render } from 'react-dom';

import Sortable from '../../components/photos/sortable.jsx';

import PhotoForm from '../../components/photos/form.jsx';

import { connect } from 'react-redux';

import { createPhoto, editPhotoForm, getPropertyPhotos, sortPhotos, saveSort, deletePhoto } from '../../actions/photos.js';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

class PhotoIndex extends React.Component {

	componentDidMount(){
		this.props.getPropertyPhotos();
	}

	render(){
		return (
			<div>
				<h1>Fotos</h1>
				<hr />
				<h3>Agregar foto</h3>
				<PhotoForm {...this.props}/>
				<hr />
				<h3>Ordenar Fotos</h3>
				<p>Arrastra las fotos para ordenarlas</p>
				<Sortable {...this.props} />
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		data: state.photosReducer.form.data,
		photos: state.photosReducer.photos,
		pathname: state.routerReducer.pathname
	}
}

const mapDispatchToProps = (dispatch, props) => {
	const pathname = props.location.pathname;
	const propertyId = getPermalinkFromRoute(pathname);
	return {
		createPhoto: params => {
			dispatch(createPhoto({...params, propertyId}));
		},
		updateFormData: params => {
			dispatch(editPhotoForm(params));
		},
		getPropertyPhotos: () => {
			dispatch(getPropertyPhotos({propertyId}));
		},
		sortPhotos: photos => {
			dispatch(sortPhotos(photos));
		},
		saveSort: params => {
			dispatch(saveSort(params));
		},
		deletePhoto: params => {
			dispatch(deletePhoto(params));
		}
	}
}

const photoIndexToStore = connect(mapStateToProps, mapDispatchToProps)(PhotoIndex);

export default photoIndexToStore;