import React from 'react';
import { render } from 'react-dom';

import Navbar from '../partials/navbar.jsx';
import PrivateNavbar from '../partials/private_navbar.jsx';
import Footer from '../partials/footer.jsx';

import PropertyNavbar from '../../components/properties/property_navbar.jsx';

import { getPermalinkFromRoute } from '../../../shared/helpers/properties.js';

export class MainLayout extends React.Component {
	render(){
		const classes = this.props.classes ? this.props.classes : [];
		const classesStr = ["first-wrapper", ...classes].join(" ");
		return (
			<div className={classesStr}>
				<Navbar />
				{this.props.children}
				<Footer />
			</div>
		);
	}
}

export class PrivateLayout extends React.Component {
	render(){
		return(
			<div> 
				<div className="row expanded">
					<div className="small-1 columns no-padding">
						<PrivateNavbar />
					</div>
					<div className="small-11 columns dashboard-content">
						{this.props.children}
					</div>
				</div>
			</div>
		);
	}
}

export class PropertyProfileLayout extends React.Component {
	render(){
		const id = getPermalinkFromRoute(); 
		return (
			<div className="property-layout"> 
				<div className="row expanded">
					<div className="small-1 columns no-padding">
						<PrivateNavbar />
					</div>
					<div className="small-11 columns">
						<div className="property-header">
							{"LIU"+id}
						</div>
						<PropertyNavbar />
						{this.props.children}
					</div>
				</div>
			</div>
		);
	}
}

export class EmptyLayout extends React.Component {
	render(){
		return(
			<div>
				{this.props.children}
			</div>
		)
	}
}
