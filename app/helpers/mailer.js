//https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

let DEFAULT_MESSAGE = "";

DEFAULT_MESSAGE += "Le brindamos la más calurosa bienvenida a nuestra organización LIU Propiedades; queremos agradecerle que nos haya elegido como sus agentes inmobiliarios. Tenga la seguridad de que haremos todo lo que esté en nuestras manos para lograr la venta y/o arriendo de su propiedad en el mejor precio, en el menor tiempo y sin problemas para usted.";
DEFAULT_MESSAGE += "\n\n";
DEFAULT_MESSAGE += "Somos un equipo capacitado con las herramientas necesarias para brindar un servicio de calidad y que la venta y/o arriendo de su propiedad sea la más efectiva y rentable. Es por ello, que es importante la colaboración por parte de usted en cuanto a las siguientes recomendaciones:";
DEFAULT_MESSAGE += "\n\n";
DEFAULT_MESSAGE += [
	"* Tener limpio y en orden el inmueble para mostrarlo",
	"* Eliminar malos olores",
	"* Si tiene mascota, evitar que perturbe al cliente",
	"* Permitir que el agente por si mismo muestre la propiedad",
	"* Evitar comentarios que muestren al cliente ansiedad por cerrar el negocio",
	"* Disposición para mostrar la propiedad en distintos horarios y días",
	"* Evitar negociaciones con el cliente sin el apoyo de su agente inmobiliario"
].join("\n");

let DEFAULT_MESSAGE_2 = "";
DEFAULT_MESSAGE_2 += "En caso de presentarse cualquier irregularidad en nuestro servicio, por favor comunicarse con el suscrito, estamos seguros que con su colaboración lograremos los resultados esperados.";
DEFAULT_MESSAGE_2 += "\n\n";
DEFAULT_MESSAGE_2 += "Agradecemos su ayuda y su preferencia";
DEFAULT_MESSAGE_2 += "\n\n";
DEFAULT_MESSAGE_2 += "Atentamente,"

const DEFAULT_PROFILE_NAME = "Publicación Principal";
const BASE_URL = "http://www.liu-propiedades.com";

export function buildEmailMessage (propertyId, publications, gender, name){
	let msg = "";
	msg += clientHeading(gender, name);
	msg += "\n\n";
	msg += DEFAULT_MESSAGE;
	msg += "\n\n";
	msg += "Su propiedad puede ser visitada en nuestro portal ";
	msg += generateMarkdownLink(BASE_URL, BASE_URL) +", haciendo click en el siguiente enlace: "
	msg += generateMarkdownLink(DEFAULT_PROFILE_NAME, generatePropertyUrl(propertyId)) + ".";
	msg += "\n\n"
	msg += "Adicionalmente, haciendo click en los enlaces, puede acceder al contenido publicado en las siguientes plataformas: ";
	msg += "\n\n";
	//this line generates the markdown list
	msg += parsePublications(publications);
	msg += "\n";
	msg += DEFAULT_MESSAGE_2;
	return msg;
}

function clientHeading(gender, name){
	if (gender === "M"){
		return "Estimado " + name + ":";
	} else {
		return "Estimada " + name + ":";
	}
}

function parsePublications(publications){
	let length = publications.length;
	let msg = "";
	for (var i=0; i < length; i++){
		msg += publicationToMarkdownLink(publications[i]);
	}
	return msg;
}

function publicationToMarkdownLink(publication){
	const platformName = publication.platform.name;
	const url = publication.url;
	return "* " + platformName + " " + generateMarkdownLink("Hacer click aquí", url) + ".\n";
}

function generatePropertyUrl (propertyId){
	return "http://www.liu-propiedades.com/es/propiedades/" + propertyId;
}

function generateMarkdownLink (alias, url){
	return `[${alias}](${url})`;
}