import { findInDict } from '../../shared/i18n/translator.js';

export const currentLanguage = () => {
 	let lang = window.location.pathname.split("/")[1].slice(0,2);
	if (lang === ""){
		lang = null;
	}
	return lang;
};

//only takes key
export const T = (lang, key) => {
	return findInDict(lang, key);
}
