import React from 'react';
import { render } from 'react-dom';

import PublicRoutes from './routing/public_routes.jsx';

import PrivateRoutes from './routing/private_routes.jsx';

import Alert from './components/misc/alert.jsx';


const Root = () => { 
	return (
		<div>
				<div>
					<PublicRoutes />
					<PrivateRoutes />
				</div>
				<Alert />
		</div>
	);
}

export default Root;
