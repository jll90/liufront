function getEnvironment(){
  return document.head.querySelector('meta[name="environment"]').content;
}

export function isDevelopment (){
	return (getEnvironment() === "development");	
}

export function isProduction () {
	return (getEnvironment() === "production");
}
