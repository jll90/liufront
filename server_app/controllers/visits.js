import db from '../db.js';
import { Visit } from '../models/visit.js';

export const postVisits = (req, res) => {
	const newVisit = new Visit(req.body);
	newVisit.save((err) => {
		res.sendStatus(201);
	});
}

export const getVisits = (req, res) => {
	const propertyId = req.params.propertyId;
	const q = Visit.find({'property_id': propertyId});
	q.exec((err, visits) => {
		res.json(visits);
	});
}