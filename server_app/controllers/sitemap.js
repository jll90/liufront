import { backendPath } from '../config/env.js';

export const indexSitemap = ((req, res) => {
	res.redirect(backendPath + "/sitemap.xml");
});
