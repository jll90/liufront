import axios from 'axios';
import { backendPath } from '../config/env.js';

import { appRenderer } from '../render.js'; 

export const getProperty = (req, res, next) => {
	let o = null;
	const url = req.originalUrl;
	let dataUrl = backendPath + url; 
	let dataUrlParams = dataUrl.split("/");
	dataUrlParams.splice(3, 1); //mutates original removes language/locale from path
	dataUrl = dataUrlParams.join("/");
	dataUrl = dataUrl.replace("/propiedades/", "/properties/");
	axios.get(dataUrl)
		.then(axiosRes => {
			const data = axiosRes.data;
			if (dataUrl.indexOf("properties") > -1){
				o = data;
			}

			if (dataUrl.indexOf("blog") > -1 || dataUrl.indexOf("blog") > -1){
				o = data;
			}
			appRenderer(req, res, o);	
		})
		.catch(err => {
			appRenderer(req, res, undefined, 404);
		})
}
