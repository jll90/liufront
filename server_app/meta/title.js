import { TL } from '../../app/helpers/i18n.js';

import { hasStr, toUpperCase } from '../../shared/tools/string_manipulation.js';

import { 
	isSale, isRent, isSeason, isHouse, isApartment, isCommercial, isLand, isOffice,
	hasBedrooms, hasBathrooms, isStudio, hasParkingSpots, hasStorageRooms
} from '../../shared/helpers/properties.js';

import inflector from '../../shared/tools/inflector.js';

export function titleGenerator(obj, url, lang){
	const T = TL(lang);

	const defaultName = T('meta.defaultName');
	let t = defaultName;	

	if (hasStr(url, "/blog/")){
		t = articleTitle(obj);
	}

	if (hasStr(url, "/blog")){
		const articlesIndexT = T('meta.articlesTitle');
		t = articlesIndexT;
	}

	if (hasStr(url, "/sale/") || hasStr(url, "/venta/")){
		const saleT = T('meta.saleTitle');
		t = saleT;
	}

	if (hasStr(url, "/rent/") || hasStr(url, "/arriendo/")){
		const rentT = T('meta.rentTitle');
		t = rentT;
	}

	if (hasStr(url, "/season/") || hasStr(url, "/temporada/")){
		const seasonT = T('meta.seasonTitle');
		t = seasonT;
	}

	if (hasStr(url, "/properties/") || hasStr(url, "/propiedades/")){
		t = propertyTitle(obj, lang);
	}

	if (hasStr(url, "/solicitudes/nuevo") || hasStr(url, "/quotes/new")){
		const quotesTitle = T('meta.quotesTitle');
		t = quotesTitle;
	}

	if (url === "/"){
		const defaultT = "Bienvenido - Welcome | LIU Propiedades";
		t = defaultT;
	}

	if (url === "/es"){
		const HT = TL("es");
		const homeT = HT('meta.homeTitle');
		t = homeT;
	}

	if (url === "/en"){
		const HT = TL("en");
		const homeT = HT('meta.homeTitle');
		t = homeT;
	}	

	if (t !== defaultName){
		t += " | " + defaultName;
	}

	return t;
}

function articleTitle(a){
	var t = a.title;
	var words = t.split(" ");
	var length = words.length;
	for (var i=0; i < length; i++){
		words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
	}
	t = words.join(" ");
	return t;
}

function propertyTitle(p, lang){
	const T = TL(lang);
	
	let propertyNames = [
		"house",
		"apartment",
		"office",
		"store", //overwrites commercial
		"industrial",
		"storage",
		"lot",
		"land",
		"parking"
	];

	let type = p.property_type;
	let op = p.operation_type;
	let title = null;

	let locationStr = `${p.county.name}, ${p.city.name}`;

	let tOperation = null;

	if (isSale(op)){
		tOperation = T('meta.title.operation.sale');
	}

	if (isRent(op)){
		tOperation = T('meta.title.operation.rent');
	}

	if (isSeason(op)){
		tOperation = T('meta.title.operation.rent');
	}

	let tPropertyType = null;

	if (isCommercial && p.right_to_key){
		tPropertyType = T('property.type.business');
	} else{
		tPropertyType = T('property.type.'+propertyNames[type]);
	}

	let tConfiguration = null;

	if (isHouse(type) || isApartment(type)){
		let bedroomStr = null;
		let bedroomWord = T('property.configuration.bedroom');
		if ( isStudio(p.single_space)){
			bedroomStr = T('property.configuration.studio');
		} else {
			bedroomStr = `${p.bedrooms} ${inflector(p.bedrooms, bedroomWord, lang)}`;
		}

		let bathroomWord = T('property.configuration.bathroom');
		let bathroomStr = `${p.bathrooms} ${inflector(p.bathrooms, bathroomWord, lang)}`;


		let parkingAndStorageStr = null;
		let parkingWord = T('property.configuration.parking_spot');
		let storageWord = T('property.configuration.storage_room');

		if (hasParkingSpots(p.parking_spots) && hasStorageRooms(p.storage_rooms)){
			parkingAndStorageStr = `${parkingWord} ${T('language.and')} ${storageWord}`;
		} else {
			if (hasParkingSpots(p.parking_spots)){
				parkingAndStorageStr = parkingWord;
			}

			if (hasStorageRooms(p.storage_rooms)){
				parkingAndStorageStr = storageWord;
			}
		}

		if (lang === "es"){
			title = `${toUpperCase(tOperation)} ${tPropertyType} de ${bedroomStr} y ${bathroomStr}`; 
			title += (parkingAndStorageStr !== null) ? ` con ${parkingAndStorageStr} ` : ` `;
			title += `en ${locationStr}`;
		}

		if (lang === "en"){
			title = `${toUpperCase(tPropertyType)} ${tOperation} in ${locationStr}`;
		}
	}

	if (isCommercial(type)){
		title = `${toUpperCase(tOperation)} ${tPropertyType} en ${locationStr}`;
	}

	if (isOffice(type)){
		title = `${toUpperCase(tOperation)} ${tPropertyType} en ${locationStr}`;
	}

	if (isLand(type)){
		let areaStr = `${p.big_surface}m2`;
	}

	return title;
}
