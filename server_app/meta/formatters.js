function formatPrice(p){
	var shortP = null;
	var pStr = null;
	if (p > 99999){
		shortP = p/1000000.0;
		pStr = shortP.toString() + " millones";
	}	else {
		shortP = p/1000.0;
		pStr = shortP.toString() + " mil";
	}

	return pStr;
}

function formatUFPrice(ufPrice){
	return ufPrice.toString() + " UF";
}

module.exports = {
	formatPrice: formatPrice, formatUFPrice: formatUFPrice
};
