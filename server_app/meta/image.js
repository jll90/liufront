const defaultImg = "http://i.imgur.com/GlCjI24.png";

import { hasStr } from '../../shared/tools/string_manipulation.js';

export function imageGenerator(obj, url){
	var img = defaultImg;

	if (hasStr(url, "/properties/") || hasStr(url, "/propiedades/")){
		img = obj.photos[0].url;
	}

	if (hasStr(url, "/blog/")){
		img = obj.image_url;
	}
	
	return img;
}
