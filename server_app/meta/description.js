import { formatPrice, formatUFPrice } from './formatters';

import { hasStr, toUpperCase } from '../../shared/tools/string_manipulation.js';

import { TL } from '../../app/helpers/i18n.js';

function articleDescription(a){
	return a.meta_description;
}

function propertyDescription(p, lang){
	let d = null;
	if (lang === "es"){
	
		d += p.public_description.split(/\n/)[0];
		d += " A sólo ";
		if (p.pricing_unit === "CLP"){
			d += formatPrice(p.price);
		} else {
			d += formatUFPrice(p.uf);
		}
		d += " - ";
		d += p.approximate_location;
	}
	return d;
}

export function descriptionGenerator(obj, path, lang){
	const T = TL(lang);

	const defaultDescription = T('meta.description.default');

	const blogIndexDescription = T('meta.description.articlesIndex');

	const saleDescription = T('meta.description.sale');

	const rentDescription = T('meta.description.rent');

	const seasonDescription = T('meta.description.season');

	const quotesDescription = T('meta.description.quotes');

	let description = null;

	if (path === "/"){
		description = null; 		
	}

	if (path === "/en" || path === "/es"){
		description = defaultDescription;
	}

	if (hasStr(path, "/blog")){
		description = blogIndexDescription;
	}

	if (hasStr(path, "/blog/")){
		description = articleDescription(obj);
	}

	if (hasStr(path, "/sale/") || hasStr(path, "/venta/")){
		description = saleDescription;
	}
	
	if (hasStr(path, "/rent/") || hasStr(path, "/arriendo/")){
		description = rentDescription;
	}
	
	if (hasStr(path, "/season/") || hasStr(path, "/temporada/")){
		description = seasonDescription;
	}

	if (hasStr(path, "/properties/") || hasStr(path, "/propiedades/")){
		description = propertyDescription(obj);
	}

	if (hasStr(path, "/solicitudes/nuevo") || hasStr(path, "/quotes/new")){
		description = quotesDescription;
	}

	return description;
}
