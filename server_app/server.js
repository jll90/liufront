import express from 'express';
import path from 'path';

import router from './routes.js'; 

import bodyParser from 'body-parser';

var pug = require('pug').__express;

const app = express();

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.use(bodyParser.json()); /* This line allows json data to come through - requires import of body-parser */;
app.use(router);


app.use('/build', express.static(path.join(__dirname, '../build')));

app.listen(8080, () => {
  console.log('App ready on port 8080!');
});
