export const env = process.env.NODE_ENV;
const dbServerIp = process.env.REW_MONGO_SERVER_IP;

export let backendPath = null;
let frontPath = null;
let reactjs = null;
export let dbPath = null;
let cssBundleName = 'styles-bundle.css';
export let cssBundlePath = '/build/' + cssBundleName; 

if (env === "development"){
	reactjs = "react-dev.js";
	backendPath = 'http://localhost:3000';
	frontPath = 'http://localhost:8080';
	dbPath = 'mongodb://localhost:27017/rew_dev';
}

if (env === "production"){
	reactjs = "react-bundle.js";
	backendPath = 'http://server.penthouse.solutions';
	frontPath = 'http://www.liu-propiedades.com';
	dbPath = 'mongodb://'+dbServerIp+'/rew_prod';
}

export let reactJsPath = '/build/' + reactjs;
