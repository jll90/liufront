import { dbPath } from './config/env.js';
import mongoose from 'mongoose'; 

let db = mongoose.connect(dbPath);

export default db;