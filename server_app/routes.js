import express from 'express';
import { getVisits, postVisits } from './controllers/visits.js';
import { getClients } from './controllers/clients.js';
import { indexSitemap } from './controllers/sitemap.js';
import { indexApp } from './controllers/index.js';
import { getProperty } from './controllers/properties.js';

const router = express.Router();

router.use((req, res, next) => {
	console.log('url1: ', req.originalUrl);
	next();
});

/* Backend App */

router.route('/clients')
	.get(getClients);

router.route('/visits')
	.post(postVisits);

router.route('/properties/:propertyId/visits')
	.get(getVisits);

/* Finish backend app */


/* From this point on all can be refactored to frontEndApp */

router.get('/sitemap.xml', indexSitemap);

router.get(/^\/{1}(es|en)\/{1}(propiedades|properties|blog)\/(\d+|[A-za-z\-0-9]+)\/?$/gi, getProperty);
/*router.get('/es/propiedades/:propertyId', getProperty);
router.get('/en/properties/:propertyId', getProperty); */
router.get(/^\/(en|es)/i, indexApp);
router.get('/', indexApp);

export default router;