import mongoose from 'mongoose';

const visitSchema = mongoose.Schema(
	{ 
		timestamp: Date, 
		property_id: Number,
		comment: String
	}
);

export const Visit = mongoose.model('Visit', visitSchema);
