import mongoose from 'mongoose';

const clientSchema = mongoose.Schema(
	{
		fullname: String,
		email: String,
		nationalId: String,
		phoneNumber: String,
		description: String,
		address: String,
		status: Number
	}
);

export const Client = mongoose.model('Client', clientSchema);