import { 
	cssBundlePath,
	env,
	reactJsPath	
} from './config/env.js'; 

import { titleGenerator } from './meta/title.js';
import { descriptionGenerator } from './meta/description.js';
import { imageGenerator } from './meta/image.js';

export const appRenderer = (req, res, obj, status = 200) => {
	let metaDescription = null;
	let metaImage = null;
	let metaTitle = null;
	if (obj !== undefined){
		const url = req.originalUrl;
		const locale = url[1] + url[2];
		try {
			metaTitle = titleGenerator(obj, url, locale); 
			metaDescription = descriptionGenerator(obj, url, locale); 
			metaImage = imageGenerator(obj, url); 
		}
		catch(e) {
			console.log(e);
		}
	}
	res.status(status);
	res.render('index', { 
		reactjs: reactJsPath, 
		env, 
		cssbundle: cssBundlePath, 
		metaTitle,
		metaImage,
		metaDescription,
		status,
		pathToResource: req.originalUrl,
	});
}
